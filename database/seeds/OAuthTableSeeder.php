<?php

use Illuminate\Database\Seeder;
// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class OAuthTableSeeder extends Seeder
{
  public function run()
  {
    DB::table('oauth_clients')->insert(
      ['id' => 'f3d259ddd3ed8ff3843839b',
      'secret' => '4c7f6f8fa93d59c45502c0ae8c4a95b',
      'name' => 'Mobile Client',
      'created_at' => '0000-00-00 00:00:00',
      'updated_at' => '0000-00-00 00:00:00']
    );
  }
}
