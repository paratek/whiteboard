<?php
use Illuminate\Database\Seeder;
// Composer: "fzaninotto/faker": "v1.3.0"

class PermissionsTableSeeder extends Seeder {

	public function run()
	{
		$now = Carbon\Carbon::now()->addMonths(-1);

		DB::table('permissions')->insert(array(
			array('name'=>'admin', 'display_name'=>'Super Admin', 'created_at'=>$now, 'updated_at'=>$now),
			array('name'=>'p1', 'display_name'=>'P1 perm', 'created_at'=>$now, 'updated_at'=>$now),
			array('name'=>'p2', 'display_name'=>'P2 perm', 'created_at'=>$now, 'updated_at'=>$now),
			array('name'=>'p3', 'display_name'=>'P3 perm', 'created_at'=>$now, 'updated_at'=>$now),
			)
		);
	}

}
