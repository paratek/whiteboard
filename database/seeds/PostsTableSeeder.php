<?php

use Illuminate\Database\Seeder;
// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class PostsTableSeeder extends Seeder
{
  public function run()
  {
    //TestDummy::times(20)->create('App\Post');
    $faker = Faker\Factory::create('en_US');

    $faker->addProvider(new Faker\Provider\Lorem($faker));

    //App\Forum::create(['name'=>'College News', 'desc'=>'Important college wide news', 'type'=>'0']);
    //App\Forum::create(['name'=>'Contests', 'desc'=>'Ongoing contests in TCET', 'type'=>'0']);
    //App\Forum::create(['name'=>'Events', 'desc'=>'Events oraganized in TCET', 'type'=>'0']);
    //App\Forum::create(['name'=>'Seminars', 'desc'=>'Seminars being conducted in TCET', 'type'=>'0']);
    //App\Forum::create(['name'=>'Trainings', 'desc'=>'Trainings being organized for students and faculty', 'type'=>'0']);



    /*
    foreach(App\Forum::all() as $forum)
    {
      foreach(App\Classroom::all() as $classroom)
      {
        $forum->classrooms()->attach($classroom->id);
      }
    }


    foreach(App\Department::all() as $dept)
    {
      $f1 = App\Forum::create(['name'=>$dept->dept_name.' Dept News', 'desc'=>$dept->dept_name.' Dept News', 'type'=>'1']);
      $f2 = App\Forum::create(['name'=>$dept->dept_name.' Dept Misc', 'desc'=>' Seminars and events being organized in '.$dept->dept_name.' department', 'type'=>'1']);
      foreach(App\Classroom::where('department_id',$dept->id)->get() as $classroom)
      {
        $f1->classrooms()->attach($classroom->id);
        $f2->classrooms()->attach($classroom->id);
      }
    }
    */

    foreach(App\SubjectForum::all() as $forum)
    {
      $now = Carbon\Carbon::now()->subDays(-10);
      $users = $forum->class_detail->users;
      if(count($users)>0)
      {
        foreach(range(1,mt_rand(20,30)) as $index1)
        {
          $post = new App\Post;
          $post->user_id = $users[mt_rand(0,count($users)-1)]['id'];
          if(mt_rand(1,4)!=1)
            $post->message = "<p>".$faker->text($maxNbChars = 50)."</p><p><strong>".$faker->text($maxNbChars = 50)."</strong></p><h1>".$faker->text($maxNbChars = 50)."</h1><pre>".$faker->text($maxNbChars = 50)."</pre>";
          else if(mt_rand(1,2)!=1)
            $post->message = "<p>".$faker->text($maxNbChars = 100)."</p><pre>".$faker->text($maxNbChars = 50)."</pre>";
          else
            $post->message = "<pre>".$faker->text($maxNbChars = 150)."</pre>";
          $post->flags = mt_rand(0,1);
          $post->upvotes = mt_rand(0,5);
          $now = $now->addHours(1.5);
          $post->created_at = $now;
          $post->updated_at = $now;
          $forum->posts()->save($post);
          $forum->increment('post_count');
        }
      }
    }

    foreach(App\ClassForum::all() as $forum)
    {
      $now = Carbon\Carbon::now()->subDays(-10);
      $users = $forum->users;
      if(count($users)>0)
      {
        foreach(range(1,mt_rand(20,30)) as $index1)
        {
          $post = new App\Post;
          $post->user_id = $users[mt_rand(0,count($users)-1)]['id'];
          if(mt_rand(1,4)!=1)
            $post->message = "<p>".$faker->text($maxNbChars = 50)."</p><p><strong>".$faker->text($maxNbChars = 50)."</strong></p><h1>".$faker->text($maxNbChars = 50)."</h1><pre>".$faker->text($maxNbChars = 50)."</pre>";
          else if(mt_rand(1,2)!=1)
            $post->message = "<p>".$faker->text($maxNbChars = 100)."</p><pre>".$faker->text($maxNbChars = 50)."</pre>";
          else
            $post->message = "<pre>".$faker->text($maxNbChars = 150)."</pre>";
          $post->flags = mt_rand(0,1);
          $post->upvotes = mt_rand(0,5);
          $now = $now->addHours(1.5);
          $post->created_at = $now;
          $post->updated_at = $now;
          $forum->posts()->save($post);
          $forum->increment('post_count');
        }
      }
    }
  }
}
