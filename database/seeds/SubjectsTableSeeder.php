<?php
use Illuminate\Database\Seeder;
// Composer: "fzaninotto/faker": "v1.3.0"

class SubjectsTableSeeder extends Seeder {

	public function run()
	{
		$subs = Excel::load('storage/app/subjects.csv')->get();

		$mods = Excel::load('storage/app/modules.csv')->get();

		foreach($subs as $sub)
		{
			$subject = \App\Subject::create(['name'=> $sub->subject_name, 'code'=>$sub->sub_code, 'is_elective'=>$sub->is_elective, 'has_prac'=>$sub->has_pract, 'has_theory'=>$sub->has_theory, 'credits'=>$sub->credits, 'shortform'=>$sub->shortform]);

			if($sub->sem%2==1)
				continue;
			if($sub->dept=='H&S')
				$classrooms = \App\Classroom::where('year_id', 1)->get();
			else
				$classrooms = \App\Classroom::where('department_id', 1)->get();

			foreach($classrooms as $class)
			{
				foreach(\App\ClassDetail::where('classroom_id', $class->id)->where('semester_id', $sub->sem)->get() as $classp)
				{
					foreach(\App\Faculty::all() as $fact)
					{
						if(mt_rand(1,20)==1 || (($fact->id==33 || $fact->id==34) && mt_rand(1,5)==1))
						{
							$fact->employee->user->classp()->attach($classp->id);
							$sub_all = \App\SubjectAllocation::create(['faculty_id'=>$fact->id, 'subject_id'=>$subject->id, 'classpivot_id'=>$classp->id, 'has_batches'=>($sub->has_pract|$sub->is_elective)]);
							break;
							/*
							if($subject->is_elective)
							{
								if($fact->id==33)
									$sub_all->update(['has_batches'=>true]);
								foreach(\App\ClasspivotUser::where('classpivot_id', $classp->id)->where('roll_no', '!=', '0')->get() as $user)
								{
									if(mt_rand(1,2)==1)
									{
										if($user->roll_no < 41 || !$sub_all->has_batches)
											$batch = 0;
										else
											$batch = 1;
										\App\Elective::create(['classpivot_user_id'=>$user->id, 'subject_allocation_id'=>$sub_all->id, 'batch_no'=>$batch]);
									}
								}
							}
							*/
						}
					}
				}
			}
		}

		foreach($mods as $mod)
		{
			$sub = \App\Subject::where('code', $mod->sub_code)->first();
			if(!is_null($sub))
				$module = \App\Module::create(['name'=>$mod->name, 'desc'=>$mod->desc, 'hours'=>$mod->hours, 'subject_id'=>$sub->id]);
		}

		foreach(\App\Year::where('name', '!=', 'FE')->get() as $year)
		{
			foreach(\App\Department::where('name', '!=', 'CMPN')->get() as $dept)
			{
				for($i=0; $i<mt_rand(4,6); $i++)
				{
					$subject = \App\Subject::create(['name'=>'Subject_'.$year->id.'_'.$dept->id.'_'.mt_rand(1,1000), 'shortform'=>'name'+mt_rand(1,400), 'code'=>'code'+mt_rand(1,400), 'is_elective'=>mt_rand(0,1), 'has_prac'=>mt_rand(0,1),
					'has_theory'=>mt_rand(0,1), 'credits'=>0.5*mt_rand(7,12) ]);

					foreach(range(3,6) as $mod)
					{
						$mname = $subject->name.'_mod_'.mt_rand(1000,9999);
						$is_lab = mt_rand(0,1);
						if($is_lab==0)
						{
							$mname = 'Exp'.$mname;
							$hours = mt_rand(5,10);
						}
						else {
							$mname = 'Exp'.$mname;
							$hours = 0;
						}
						\App\Module::create(['name'=>$mname, 'desc'=>$mname.'_desc',
						'subject_id'=>$subject->id, 'hours'=>$hours]);
					}

					foreach(\App\Classroom::where('year_id', $year->id)->where('department_id', $dept->id)->get() as $class)
					{
						foreach(\App\ClassDetail::where('classroom_id', $class->id)->get() as $classp)
						{
							foreach(\App\Faculty::all() as $fact)
							{
								if(mt_rand(1,20)==1 && ($fact->id!=33 || $fact->id!=34))
								{
									$fact->employee->user->classp()->attach($classp->id);
									$sub_all = \App\SubjectAllocation::create(['faculty_id'=>$fact->id, 'subject_id'=>$subject->id, 'classpivot_id'=>$classp->id, 'has_batches'=>($subject->has_prac|$subject->is_elective)]);

									/*
									if($subject->is_elective)
									{
										foreach(\App\ClasspivotUser::where('classpivot_id', $classp->id)->get() as $user)
										{
											if($user->roll_no < 41 || !$sub_all->has_batches)
												$batch = 0;
											else
												$batch = 1;
											if(mt_rand(1,2)==1)
											{
												\App\Elective::create(['classpivot_user_id'=>$user->id, 'subject_allocation_id'=>$sub_all->id, 'batch_no'=>$batch]);
											}
										}
									}
									*/

								}
							}
						}
					}
				}
			}
		}


		$ss = \App\Subject::where('is_elective', 1)->lists('id');
		foreach(\App\ClassDetail::all() as $classp)
		{
			$subs = \App\SubjectAllocation::where('classpivot_id', $classp->id)->whereIn('subject_id', $ss)->get();
			$subs_no = count($subs);
			if($subs_no==0)
				continue;
			foreach(\App\ClasspivotUser::where('classpivot_id', $classp->id)->where('roll_no', '!=', '0')->get() as $key=>$user)
			{
				if($user->roll_no < 41 || !$sub_all->has_batches)
					$batch = 0;
				else
					$batch = 1;
				\App\Elective::create(['classpivot_user_id'=>$user->id, 'subject_allocation_id'=>$subs[$user->roll_no % $subs_no]->id, 'batch_no'=>$batch]);
			}
		}


		foreach(\App\ClassDetail::all() as $classp)
		{
			$date = Carbon\Carbon::now()->subDays(55);
			$subs = $classp->subjects;
			$subsc = count($subs);
			$subs_n = $subs->filter(function ($f) use($ss) {
				return !in_array($f->subject_id, $ss->toArray());
			})->values();
			$subs_nc = count($subs_n);
			if($subs_nc==0)
				continue;
			$subs_e = $subs->filter(function ($f) use($ss) {
				return in_array($f->subject_id, $ss->toArray());
			})->values();
			$subs_ec = count($subs_e);
			$tot = 0;
			foreach(range(0,50) as $did)
			{
				$date = $date->addDay();
				if(!$date->isWeekday())
					continue;
				foreach(array(1, 5, 7, 12, 16, 20) as $key=>$tid)
				{
					$sa = $subs_n[$tot%$subs_nc];
					$tot=$tot+1;

					if(mt_rand(1,8)==1 && $subs_ec!=0)
						$s_arr = $subs_e;
					else
						$s_arr = array($sa);

					foreach($s_arr as $sas)
					{
						$sub = \App\Subject::find($sas->subject_id);
						$batch = null;
						if($sas->has_batches==1)
							$batch = mt_rand(0,1);
						$is_lab = mt_rand(0,$sub->has_prac);
						if($is_lab==0)
							$modules = \App\Module::where('subject_id', $sub->id)->where('hours', '!=', 0)->lists('id');
						else
							$modules = \App\Module::where('subject_id', $sub->id)->where('hours', 0)->lists('id');
						if(count($modules)==0)
							continue;
						$fl = \App\FacultyLecture::create(['subject_allocation_id'=>$sas->id, 'is_lab'=>$is_lab,
						'timeslot_id'=>$tid, 'module_id'=>$modules[mt_rand(0,count($modules)-1)],
						'date'=>$date->toDateString(), 'batch_no'=>$batch]);
						$cp_id = $sas->class_detail->id;
						$users = \App\ClasspivotUser::where('classpivot_id', $cp_id)->where('roll_no', '!=', 0)->get();

						if($sub->is_elective==1)
						{
							$uid = \App\Elective::where('subject_allocation_id', $sas->id)->lists('classpivot_user_id');
							$users = \App\ClasspivotUser::whereIn('id', $uid)->where('roll_no', '!=', 0)->get();
						}

						foreach($users as $user1)
						{
							if($sas->has_batches && $fl->is_lab==1 && $user1->batch_no!=$batch)
								continue;
							if(mt_rand(0,4)==4)
								\App\StudentAttendance::create(['faculty_lecture_id'=>$fl->id,
								'classpivot_user_id'=>$user1->id,
								'is_present'=>false]);
							else
								\App\StudentAttendance::create(['faculty_lecture_id'=>$fl->id, 'classpivot_user_id'=>$user1->id, 'is_present'=>true]);
						}
					}

				}
			}
		}
		// 1, 5, 7, 12, 16, 20
		/*
		foreach(\App\Faculty::all() as $fact)
		{
		  $date = Carbon\Carbon::now()->subDays(55);
			foreach(range(0,50) as $did)
			{
				$date = $date->addDay();
				if($date->isWeekday())
				{
					$ts = \App\Timeslot::all();
					for($tid=0; $tid<count($ts); $tid++)
					{
						if(mt_rand(0,1)==1)
						{
							$sas = \App\SubjectAllocation::where('faculty_id', $fact->id)->lists('id');
							if(count($sas)==0)
								continue;
							$sas_id = $sas[mt_rand(0,count($sas)-1)];

							$ts_s_c = \Carbon\Carbon::parse($ts[$tid]->start_time);
							$ts_e_c = \Carbon\Carbon::parse($ts[$tid]->end_time);

							$temp = \App\FacultyLecture::whereIn('subject_allocation_id', $sas)->where('timeslot_id', $ts[$tid]->id)->where('date', $date->toDateString())->first();
				      if($temp!=null)
								continue;
				      $temp = \App\FacultyLecture::whereIn('subject_allocation_id', $sas)->where('date', $date->toDateString())->get();
							$flag = false;
				      foreach($temp as $t1)
				      {
				        $t1_s_c = \Carbon\Carbon::parse($t1->timeslot->start_time);
				        $t1_e_c = \Carbon\Carbon::parse($t1->timeslot->end_time);
				        if($ts_s_c->between($t1_s_c, $t1_e_c, false) || $ts_e_c->between($t1_s_c, $t1_e_c, false) ||
				        $t1_s_c->between($ts_s_c, $ts_e_c, false) || $t1_e_c->between($ts_s_c, $ts_e_c, false))
				        {
									$flag = true;
									break;
								}
				      }
							if($flag)
								continue;
							$sas = \App\SubjectAllocation::find($sas_id);
							$sub = \App\Subject::find($sas->subject_id);
							$batch = null;
							if($sas->has_batches==1)
								$batch = mt_rand(0,1);
							$is_lab = mt_rand(0,$sub->has_prac);
							if($is_lab==0)
								$modules = \App\Module::where('subject_id', $sub->id)->where('hours', '!=', 0)->lists('id');
							else
								$modules = \App\Module::where('subject_id', $sub->id)->where('hours', 0)->lists('id');
							if(count($modules)==0)
								continue;
							$fl = \App\FacultyLecture::create(['subject_allocation_id'=>$sas_id, 'is_lab'=>$is_lab,
							'timeslot_id'=>$ts[$tid]->id, 'module_id'=>$modules[mt_rand(0,count($modules)-1)],
							'date'=>$date->toDateString(), 'batch_no'=>$batch]);
							$cp_id = $sas->class_detail->id;
							$users = \App\ClasspivotUser::where('classpivot_id', $cp_id)->where('roll_no', '!=', 0)->get();

							if($sub->is_elective==1)
							{
								$uid = \App\Elective::where('subject_allocation_id', $sas->id)->lists('classpivot_user_id');
			          $users = \App\ClasspivotUser::whereIn('id', $uid)->where('roll_no', '!=', 0)->get();
							}

							foreach($users as $user1)
							{
								if($sas->has_batches && $fl->is_lab==1 && $user1->batch_no!=$batch)
									continue;
								if(mt_rand(0,4)==4)
									\App\StudentAttendance::create(['faculty_lecture_id'=>$fl->id,
									'classpivot_user_id'=>$user1->id,
									'is_present'=>false]);
								else
									\App\StudentAttendance::create(['faculty_lecture_id'=>$fl->id, 'classpivot_user_id'=>$user1->id, 'is_present'=>true]);
							}
						}
					}
				}
			}
		}
		*/
	}
}
