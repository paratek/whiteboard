<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class UsersTableSeeder extends Seeder
{

  public function run()
  {
    // TestDummy::times(20)->create('App\Post');
    $faker = Faker::create();
    $now = Carbon\Carbon::now()->addMonths(-1);
    $user = new App\User;
    $user->name = 'Rupesh Parab';
    $user->email = 'admin@admin.com';
    $user->password = Hash::make('admin123');
    $now = $now->addHours(2);
    $user->created_at = $now;
    $user->updated_at = $now;
    $user->remember_token = md5(uniqid(mt_rand(), true));
    $user->save();

    DB::table('role_user')->insert(array('user_id'=>$user->id, 'role_id'=>'1'));

    $ecid = 1;

    $fcid = 1;

    $eid = DB::table('employees')->insertGetId(array('user_id'=>$user->id, 'r_no'=>mt_rand(1000000,9999999), 'shortform'=>'ABC', 'category_id'=>$ecid));

    DB::table('faculties')->insert(array('employee_id'=>$eid, 'designation'=>'Asst. Prfessor', 'is_approved'=>true, 'category_id'=>$fcid, 'department_id'=>1));



    $user = new App\User;
    $user->name = 'Rekha Sharma';
    $user->email = 'rekhasharma@tcet.com';
    $user->password = Hash::make('user123');
    $now = $now->addHours(2);
    $user->created_at = $now;
    $user->updated_at = $now;
    $user->remember_token = md5(uniqid(mt_rand(), true));
    $user->save();

    DB::table('role_user')->insert(array('user_id'=>$user->id, 'role_id'=>'5'));

    $ecid = 1;

    $fcid = 1;

    $eid = DB::table('employees')->insertGetId(array('user_id'=>$user->id, 'r_no'=>mt_rand(1000000,9999999), 'shortform'=>'ABC', 'category_id'=>$ecid));

    DB::table('faculties')->insert(array('employee_id'=>$eid, 'designation'=>'HOD', 'is_approved'=>true, 'category_id'=>$fcid, 'department_id'=>1));

    foreach(range(3, 2750) as $index)
    {
      $user = new App\User;
      $user->name = $faker->name;
      $user->email = $faker->username.mt_rand(1,5).'@'.$faker->safeEmailDomain;
      $tp = App\User::where('email', $user->email)->first();
      while($tp!=null)
      {
        $user->email = $faker->username.mt_rand(1,5).'@'.$faker->safeEmailDomain;
        $tp = App\User::where('email', $user->email)->first();
      }
      $user->password = Hash::make('user123');
      $user->created_at = $now;
      $user->updated_at = $now;
      $user->remember_token = md5(uniqid(mt_rand(), true));
      $user->save();
      if(mt_rand(1,50)==50)
      {
        $role = \App\Role::where('name', 'fact')->first();
        DB::table('role_user')->insert(array('user_id'=>$user->id, 'role_id'=>$role->id));

        $eid = DB::table('employees')->insertGetId(array('user_id'=>$user->id, 'r_no'=>mt_rand(1000000,9999999), 'shortform'=>'ABC', 'category_id'=>$ecid));

        DB::table('faculties')->insert(array('employee_id'=>$eid, 'designation'=>'Asst. Prfessor', 'is_approved'=>true, 'category_id'=>$fcid));
      }
      else
      {
        $classp = ($user->id % 32) + 1;
        $count = DB::table('classpivot_user')->where('classpivot_id', $classp)->count();
        \App\Student::create(['user_id'=>$user->id, 'gr_no'=>'TCET'+mt_rand(10000,99999)]);
        if($count<40)
          $batch = 0;
        else
          $batch = 1;
        DB::table('classpivot_user')->insert(array('classpivot_id'=>$classp, 'user_id'=>$user->id, 'roll_no'=>$count+1, 'batch_no'=>$batch));
        $role = App\Role::where('name', 'stud')->first();
        DB::table('role_user')->insert(array('user_id'=>$user->id, 'role_id'=>$role->id));
      }
    }
  }

}
