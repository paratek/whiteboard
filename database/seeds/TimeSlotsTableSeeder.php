<?php
use Illuminate\Database\Seeder;
// Composer: "fzaninotto/faker": "v1.3.0"

class TimeSlotsTableSeeder extends Seeder {

	public function run()
	{

		DB::table('timeslots')->insert(array(
			array('start_time'=>'08:30:00', 'end_time'=>'09:30:00'),
			array('start_time'=>'08:30:00', 'end_time'=>'10:30:00'),
			array('start_time'=>'09:15:00', 'end_time'=>'10:15:00'),
			array('start_time'=>'09:15:00', 'end_time'=>'11:15:00'),
			array('start_time'=>'09:30:00', 'end_time'=>'10:30:00'),
			array('start_time'=>'10:15:00', 'end_time'=>'11:15:00'),
			array('start_time'=>'10:45:00', 'end_time'=>'11:45:00'),
			array('start_time'=>'10:45:00', 'end_time'=>'12:45:00'),
			array('start_time'=>'11:30:00', 'end_time'=>'12:30:00'),
			array('start_time'=>'11:30:00', 'end_time'=>'13:30:00'),
			array('start_time'=>'11:45:00', 'end_time'=>'12:45:00'),
			array('start_time'=>'13:30:00', 'end_time'=>'14:30:00'),
			array('start_time'=>'13:30:00', 'end_time'=>'15:30:00'),
			array('start_time'=>'14:15:00', 'end_time'=>'15:15:00'),
			array('start_time'=>'14:15:00', 'end_time'=>'16:15:00'),
			array('start_time'=>'14:30:00', 'end_time'=>'15:30:00'),
			array('start_time'=>'14:30:00', 'end_time'=>'16:30:00'),
			array('start_time'=>'15:15:00', 'end_time'=>'16:15:00'),
			array('start_time'=>'15:15:00', 'end_time'=>'17:15:00'),
			array('start_time'=>'15:30:00', 'end_time'=>'16:30:00'),
			array('start_time'=>'15:30:00', 'end_time'=>'17:30:00'),
			array('start_time'=>'16:15:00', 'end_time'=>'17:15:00'),
			array('start_time'=>'16:15:00', 'end_time'=>'18:15:00'),
			)
		);
	}

}
