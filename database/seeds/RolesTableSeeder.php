<?php
use Illuminate\Database\Seeder;
// Composer: "fzaninotto/faker": "v1.3.0"

class RolesTableSeeder extends Seeder {

	public function run()
	{
		$now = Carbon\Carbon::now()->addMonths(-1);
		DB::table('roles')->insert(array(
				array('name'=>'admin', 'display_name'=>'Admin', 'created_at'=>$now, 'updated_at'=>$now),
				array('name'=>'moderator', 'display_name'=>'Moderator', 'created_at'=>$now, 'updated_at'=>$now),
				array('name'=>'fact', 'display_name'=>'Faculty', 'created_at'=>$now, 'updated_at'=>$now),
				array('name'=>'stud', 'display_name'=>'Student', 'created_at'=>$now, 'updated_at'=>$now),
				array('name'=>'hod', 'display_name'=>'H.O.D.', 'created_at'=>$now, 'updated_at'=>$now)
			)
		);

		DB::table('permission_role')->insert(array(
			array('permission_id'=>'1', 'role_id'=>'1'),
			array('permission_id'=>'2', 'role_id'=>'1'),
			array('permission_id'=>'3', 'role_id'=>'1'),
			array('permission_id'=>'4', 'role_id'=>'1'),
			array('permission_id'=>'2', 'role_id'=>'2'),
			array('permission_id'=>'3', 'role_id'=>'2'),
			array('permission_id'=>'4', 'role_id'=>'2'),
			array('permission_id'=>'2', 'role_id'=>'3'),
			array('permission_id'=>'3', 'role_id'=>'3'),
			array('permission_id'=>'4', 'role_id'=>'3'),
			array('permission_id'=>'3', 'role_id'=>'4'),
			array('permission_id'=>'4', 'role_id'=>'4')
			)
		);
	}

}
