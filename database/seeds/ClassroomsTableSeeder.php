<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use Faker\Factory as Faker;

class ClassroomsTableSeeder extends Seeder
{
  public function run()
  {
    $faker = Faker::create();

    \App\SemesterCategory::create(['description'=>'Odd Semester']);
    \App\SemesterCategory::create(['description'=>'Even Semester']);

    foreach(range(1,8) as $sem)
      \App\Semester::create(['name'=>'Semster '.$sem, 'semester_categories_id'=>($sem+1)%2 + 1]);

    $branch_ar = array("CMPN", "IT", "MECH", "ETRX");
    \App\Year::create(['name'=>'FE']);
    \App\Year::create(['name'=>'SE']);
    \App\Year::create(['name'=>'TE']);
    \App\Year::create(['name'=>'BE']);
    $now = Carbon\Carbon::now()->addMonths(-1);
    foreach($branch_ar as $branch)
      \App\Department::create(['name'=>$branch]);
    foreach(range(2012,2016) as $ayear)
      \App\AcademicYear::create(['start_year'=>$ayear, 'end_year'=>$ayear+4]);

    $ecid = DB::table('employee_categories')->insertGetId(array('description'=>'Grade A'));

    $fcid = DB::table('faculty_categories')->insertGetId(array('description'=>'Teaching Faculty'));

    foreach(range(1,4) as $branch)
    {
      foreach(range(1,4) as $year)
      {
        foreach(range(1,2) as $div)
        {
          if($div==1)
            $c = \App\Classroom::create(['name'=>\App\Year::find($year)->name.' '.\App\Department::find($branch)->name.' A', 'department_id'=>$branch, 'year_id'=>$year]);
          else
            $c = \App\Classroom::create(['name'=>\App\Year::find($year)->name.' '.\App\Department::find($branch)->name.' B', 'department_id'=>$branch, 'year_id'=>$year]);

          $user = new App\User;
          $user->name = $faker->name;
          $user->email = $faker->username.mt_rand(1,5).'@'.$faker->safeEmailDomain;
          $tp = App\User::where('email', $user->email)->first();
          while($tp!=null)
          {
            $user->email = $faker->username.mt_rand(1,5).'@'.$faker->safeEmailDomain;
            $tp = App\User::where('email', $user->email)->first();
          }
          $user->password = Hash::make('user123');
          $now = $now->addHours(2);
          $user->created_at = $now;
          $user->updated_at = $now;
          $user->remember_token = md5(uniqid(mt_rand(), true));
          $user->save();
          $role = \App\Role::where('name', 'fact')->first();
          DB::table('role_user')->insert(array('user_id'=>$user->id, 'role_id'=>$role->id));

          $eid = DB::table('employees')->insertGetId(array('user_id'=>$user->id, 'r_no'=>mt_rand(1000000,9999999), 'shortform'=>'ABC', 'category_id'=>$ecid));

          $fid = \App\Faculty::create(['employee_id'=>$eid, 'designation'=>'Asst. Professor', 'is_approved'=>true, 'category_id'=>$fcid]);



          \App\ClassDetail::create(['classroom_id'=>$c->id ,'semester_id'=>2*$year, 'academic_year_id'=>$year, 'class_incharge_id'=>$fid->id]);
        }
      }
    }
  }

}
