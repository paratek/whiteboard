<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call('PermissionsTableSeeder');
        $this->call('TimeSlotsTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('ClassroomsTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('OAuthTableSeeder');
        $this->call('SubjectsTableSeeder');
        //$this->call('PostsTableSeeder');
        $this->call('EventsTableSeeder');
        Model::reguard();
    }
}
