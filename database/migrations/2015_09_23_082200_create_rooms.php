<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRooms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rooms', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('description');
			$table->integer('room_category_id')->unsigned();
			$table->integer('incharge_employee_id')->unsigned();
			$table->timestamps();
			$table->foreign('room_category_id')->references('id')->on('room_categories')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('incharge_employee_id')->references('id')->on('employees')
                ->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rooms');
	}

}
