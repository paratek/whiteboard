<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('events', function(Blueprint $table)
  		{
  			$table->increments('id');
        $table->text('title');
        $table->longText('main')->nullable();
        $table->longText('about')->nullable();
        $table->longText('learn_more')->nullable();
        $table->integer('user_id')->unsigned();
        $table->dateTime('start');
        $table->dateTime('end');
        $table->integer('capacity')->nullable();
        $table->integer('seats_left')->nullable();
  			$table->timestamps();
        $table->foreign('user_id')->references('id')->on('users')
            ->onUpdate('cascade')->onDelete('cascade');

  		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('events');
    }
}
