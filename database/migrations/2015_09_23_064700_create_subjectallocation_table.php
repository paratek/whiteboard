<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectallocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subject_allocations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('faculty_id')->unsigned();
			$table->integer('subject_id')->unsigned();
			$table->integer('classpivot_id')->unsigned();
			$table->integer('post_count')->unsigned()->default(0);
			$table->boolean('has_batches')->default(false);
			$table->timestamps();
			$table->foreign('faculty_id')->references('id')->on('faculties')
                ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('subject_id')->references('id')->on('subjects')
                ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('classpivot_id')->references('id')->on('classpivot')
          			->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subject_allocations');
	}

}
