<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClasspivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('classpivot', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('classroom_id')->unsigned();
			$table->integer('semester_id')->unsigned();
			$table->integer('academic_year_id')->unsigned();
			$table->integer('post_count')->unsigned()->default(0);
			$table->integer('class_incharge_id')->unsigned();
			$table->foreign('classroom_id')->references('id')->on('classrooms')
            ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('semester_id')->references('id')->on('semesters')
            ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('academic_year_id')->references('id')->on('academic_years')
            ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('class_incharge_id')->references('id')->on('faculties')
            ->onUpdate('cascade')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('classpivot');
	}

}
