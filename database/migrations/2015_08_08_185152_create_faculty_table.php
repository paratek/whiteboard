<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacultyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('faculties', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('employee_id')->unsigned();
			$table->string('designation');
			$table->boolean('is_approved');
			$table->integer('category_id')->unsigned();
			$table->integer('department_id')->unsigned()->nullable();
			$table->timestamps();

      $table->foreign('employee_id')->references('id')->on('employees')
      			->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('category_id')->references('id')->on('faculty_categories')
            ->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('department_id')->references('id')->on('departments')
        ->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('faculties');
	}

}
