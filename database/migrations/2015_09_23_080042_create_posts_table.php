<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->text('message');
			$table->integer('flags')->unsigned()->default(0);
			$table->integer('upvotes')->unsigned()->default(0);
			$table->morphs('postable');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('classpivot_user')
        ->onUpdate('cascade')->onDelete('cascade');
			/*
			$table->foreign('forum_id')->references('id')->on('subject_allocations')
        ->onUpdate('cascade')->onDelete('cascade');
			*/
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
