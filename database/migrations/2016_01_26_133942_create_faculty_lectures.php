<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacultyLectures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('faculty_lectures', function(Blueprint $table)
      {
        $table->increments('id');
        $table->integer('subject_allocation_id')->unsigned();
        $table->integer('timeslot_id')->unsigned();
        $table->integer('module_id')->unsigned()->nullable();
        $table->tinyInteger('batch_no')->unsigned()->nullable();
        $table->boolean('is_lab')->default(false);
        $table->date('date');

  			$table->timestamps();
  			$table->foreign('module_id')->references('id')->on('modules')
                  ->onUpdate('cascade')->onDelete('cascade');
        $table->foreign('subject_allocation_id')->references('id')->on('subject_allocations')
            ->onUpdate('cascade')->onDelete('cascade');
         $table->foreign('timeslot_id')->references('id')->on('timeslots')
            ->onUpdate('cascade')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
  		Schema::drop('faculty_lectures');
    }
}
