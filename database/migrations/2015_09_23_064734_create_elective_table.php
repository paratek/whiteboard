<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectiveTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('electives', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('subject_allocation_id')->unsigned();
			$table->tinyInteger('batch_no')->unsigned();
			$table->integer('classpivot_user_id')->unsigned();
			$table->timestamps();

			$table->foreign('classpivot_user_id')->references('id')->on('classpivot_user')
                ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('subject_allocation_id')->references('id')->on('subject_allocations')
                ->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('electives');
	}

}
