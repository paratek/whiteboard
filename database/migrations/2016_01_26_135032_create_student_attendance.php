<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendance extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_attendance', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('faculty_lecture_id')->unsigned();
      $table->integer('classpivot_user_id')->unsigned();
      $table->boolean('is_present');

			$table->timestamps();
      $table->foreign('faculty_lecture_id')->references('id')->on('faculty_lectures')
          ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('classpivot_user_id')->references('id')->on('classpivot_user')
          ->onUpdate('cascade')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_attendance');
	}

}
