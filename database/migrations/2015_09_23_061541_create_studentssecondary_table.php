<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentssecondaryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_secondary_details', function(Blueprint $table)
		{
			$table->integer('stud_id')->unsigned();
			$table->integer('course_id')->unsigned();
      $table->string('surname');
      $table->string('first_name');
      $table->string('middle_name');
      $table->string('maidens_name');
      $table->string('permanent_address');
      $table->string('current_address');
      $table->string('student_mobile_no');
      $table->string('parent_mobile_no');
      $table->string('student_email');
      $table->string('parent_email');
			$table->timestamps();

			$table->primary('stud_id');
			$table->foreign('stud_id')->references('id')->on('students')
            ->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('course_id')->references('id')->on('courses')
			      ->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_secondary_details');
	}

}
