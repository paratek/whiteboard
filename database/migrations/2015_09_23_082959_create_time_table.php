<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('time_table', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('classpivot_id')->unsigned();
			$table->integer('weekday_id')->unsigned();
			$table->integer('timeslot_id')->unsigned();
			$table->integer('subject_allocations_id')->unsigned();
			$table->integer('room_id')->unsigned();

			$table->timestamps();

			$table->foreign('classpivot_id')->references('id')->on('classpivot')
                ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('weekday_id')->references('id')->on('week_days')
                ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('timeslot_id')->references('id')->on('timeslots')
                ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('subject_allocations_id')->references('id')->on('subject_allocations')
                ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('room_id')->references('id')->on('rooms')
                ->onUpdate('cascade')->onDelete('cascade');
		});




	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('time_table');
	}

}
