<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesecondaryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employee_secondary_details', function(Blueprint $table)
		{
			$table->integer('emp_id')->unsigned();
      $table->string('surname');
      $table->string('first_name');
      $table->string('middle_name');
      $table->string('maidens_name');
      $table->string('permanent_address');
      $table->string('current_address');
      $table->string('employee_mobile_no');
      $table->string('employee_email');

			$table->timestamps();

			$table->primary('emp_id');
			$table->foreign('emp_id')->references('id')->on('employees')
                ->onUpdate('cascade')->onDelete('cascade');


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employee_secondary_details');
	}

}
