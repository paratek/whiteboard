@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					You are logged in!
				</div>
			</div>
		</div>
	</div>
	{{ dump(Excel::load('storage/app/subjects.csv')->get()) }}
	{{ dump(Excel::load('storage/app/modules.csv')->get()) }}
	{{ dump($class) }}
	{{ dump(Auth::user()->load('classp','roles')->toArray()) }}
</div>
@endsection
