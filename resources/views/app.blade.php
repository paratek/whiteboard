<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/datatables.bootstrap.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<!--link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'-->
	<link href="{{ asset('/css/roboto.css') }}" rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	@yield('head')
</head>
<?php
	$guest = Auth::guest();
	if(!$guest)
		$roles = Auth::user()->roles->lists('name')->toArray();
?>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">WhiteBoard</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				<ul class="nav navbar-nav">
					<li class={{ active_class(if_uri(['home']))}}><a href="{{ url('/home') }}">Home</a></li>
					<li class={{ active_class(if_uri(['event*']))}}><a href="{{ url('/event') }}">Events</a></li>
				@if (!$guest)
					@if(in_array('admin', $roles) || in_array('fact', $roles) || in_array('hod', $roles))
					<li class={{ active_class(if_uri_pattern(['lecture*']))}}><a href="{{ url('/lecture') }}">Lecture</a></li>
					@elseif(in_array('admin', $roles) || in_array('stud', $roles) || in_array('hod', $roles))
					<li class={{ active_class(if_uri_pattern(['attendance*']))}}><a href="{{ url('/attendance/analytics') }}">Attendance</a></li>
					@endif
				@endif
			</ul>

				<ul class="nav navbar-nav navbar-right">
					@if ($guest)
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu" style="width:100%;">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@if (if_uri_pattern(['lecture*']))
	<nav class="navbar navbar-default sec-nav">
		<div class="container-fluid">

				@if (!$guest)
				<ul class="nav navbar-nav">
					<li class={{ active_class(if_uri(['lecture']))}}><a href="{{ url('/lecture') }}">Index</a></li>
					<li class={{ active_class(if_uri(['lecture/create']))}}><a href="{{ url('/lecture/create') }}">New Lecture</a></li>
					<li class={{ active_class(if_uri(['lecture/analytics']))}}><a href="{{ url('/lecture/analytics') }}">Analytics</a></li>
					@if(in_array('admin', $roles) || in_array('hod', $roles))
					<li class={{ active_class(if_uri(['lecture/reports']))}}><a href="{{ url('/lecture/reports') }}">Reports</a></li>
					<li class={{ active_class(if_uri(['lecture/subject']))}}><a href="{{ url('/lecture/subject') }}">Allocated Subject</a></li>
					<li class={{ active_class(if_uri(['lecture/subject/new']))}}><a href="{{ url('/lecture/subject/new') }}">Assign Subject</a></li>

					@endif
				</ul>
				@endif
		</div>
	</nav>
	@elseif (if_uri(['home']))
	@endif



	@yield('content')

	@include('footer')

	<!-- Scripts -->
	<script src="{{ asset('/js/jquery.min.js') }}"></script>

	<!-- DataTables -->

	<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
	<!--script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script-->
	<!--script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script-->

	<!-- App scripts -->
	@stack('scripts')

</body>
</html>
