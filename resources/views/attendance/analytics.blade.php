@extends('app')

@section('content')
<div class="container">
  <div class="row">
    <div class="center-block">
      <div id="lectures_div"></div>
      @calendarchart('Lectures', 'lectures_div')
      <hr></hr>
      <h4><p class="text-center">Lectures Conducted (Subject-wise)</p></h4>
      <div id="pie_sub"></div>
      @piechart('SubTotLectures', 'pie_sub')
      <hr></hr>
      <h4><p class="text-center">Average Attendance (Subject-wise)</p></h4>
      <div id="pie_atts"></div>
      @gaugechart('AvgAttSub', 'pie_atts')
      <hr></hr>
      <h4><p class="text-center">Average Attendance (Month-wise)</p></h4>
      <div id="avg_attc"></div>
      @gaugechart('AvgAttClass', 'avg_attc')
      <hr></hr>
    </div>
  </div>
</div>
@endsection
