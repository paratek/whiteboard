@extends('app')

@section('content')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="{{ asset('/js/multiselect.min.js') }}"></script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Attendance</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					@if (session('success'))
						<div class="alert alert-success">
							<strong>{{session('success')}}</strong>
						</div>
					@endif

          @yield('php-attendance')

					<form class="form-horizontal" role="form" method="POST" action="{{ $form_action }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="_method" value="{{ $method }}">
						<input type="hidden" name="fl" value="{{ $fl }}">
						<div class="form-group">
							<div class="row">
							  <div class="col-md-4 col-xs-4 col-md-offset-1 col-xs-offset-1">
									<p><b>Present</b></p>
							    <select name="present[]" id="multiselect" class="form-control" size="25" multiple="multiple">
										@foreach($users_p as $user)
											@if($user->roll_no!=0)
											<option value="{{$user->id}}">{{$user->roll_no}} - {{$user->user->name}}</option>
											@endif
										@endforeach
							    </select>
							  </div>
							  <div class="col-xs-2">
									<button type="button" id="multiselect_undo" class="btn btn-primary btn-block">undo</button>
							    <button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
							    <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
							    <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
							    <button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
									<button type="button" id="multiselect_redo" class="btn btn-warning btn-block">redo</button>
								</div>
							  <div class="col-md-4 col-xs-4 ">
									<p><b>Absent</b></p>
							    <select name="absent[]" id="multiselect_to" class="form-control" size="25" multiple="multiple">
										@if(isset($users_a))
										@foreach($users_a as $user)
											@if($user->roll_no!=0)
											<option value="{{$user->id}}">{{$user->roll_no}} - {{$user->user->name}}</option>
											@endif
										@endforeach
										@endif
							    </select>
							  </div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-5">
								<button type="submit" class="btn btn-primary">
									{{ $btn_name }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#multiselect').multiselect({
		submitAllLeft: true,
		submitAllRight: true,
		keepRenderingSort: true,
		search: {
			left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
			right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
		}
	});
});
</script>
@endsection
