@extends('app')

@section('content')
<link href="{{ asset('/css/data_table_custom.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading">Labs/Lectures Conducted</div>
				<div class="panel-body">
					<div class="container-fluid">
		        <!-- Table -->
		        <table id="classForumTable" class="table">
							<thead>
								<tr>
									<th>User</th>
									<th>Message</th>
									<th>Date</th>
									<th></th>
								</tr>
							</thead>
		        </table>
		    </div>
			</div>

			<div class="panel-footer">
			  <div class="input-group">
					​<textarea id="btn-input" class="form-control input-sm" rows="3" placeholder="Type your message here..."></textarea>
					<span class="input-group-btn">
			        <button class="btn btn-warning btn-sm" id="btn-chat">Send</button>
			    </span>
			  </div>
			</div>
		</div>
	</div>
</div>

@endsection




@section('scripts')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables.bootstrap.js') }}"></script>
<script src="{{ asset('/js/handlebars.js') }}"></script>

<script id="details-template" type="text/x-handlebars-template">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Lecture's Attendance</div>
			<div class="panel-body">
				<button class="btn btn-primary" type="button">
				  <span class="badge">@{{module_name}}</span>  @{{module_desc}}
				</button>
				<br>
				<br>
				<button class="btn btn-success" type="button">
				  Present <span id="total-present-@{{id}}" value="@{{present}}" class="badge">@{{present}}</span>
				</button>
				<button class="btn btn-warning" type="button">
				  Absent <span id="total-absent-@{{id}}" value="@{{absent}}" class="badge">@{{absent}}</span>
				</button>
			</div>
	    <table class="table table-striped details-table" id="attendance-@{{id}}">
	        <thead>
	        <tr>
	            <th>Roll No</th>
	            <th>Name</th>
							<th>Present<th>
	        </tr>
	        </thead>
	    </table>
		</div>
	</div>
</script>
<script>
function changeAttendance(id, parent) {
	console.log(id);
	var present = $('#present-'+id).attr('is_present');
	$('#present-'+id).parents('tr').toggleClass('info');
	var pres = $('#total-present-'+parent).attr('value');
	var abs = $('#total-absent-'+parent).attr('value');
	console.log("Pres = " + pres + ", Abs = " + abs);
	if(present==0)
	{
		console.log(id + " " + present + " -> Present");
		$('#present-'+id).html('<span class="glyphicon glyphicon-ok gi-2x" aria-hidden="true" style="color:green"></span>');
		$('#present-'+id).attr('is_present', 1);
		pres++;
		abs--;
		present=1;
	}
	else
	{
			$('#present-'+id).html('<span class="glyphicon glyphicon-remove gi-2x" aria-hidden="true" style="color:red"></span>');
			$('#present-'+id).attr('is_present', 0);
			pres--;
			abs++;
			present=0;
	}
	$('#total-present-'+parent).attr('value', pres);
	$('#total-absent-'+parent).attr('value', abs);
	$('#total-present-'+parent).text(pres);
	$('#total-absent-'+parent).text(abs);
	$.ajax({
			 url: '/attendance/' + id +'/edit',
			 data: { "_token": "{{ csrf_token() }}" },
			 type: 'GET',
			 success: function(json){
							//perform operation
					$('#present-'+id).parents('tr').toggleClass('info');
					$('#present-'+id).parent('tr').toggleClass('success');
			 },
			 error: function(json) {
				 	console.log("hi");
	 				console.log("json");
					$('#present-'+id).parents('tr').toggleClass('info')
				 	//$('#lectureTable').DataTable().ajax.reload();
					if(present===0)
					{
							$('#present-'+id).html('<span class="glyphicon glyphicon-ok gi-2x" aria-hidden="true" style="color:green"></span>');
							$('#present-'+id).attr('is_present', 1);
							pres++;
							abs--;
					}
					else
					{
							$('#present-'+id).html('<span class="glyphicon glyphicon-remove gi-2x" aria-hidden="true"></span>');
							$('#present-'+id).attr('is_present', 0);
							pres--;
							abs++;
					}
					$('#total-present-'+parent).attr('value', pres);
					$('#total-absent-'+parent).attr('value', abs);
					$('#total-present-'+parent).text(pres);
					$('#total-absent-'+parent).text(abs);
			 }
	 });
}
</script>
<script>
$(document).ready(function() {
	var template = Handlebars.compile($("#details-template").html());
	var table = $('#classForumTable').DataTable({
					ajax: '/dt/forum/class/',
					columns: [
						{data: 'user_name', name: 'user', orderable: false },
						{data: 'message', name: 'message',  orderable: false },
						{data: 'created_at', name: 'date'},
						{data: 'options', name: 'options', orderable: false, searchable: false }
					],
        "order": [[1, 'desc']],
			});


	 $('#lectureTable tbody').on( 'click', '[id^=delete-lecture]', function () {
		var row = table.row( $(this).parents('tr') );
		console.log($(this))
		console.log($(this).closest('tr'))
		$(this).closest('tr').addClass('danger');

		//row.remove().draw();
		$.ajax({
				 url: '/lecture/' + $(this).val(),
				 data: { "_token": "{{ csrf_token() }}" },
				 type: 'DELETE',
				 success: function(json){
						row.remove().draw();
				 },
				 error: function() {
			 			$(this).closest('tr').addClass('danger');
				 }
		 });

			 console.log($(this).val());
	 } );


});
</script>
@endsection
