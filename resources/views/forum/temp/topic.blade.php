@extends('app')

@section('head')
	<!-- Font
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	-->
	<style>
	div[contenteditable="true"] {
		cursor: text;
    background: rgb(255, 255, 255);
    padding: 20px;
    border: white;
    border-width: 200px;
    outline: rgb(221, 221, 221) auto 5px;
}
	</style>
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="/css/bootstrap.min.css">

	<!-- Bootstrap Validator -->
	<link href="/css/bootstrapValidator.min.css" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="/css/font-awesome.min.css" rel="stylesheet">

	<!-- LightBox -->
	<link href="/css/lightbox.css" rel="stylesheet">

	<!-- eeBootstrap -->
	<link href="/css/eeBootstrap.css" rel="stylesheet">

	<!-- Custom -->
	<link href="/css/custom.css" rel="stylesheet">

	<link href="/vendor/alloy-editor/assets/alloy-editor-ocean-min.css" rel="stylesheet">

  <script src="/vendor/alloy-editor/alloy-editor-all-min.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
	<!--

	function navHover(which)
	{
		if (document.getElementById(which.id))
		{
			cur = document.getElementById(which.id).className;

			if (cur == 'buttonLarge')
			{
				document.getElementById(which.id).className = 'buttonLargeHover'
			}
			else if (cur == 'buttonSmall')
			{
				document.getElementById(which.id).className = 'buttonSmallHover'
			}
		}
	}

	function navReset(which)
	{
		if (document.getElementById(which.id))
		{
			cur = document.getElementById(which.id).className;

			if (cur == 'buttonLargeHover')
			{
				document.getElementById(which.id).className = 'buttonLarge'
			}
			else if (cur == 'buttonSmallHover')
			{
				document.getElementById(which.id).className = 'buttonSmall'
			}
		}
	}


	function navJump(where)
	{
		window.location=where;
	}
	-->
	</script>

	<script type="text/javascript">
<!--

function showimage(loc, width, height)
{
	window.open(loc,'Image','width='+width+',height='+height+',screenX=0,screenY=0,top=0,left=0,toolbar=0,status=0,scrollbars=0,location=0,menubar=1,resizable=1');
	return false;
}

function showfastreply()
{
	if (document.getElementById('fastreply').style.display == "block")
	{
		document.getElementById('fastreply').style.display = "none";
	}
	else
	{
		document.getElementById('fastreply').style.display = "block";
		document.getElementById('fastreply_text').focus();
	}
}

function showHideRow(el)
{
	if (document.getElementById(el).style.display == "")
	{
		document.getElementById(el).style.display = "none";
	}
	else
	{
		document.getElementById(el).style.display = "";
	}
}
//-->
</script>

@endsection

@section('content')

<div class="container"> <!-- Start page content -->
		<div class="clearfix">&nbsp;</div>
		<div class="row">
			<div class="col-md-12 hidden-xs">
				<ol class="breadcrumb">
					<li><a href="/forum" title="Forum">Forum</a></li>
					<li><a href="/forum/{{ $forum->id }}/topic" title="{{$forum->name}}">{{ $forum->name }}</a></li>
					<li><a href="/forum/{{$forum->id}}/topic/{{ $topic->id }}" title="{{$topic->title}}">{{ $topic->title }}</a></li>
				</ol>
			</div>
		</div>

		<script type="text/javascript">
		<!--

		function showHideRow(el)
		{
			if (document.getElementById(el).style.display == "")
			{
				document.getElementById(el).style.display = "none";
			}
			else
			{
				document.getElementById(el).style.display = "";
			}
		}
		//-->
		</script>

		<div class="row">
			<div class="col-md-12 center-block">
				<div class="row">
					<div class="col-md-12">
						<p class="text-muted">Announcements</p>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-info topic-marker">
									<i class="fa fa-bullhorn fa-lg"></i>
									<a href="announcement" title="announcement"> WhiteBoard deployed in TCET</a>
									<small class="text-muted">&nbsp;&nbsp;Posted: 04-05-2015 10:10&nbsp;&nbsp;by&nbsp;WhiteBoard Team</small>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">

			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="row">
			<div class="col-md-9 col-sm-9 col-xs-9">
				<h4 class="text-truncated">{{ $topic->title }}</h4>
			</div>
		</div>
		<div class="clearfix top-border">&nbsp;</div>

		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<button type="button" class="btn btn-primary btn-sm" id="pr1" onclick="navJump('/forum/{{$forum->id}}/topic/create')" onmouseover="navHover(this);" onmouseout="navReset(this);">New Topic</button>
				</div>
			</div>
		</div>

		<div class="clearfix">&nbsp;</div>

		@foreach($posts as $post)
		<div class="panel panel-default" id="a9">
			<div class="panel-heading">
				<div class="panel-title">{{ $post->author }}
					<div class="pull-right">
						<div class="text-muted"><span class="hidden-md hidden-lg"><i class="fa fa-calendar"></i> </span><small><span class="hidden-sm hidden-xs">Posted:</span> {{ $post->created_at }}</small></div>
					</div>
				</div>
			</div>
			<div class="panel-body thread-row">
				<div class="row thread-row">
					<div class="col-md-2 col-sm-3 hidden-xs text-center userblock">
						<div class="clerafix">&nbsp;</div>
						<p class="h1">{{ $post->user->name[0]}}</p>
						<div class="push_bottom_5"></div>
						<div class="label label-default">{{ $post->user->roles[0]->display_name }}</div>
						<div class="push_bottom_5"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
						<div class="text-muted text-left"><small>Total Posts:&nbsp; 96</small></div>
						<div class="clerafix">&nbsp;</div>
					</div>
					<div class="col-md-10 col-sm-9 col-xs-12">
						<div class="clerafix">&nbsp;</div>
						<div class="content_body"><p>{!! $post->message !!}</p></div>
					</div>
				</div>

			</div>
		</div>
		@endforeach

		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<button type="button" class="btn btn-primary btn-sm" id="fr" onclick="showfastreply();return false;" onmouseover="navHover(this);" onmouseout="navReset(this);">Fast Reply</button>
					<button type="button" class="btn btn-primary btn-sm" id="pr1" onclick="navJump('/forum/{{$forum->id}}/topic/create')" onmouseover="navHover(this);" onmouseout="navReset(this);">New Topic</button>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="clearfix">&nbsp;</div>
				<div id="fastreply" style="display: none; padding: 0px;">
					<form id="submit_post" name="submit_post" method="POST" action="/forum/{{$forum->id}}/topic/{{$topic->id}}/post">
						<div class="hiddenFields">
							<input type="hidden" name="ACT" value="23">
							<input type="hidden" name="FROM" value="forum">
							<input type="hidden" name="mbase" value="http://www.eebootstrap.com/forum/member/">
							<input type="hidden" name="board_id" value="1">
							<input type="hidden" name="RET" value="http://www.eebootstrap.com/forum/viewthread/75/">
							<input type="hidden" name="topic_id" value="75">
							<input type="hidden" name="forum_id" value="6">
							<input type="hidden" name="smileys" value="y">
							<input type="hidden" name="site_id" value="1">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						</div>


						<div class="form-group">
							<label for="body">Fast Reply</label>
							<div class="clearfix">&nbsp;</div>
							<textarea name="message" class="form-control" id="fastreply_text" rows="12" contenteditable="true" data-placeholder="Enter your message here">Enter your message here</textarea>
						</div>
						<input type="submit" name="submit" class="btn btn-primary btn-sm" value="Submit Post">
					</form>
				</div>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>

		{!! $posts->render() !!}

		<div class="clearfix">&nbsp;</div>
	</div><!-- End page content -->
	<div id="footer" class="navbar-default">
		<div class="container">
			<div class="row">&nbsp;</div>
			<div class="row">
				<div class="col-md-5 hidden-xs">
					<p class="text-muted text-center">
						<small><a href="whiteboard.co/">WhiteBoard</a> - version 1.0.0<br>
							<!-- Script Executed in 0.1788 seconds <br /> -->
							<a href="http://laravel.com/"><em>Powered by Laravel</em></a></small>
						</p>
					</div>

					<div class="col-md-2">

						<p class="text-center">
							<a class="pop-tooltip" href="/rss/2/" data-toggle="tooltip" title="" data-original-title="RSS 2.0"><i class="fa fa-rss-square fa-2x text-info"></i></a>&nbsp;&nbsp;&nbsp;
							<a class="pop-tooltip" href="/atom/2/" data-toggle="tooltip" title="" data-original-title="Atom Feed"><i class="fa fa-rss-square fa-2x text-warning"></i></a>
						</p>

					</div>
					<div class="col-md-5">
						<p class="text-muted text-center">
							<small>WhiteBoard 1.0</small><br>
							<small>developed &amp; designed by&nbsp;&nbsp;<a href="http://www.paratek.co"><em>Paratek</em></a>.</small><br>
						</p>
					</div>
				</div>

			</div>

			<a href="#" class="btn back-to-top btn-light"> <i class="fa fa-chevron-up fa-lg"></i> </a>

		</div>

		<script>
	  var editor = AlloyEditor.editable('fastreply_text');
	  </script>

		<!-- Jquery -->
		<script src="/js/jquery-1.11.1.min.js"></script>

		<!-- Bootstrap-->
		<script src="/js/bootstrap.min.js"></script>

		<!-- Jquery Cookie -->
		<script src="/js/jquery.cookie-1.4.0.js"></script>

		<!-- Bootstrap Validator -->
		<script src="/js/bootstrapValidator.min.js"></script>

		<!-- Bootstrap Filestyle  -->
		<script src="/js/bootstrap-filestyle.min.js"></script>

		<!-- Lightbox  -->
		<script src="/js/lightbox.min.js"></script>

		<!-- Jquery scrollToTop -->
		<script src="/js/jquery-scrollToTop.min"></script>

		<!--Style Switcher -->
		<script src="/js/jquery.style.switcher.js"></script>

		<!-- eeBootstrap -->
		<script src="/js/eeBootstrap.js"></script>

		<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src=""><div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div></body></html>

@endsection
