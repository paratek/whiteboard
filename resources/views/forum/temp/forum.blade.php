@extends('app')

@section('head')

<link href="{{ asset('/css/pt_sans.css') }}" rel='stylesheet' type='text/css'>


<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="/css/bootstrap.min.css">

<!-- Bootstrap Validator -->
<link href="/css/bootstrapValidator.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- LightBox -->
<link href="/css/lightbox.css" rel="stylesheet">

<!-- eeBootstrap -->
<link href="/css/eeBootstrap.css" rel="stylesheet">

<!-- Custom -->
<link href="/css/custom.css" rel="stylesheet">

<script type="text/javascript">
<!--

function navHover(which)
{
	if (document.getElementById(which.id))
	{
		cur = document.getElementById(which.id).className;

		if (cur == 'buttonLarge')
		{
			document.getElementById(which.id).className = 'buttonLargeHover'
		}
		else if (cur == 'buttonSmall')
		{
			document.getElementById(which.id).className = 'buttonSmallHover'
		}
	}
}

function navReset(which)
{
	if (document.getElementById(which.id))
	{
		cur = document.getElementById(which.id).className;

		if (cur == 'buttonLargeHover')
		{
			document.getElementById(which.id).className = 'buttonLarge'
		}
		else if (cur == 'buttonSmallHover')
		{
			document.getElementById(which.id).className = 'buttonSmall'
		}
	}
}

function navJump(where)
{
	window.location=where;
}
-->
</script>

<script type="text/javascript">
<!--

function showimage(loc, width, height)
{
	window.open(loc,'Image','width='+width+',height='+height+',screenX=0,screenY=0,top=0,left=0,toolbar=0,status=0,scrollbars=0,location=0,menubar=1,resizable=1');
	return false;
}

function showfastreply()
{
	if (document.getElementById('fastreply').style.display == "block")
	{
		document.getElementById('fastreply').style.display = "none";
	}
	else
	{
		document.getElementById('fastreply').style.display = "block";
		document.getElementById('body').focus();
	}
}

function showHideRow(el)
{
	if (document.getElementById(el).style.display == "")
	{
		document.getElementById(el).style.display = "none";
	}
	else
	{
		document.getElementById(el).style.display = "";
	}
}
//-->
</script>

@endsection

@section('content')

<div class="container"> <!-- Start page content -->
	<div class="clearfix">&nbsp;</div>
	<div class="row">
		<div class="col-md-12 hidden-xs">
			<ol class="breadcrumb"><li><a href="/forum" title="Forum">Forum</a></li><li><a href="/forum/{{$forum->id}}/topic" title="{{$forum->name}}">{{$forum->name}}</a></li></ol>
		</div>
	</div>

	<script type="text/javascript">
	<!--

	function showHideRow(el)
	{
		if (document.getElementById(el).style.display == "")
		{
			document.getElementById(el).style.display = "none";
		}
		else
		{
			document.getElementById(el).style.display = "";
		}
	}
	//-->
	</script>

	<div class="row">
		<div class="col-md-12 center-block">
			<div class="row">
				<div class="col-md-12">
					<p class="text-muted">Announcements</p>
				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-info topic-marker">
								<i class="fa fa-bullhorn fa-lg"></i>
								<a href="announcement" title="announcement"> WhiteBoard deployed in TCET</a>
								<small class="text-muted">&nbsp;&nbsp;Posted: 04-05-2015 10:10&nbsp;&nbsp;by&nbsp;WhiteBoard Team</small>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! $topics->render() !!}
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<button type="button" class="btn btn-primary btn-sm" id="nt2" onclick="navJump('/forum/create')" onmouseover="navHover(this);" onmouseout="navReset(this);">New Forum</button>
				<button type="button" class="btn btn-primary btn-sm" id="nt2" onclick="navJump('/forum/{{$forum->id}}/topic/create')" onmouseover="navHover(this);" onmouseout="navReset(this);">New Topic</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		</div>
	</div>
	&nbsp;
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Topics</div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Topic Title</th>
							<th class="text-right">Replies</th>
							<th class="text-right">Views</th>
							<th class="hidden-xs">Latest Post Info</th>
						</tr>
					</thead>
					<tbody>
						@foreach($topics as $topic)
						<tr id="a1" class="">
							<td class="col-md-6">
								<div>
									<span class="PostPrefix">
										<span class="PostPrefixView"></span>
										<a href="/forum/{{$forum->id}}/topic/{{$topic->id}}"><span class="PostPrefixTitle">{{ $topic['title'] }}&nbsp;</span></a>
									</span>
								</div>
								<small class="text-muted">Author: <a href="/user/{{$topic->user_id}}/">{{ $topic['author'] }}</a></small>
							</td>
							<td class="col-md-1 text-right"><span class="badge">{{$topic->count}}</span></td>
							<td class="col-md-1 text-right"><span class="badge">{{$topic->views}}</span></td>
							<td class="col-md-4 hidden-xs">
								<div>
									Posted: {{ Carbon\Carbon::parse($topic->created_at)->diffForHumans() }}<br>
									<small class="text-muted">Author: <a href="/user/{{$topic->last_post->user_id}}/">{{$topic->last_post->author}}</a></small>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<button type="button" class="btn btn-primary btn-sm" id="nt2" onclick="navJump('/forum/create')" onmouseover="navHover(this);" onmouseout="navReset(this);">New Forum</button>
				<button type="button" class="btn btn-primary btn-sm" id="nt3" onclick="navJump('/forum/{{$forum->id}}/topic/create')" onmouseover="navHover(this);" onmouseout="navReset(this);">New Topic</button>
			</div>
		</div>
	</div>

	{!! $topics->render() !!}

</div><!-- End page content -->

<div id="footer" class="navbar-default">
	<div class="container">
		<div class="row">&nbsp;</div>
		<div class="row">
			<div class="col-md-5 hidden-xs">
				<p class="text-muted text-center">
					<small><a href="whiteboard.co/">WhiteBoard</a> - version 1.0.0<br>
						<!-- Script Executed in 0.1788 seconds <br /> -->
						<a href="http://laravel.com/"><em>Powered by Laravel</em></a></small>
					</p>
				</div>

				<div class="col-md-2">

					<p class="text-center">
						<a class="pop-tooltip" href="/rss/2/" data-toggle="tooltip" title="" data-original-title="RSS 2.0"><i class="fa fa-rss-square fa-2x text-info"></i></a>&nbsp;&nbsp;&nbsp;
						<a class="pop-tooltip" href="/atom/2/" data-toggle="tooltip" title="" data-original-title="Atom Feed"><i class="fa fa-rss-square fa-2x text-warning"></i></a>
					</p>

				</div>
				<div class="col-md-5">
					<p class="text-muted text-center">
						<small>WhiteBoard 1.0</small><br>
						<small>developed &amp; designed by&nbsp;&nbsp;<a href="http://www.paratek.co"><em>Paratek</em></a>.</small><br>
					</p>
				</div>
			</div>

		</div>

		<a href="#" class="btn back-to-top btn-light"> <i class="fa fa-chevron-up fa-lg"></i> </a>

	</div>

	<!-- Jquery -->
	<script src="/js/jquery-1.11.1.min.js"></script>

	<!-- Bootstrap-->
	<script src="/js/bootstrap.min.js"></script>

	<!-- Jquery Cookie -->
	<script src="/js/jquery.cookie-1.4.0.js"></script>

	<!-- Bootstrap Validator -->
	<script src="/js/bootstrapValidator.min.js"></script>

	<!-- Bootstrap Filestyle  -->
	<script src="/js/bootstrap-filestyle.min.js"></script>

	<!-- Lightbox  -->
	<script src="/js/lightbox.min.js"></script>

	<!-- Jquery scrollToTop -->
	<script src="/js/jquery-scrollToTop.min"></script>

	<!--Style Switcher -->
	<script src="/js/jquery.style.switcher.js"></script>

	<!-- eeBootstrap -->
	<script src="/js/eeBootstrap.js"></script>

	<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src=""><div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div></body></html>

@endsection
