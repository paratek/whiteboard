@extends('app')

@section('head')

<!-- Font
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
-->

<!-- Bootstrap core CSS -->
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>


<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="/css/bootstrap.min.css">

<!-- Bootstrap Validator -->
<link href="/css/bootstrapValidator.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- LightBox -->
<link href="/css/lightbox.css" rel="stylesheet">

<!-- eeBootstrap -->
<link href="/css/eeBootstrap.css" rel="stylesheet">

<!-- Custom -->
<link href="/css/custom.css" rel="stylesheet">

<script type="text/javascript">
<!--

function navHover(which)
{
  if (document.getElementById(which.id))
  {
    cur = document.getElementById(which.id).className;

    if (cur == 'buttonLarge')
    {
      document.getElementById(which.id).className = 'buttonLargeHover'
    }
    else if (cur == 'buttonSmall')
    {
      document.getElementById(which.id).className = 'buttonSmallHover'
    }
  }
}

function navReset(which)
{
  if (document.getElementById(which.id))
  {
    cur = document.getElementById(which.id).className;

    if (cur == 'buttonLargeHover')
    {
      document.getElementById(which.id).className = 'buttonLarge'
    }
    else if (cur == 'buttonSmallHover')
    {
      document.getElementById(which.id).className = 'buttonSmall'
    }
  }
}


function navJump(where)
{
  window.location=where;
}
-->
</script>
<script type="text/javascript">
<!--

function show_element(which)
{
  document.getElementById(which + 'on').style.display = "block";
  document.getElementById(which + 'off').style.display = "none";
  set_cookie(which, 'on');
}


function hide_element(which)
{
  document.getElementById(which + 'on').style.display = "none";
  document.getElementById(which + 'off').style.display = "block";
  set_cookie(which, 'off');
}

function set_cookie(which, state)
{
  document.cookie = "exp_state" + which + "=" + state + "; path=" + "/" + ';';
}

function fetch_cookie(which)
{
  thecookie = "exp_state" + which + '=';
  str_pos  = document.cookie.indexOf(thecookie);
  cc = false;

  if (str_pos != -1)
  {
    cstart = str_pos + thecookie.length;
    cend   = document.cookie.indexOf(";", cstart);

    if (cend == -1)
    cend = document.cookie.length;

    cc = unescape(document.cookie.substring(cstart, cend));
  }

  return cc;
}

function set_display()
{
  var el = new Array();
  el[0] = "forum1";el[1] = "forum4";el[2] = "forumstats";

  for (i = 0 ; i < el.length; i++ )
  {
    if (fetch_cookie(el[i]) != false)
    {
      if (fetch_cookie(el[i]) == 'on')
      {
        show_element(el[i]);
      }
      else
      {
        hide_element(el[i]);
      }
    }
  }
}

-->
</script>

@endsection

@section('content')
<div class="container"> <!-- Start page content -->
  <div class="clearfix">&nbsp;</div>
  <div class="row">
    <div class="col-md-12 hidden-xs">
      <ol class="breadcrumb"><li class="active">Forum Home</li></ol>
    </div>
  </div>
  <div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<button type="button" class="btn btn-primary btn-sm" id="nt2" onclick="navJump('/forum/create')" onmouseover="navHover(this);" onmouseout="navReset(this);">New Forum</button>
			</div>
		</div>
	</div>
  <div class="clearfix">&nbsp;</div>
  <div class="panel-group" id="accordion_forum1">
    <div class="panel panel-default">
      <a class="panel-default" data-toggle="collapse" data-parent="#accordion_forum1" href="#forum1" aria-expanded="true">
        <div class="panel-heading">
          <i class="fa fa-fw fa-folder-open-o"></i> <strong>General</strong> <span class="pull-right"><i class="fa fa-compress"></i></span>
        </div>
      </a>
      <div id="forum1" class="panel-collapse collapse in" aria-expanded="true">

        <table class="table table-hover">
          <thead>
            <tr>
              <th>Forum Name</th>
              <th class="text-right">Topics</th>
              <th class="text-right">Replies</th>
              <th class="hidden-xs">Latest Post Info</th>
            </tr>
          </thead>
          <tbody>
            @foreach($forums as $forum)
            @if($forum->type=='0')
            <tr>
              <td class="col-md-6 col-sm-6">
                <div><a href="/forum/{{$forum->id}}/topic" title="{{$forum->name}}"><strong>{{$forum->name}}</strong></a></div>
                <div class="hidden-xs"><em>{{$forum->desc}}</em></div>
              </td>
              <td class="col-md-1 col-sm-1 text-right"><span class="badge">{{$forum->topics}}</span></td>
              <td class="col-md-1 col-sm-1 text-right"><span class="badge">{{$forum->replies}}</span></td>
              <td class="col-md-4 col-sm-4 hidden-xs">
                <div>
                  <a href="/forum/{{$forum->id}}/topic/{{$forum->last_topic->id}}">{{ $forum->last_topic->title }}</a><br>
                  <i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($forum->last_topic->updated_at)->diffForHumans() }}<br>
                  <i class="fa fa-user"></i> <a href="/user/{{$forum->last_topic->user_id}}">{{\App\User::find($forum->last_topic->user_id)->name}}</a>
                </div>
              </td>
            </tr>
            @endif
            @endforeach
          </tbody>
          <tbody>
          </tbody></table>
        </div>
      </div>
    </div><!-- General -->
    <div class="panel-group" id="accordion_forum4">
      <div class="panel panel-default">
        <a class="panel-default" data-toggle="collapse" data-parent="#accordion_forum4" href="#forum4">
          <div class="panel-heading">
            <i class="fa fa-folder-open-o fa-fw"></i> <strong>Department</strong> <span class="pull-right"><i class="fa fa-compress"></i></span>
          </div>
        </a>
        <div id="forum4" class="panel-collapse collapse in">

          <div class="panel-body">
            <div class="hidden-xs"><em>Forums of department</em></div>
          </div>


          <table class="table table-hover">
            <thead>
              <tr>


                <th>Forum Name</th>
                <th class="text-right">Topics</th>
                <th class="text-right">Replies</th>
                <th class="hidden-xs">Latest Post Info</th>
              </tr>
            </thead>
            <tbody>
              @foreach($forums as $forum)
              @if($forum->type==1)
              <tr>
                <td class="col-md-6 col-sm-6">
                  <div><a href="/forum/{{$forum->id}}/topic" title="{{$forum->name}}"><strong>{{$forum->name}}</strong></a></div>
                  <div class="hidden-xs"><em>{{$forum->desc}}</em></div>
                </td>
                <td class="col-md-1 col-sm-1 text-right"><span class="badge">{{$forum->topics}}</span></td>
                <td class="col-md-1 col-sm-1 text-right"><span class="badge">{{$forum->replies}}</span></td>
                <td class="col-md-4 col-sm-4 hidden-xs">
                  <div>
                    <a href="/forum/{{$forum->id}}/topic/{{$forum->last_topic->id}}">{{ $forum->last_topic->title }}</a><br>
                    <i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($forum->last_topic->updated_at)->diffForHumans() }}<br>
                    <i class="fa fa-user"></i> <a href="/user/{{$forum->last_topic->user_id}}">{{\App\User::find($forum->last_topic->user_id)->name}}</a>
                  </div>
                </td>
              </tr>
              @endif
              @endforeach

            </tbody></table>
          </div>
        </div>
      </div>

      <div class="panel-group" id="accordion_forum4">
        <div class="panel panel-default">
          <a class="panel-default" data-toggle="collapse" data-parent="#accordion_forum4" href="#forum4">
            <div class="panel-heading">
              <i class="fa fa-folder-open-o fa-fw"></i> <strong>Class</strong> <span class="pull-right"><i class="fa fa-compress"></i></span>
            </div>
          </a>
          <div id="forum4" class="panel-collapse collapse in">

            <div class="panel-body">
              <div class="hidden-xs"><em>Forums of class</em></div>
            </div>


            <table class="table table-hover">
              <thead>
                <tr>


                  <th>Forum Name</th>
                  <th class="text-right">Topics</th>
                  <th class="text-right">Replies</th>
                  <th class="hidden-xs">Latest Post Info</th>
                </tr>
              </thead>
              <tbody>
                @foreach($forums as $forum)
                @if($forum->type==2)
                <tr>
                  <td class="col-md-6 col-sm-6">
                    <div><a href="/forum/{{$forum->id}}/topic" title="{{$forum->name}}"><strong>{{$forum->name}}</strong></a></div>
                    <div class="hidden-xs"><em>{{$forum->desc}}</em></div>
                  </td>
                  <td class="col-md-1 col-sm-1 text-right"><span class="badge">{{$forum->topics}}</span></td>
                  <td class="col-md-1 col-sm-1 text-right"><span class="badge">{{$forum->replies}}</span></td>
                  <td class="col-md-4 col-sm-4 hidden-xs">
                    <div>
                      <a href="/forum/{{$forum->id}}/topic/{{$forum->last_topic->id}}">{{ $forum->last_topic->title }}</a><br>
                      <i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($forum->last_topic->updated_at)->diffForHumans() }}<br>
                      <i class="fa fa-user"></i> <a href="/user/{{$forum->last_topic->user_id}}">{{\App\User::find($forum->last_topic->user_id)->name}}</a>
                    </div>
                  </td>
                </tr>
                @endif
                @endforeach

              </tbody></table>
            </div>
          </div>
        </div>

      <div class="panel-group" id="accordion_recent_topics">
        <div class="panel panel-default">
          <a class="panel-default" data-toggle="collapse" data-parent="#accordion_recent_topics" href="#recent_topics">
            <div class="panel-heading">
              <i class="fa fa-bullhorn"></i> Most Recent Topics <span class="pull-right"><i class="fa fa-compress"></i></span>
            </div>
          </a>
          <div id="recent_topics" class="panel-collapse collapse in">
            <table class="table table-hover table-striped">
              <thead>
                <tr>
                  <th class="col-md-6">Title</th>
                  <th class="col-md-2 text-right">Updated At</th>
                  <th class="hidden-xs col-md-4">Author</th>
                </tr>
              </thead>
              <tbody>
                @foreach($recent as $topic)
                <tr>
                  <td class="col-md-6"><a href="/forum/{{$topic->forum_id}}/topic/{{$topic->id}}">{{$topic->title}}</a></td>
                  <td class="col-md-1 text-right"><span class="badge">{{$topic->updated_at->format('d M')}}</span></td>
                  <td class="hidden-xs col-md-4"><a href="/user/{{$topic->user_id}}">{{\App\User::find($topic->user_id)->name}}</a></td>
                </tr>
                @endforeach
              </tbody><tbody>
              </tbody></table>
            </div>
          </div>
        </div>

        <div class="panel-group" id="accordion_most_popular_posts">
          <div class="panel panel-default">
            <a class="panel-default" data-toggle="collapse" data-parent="#accordion_most_popular_posts" href="#most_popular_posts">
              <div class="panel-heading">
                <i class="fa fa-trophy"></i> Most Popular Threads <span class="pull-right"><i class="fa fa-expand"></i></span>
              </div>
            </a>
            <div id="most_popular_posts" class="panel-collapse collapse in">
              <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th class="col-md-6">Title</th>
                    <th class="col-md-1 text-right">Replies</th>
                    <th class="col-md-1 text-right">Views</th>
                    <th class="hidden-xs col-md-4">Author</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($popular as $topic)
                  <tr>
                    <td class="col-md-6"><a href="/forum/{{$topic->forum_id}}/topic/{{$topic->id}}">{{$topic->title}}</a></td>
                    <td class="col-md-1 text-right"><span class="badge">{{$topic->count}}</span></td>
                    <td class="col-md-1 text-right"><span class="badge">{{$topic->views}}</span></td>
                    <td class="hidden-xs col-md-4"><a href="/user/{{$topic->user_id}}">{{\App\User::find($topic->user_id)->name}}</a></td>
                  </tr>
                  @endforeach

                </tbody><tbody>
                </tbody></table>
              </div>
            </div>

            <div class="clearfix">&nbsp;</div>
            <div class="row">
          		<div class="col-md-12">
          			<div class="pull-right">
          				<button type="button" class="btn btn-primary btn-sm" id="nt2" onclick="navJump('/forum/create')" onmouseover="navHover(this);" onmouseout="navReset(this);">New Forum</button>
          			</div>
          		</div>
          	</div>
            <div class="clearfix">&nbsp;</div>
          </div>
          <script type="text/javascript">
          <!--

          function navHover(which)
          {
            if (document.getElementById(which.id))
            {
              cur = document.getElementById(which.id).className;

              if (cur == 'buttonLarge')
              {
                document.getElementById(which.id).className = 'buttonLargeHover'
              }
              else if (cur == 'buttonSmall')
              {
                document.getElementById(which.id).className = 'buttonSmallHover'
              }
            }
          }

          function navReset(which)
          {
            if (document.getElementById(which.id))
            {
              cur = document.getElementById(which.id).className;

              if (cur == 'buttonLargeHover')
              {
                document.getElementById(which.id).className = 'buttonLarge'
              }
              else if (cur == 'buttonSmallHover')
              {
                document.getElementById(which.id).className = 'buttonSmall'
              }
            }
          }


          function navJump(where)
          {
            window.location=where;
          }
          -->
          </script>
        </div>
        <!-- End page content -->

        <div id="footer" class="navbar-default">
          <div class="container">
            <div class="row">&nbsp;</div>
            <div class="row">
              <div class="col-md-5 hidden-xs">
                <p class="text-muted text-center">
                  <small><a href="whiteboard.co/">WhiteBoard</a> - version 1.0.0<br>
                    <!-- Script Executed in 0.1788 seconds <br /> -->
                    <a href="http://laravel.com/"><em>Powered by Laravel</em></a></small>
                  </p>
                </div>

                <div class="col-md-2">

                  <p class="text-center">
                    <a class="pop-tooltip" href="/rss/2/" data-toggle="tooltip" title="" data-original-title="RSS 2.0"><i class="fa fa-rss-square fa-2x text-info"></i></a>&nbsp;&nbsp;&nbsp;
                    <a class="pop-tooltip" href="/atom/2/" data-toggle="tooltip" title="" data-original-title="Atom Feed"><i class="fa fa-rss-square fa-2x text-warning"></i></a>
                  </p>

                </div>
                <div class="col-md-5">
                  <p class="text-muted text-center">
                    <small>WhiteBoard 1.0</small><br>
                    <small>developed &amp; designed by&nbsp;&nbsp;<a href="http://www.paratek.co"><em>Paratek</em></a>.</small><br>
                  </p>
                </div>
              </div>

            </div>

            <a href="#" class="btn back-to-top btn-light"> <i class="fa fa-chevron-up fa-lg"></i> </a>

          </div>

          <!-- Jquery -->
          <script src="/js/jquery-1.11.1.min.js"></script>

          <!-- Bootstrap-->
          <script src="/js/bootstrap.min.js"></script>

          <!-- Jquery Cookie -->
          <script src="/js/jquery.cookie-1.4.0.js"></script>

          <!-- Bootstrap Validator -->
          <script src="/js/bootstrapValidator.min.js"></script>

          <!-- Bootstrap Filestyle  -->
          <script src="/js/bootstrap-filestyle.min.js"></script>

          <!-- Lightbox  -->
          <script src="/js/lightbox.min.js"></script>

          <!-- Jquery scrollToTop -->
          <script src="/js/jquery-scrollToTop.min"></script>

          <!--Style Switcher -->
          <script src="/js/jquery.style.switcher.js"></script>

          <!-- eeBootstrap -->
          <script src="/js/eeBootstrap.js"></script>

          <div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src=""><div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div></body></html>

        @endsection
