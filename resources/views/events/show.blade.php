@extends('app')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/content-tools/css/sandbox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/content-tools/css/content-tools.min.css') }}">
@endsection

@section('content')
<div class="container">
  <article class="article">
    <section class="[ article__content ]" data-name="main" data-editable>
      <h1>Event Name</h1>
      <blockquote>
          Event Description
      </blockquote>
      <img alt="Example of bad variable names" height="174" src="http://whiteboard.co/vendor/content-tools/images/image.png" width="600">
      <p class="article__caption">
          <b>Above:</b>&nbsp;Here you see a photo of the previous year's event.
      </p>
      <p>
          The names we devise for folders, files, variables, functions, classes and any other user definable construct will (for the most part) determine how easily someone else can read and understand what we have written.
      </p>
      <p>
          To avoid repetition in the remainder of this section we use the term variable to cover folders, files, variables, functions, and classes.
      </p>
      <iframe frameborder="0" height="300" src="https://www.youtube.com/embed/I9jSKFk5Zp0" width="400"></iframe>
      <h2>Be descriptive and concise</h2>
      <p>
          It’s the most obvious naming rule of all but it’s also perhaps the most difficult. In my younger years I would often use very descriptive names, I pulled the following doozy from the archives:
      </p>
    </section>
    <aside class="article__related">
        <div class="[ article__author ]  [ author ]" data-name="about" data-editable>

            <h3 class="author__about">About the speaker</h3>

            <img
                src="{{ asset('/vendor/content-tools/images/author-pic.jpg') }}"
                alt="Anthony Blackshaw"
                width="80"
                class="[ author__pic ]  [ align-right ]"
                >

            <p class="author__bio">
                Anthony Blackshaw is a co-founder of Getme, an
                employee owned company with a focus on web tech. He
                enjoys writing and talking about tech, especially
                code and the occasional Montecristo No.2s.
            </p>

        </div>
        <br />
        <div class="[ article__author ]  [ author ]" style="background:#CE489E">

            <h3 class="author__about">List of attendees</h3>
            <table class="ce-element ce-element--type-table">

              <tbody class="ce-element ce-element--type-table-section">
                <tr class="ce-element ce-element--type-table-row">
                  <td class="ce-element ce-element--type-table-cell">
                    <div class="ce-element ce-element--type-table-cell-text ce-element--focused">Rupesh Parab</div>
                  </td>
                </tr>
                <tr class="ce-element ce-element--type-table-row">
                  <td class="ce-element ce-element--type-table-cell">
                    <div class="ce-element ce-element--type-table-cell-text ce-element--focused">asdasdasd</div>
                  </td>
                </tr>
                <tr class="ce-element ce-element--type-table-row">
                  <td class="ce-element ce-element--type-table-cell">
                    <div class="ce-element ce-element--type-table-cell-text ce-element--focused">asdasdasdas</div>
                  </td>
                </tr>
                </tbody>
            </table>
            <p class="author__bio">
                <a href="https://google.com" style="color:#000000">Click here</a> for the list of all the attendees.
            </p>

        </div>
        <div class="[ article__learn-more ]  [ learn-more ]" data-name="learn-more" data-editable>

            <h3>Want to learn more?</h3>

            <p>
                If you'd like to learn more about the event,
                <a href="https://google.com"><u>click here.</u></a>
            </p>

        </div>
    </aside>
  </article>
  <script type="text/javascript" src="{{ asset('/vendor/content-tools/js/content-tools.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/vendor/content-tools/js/sandbox.js') }}"></script>
</div>
<script
@endsection
