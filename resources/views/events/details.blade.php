@extends('app')

@section('head')
<link href="{{ asset('/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/vendor/multiple-select/css/multiple-select.css') }}" rel="stylesheet" type="text/css">

@endsection

@yield('php-code')

@section('content')
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="{{ asset('/js/dropdown-plugin.js') }}"></script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{$panel_heading}}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					@if (session('success'))
						<div class="alert alert-success">
							<strong>{{session('success')}}</strong>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ $form_action }}">
						{{ method_field($form_method) }}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">

							<label class="col-md-4 control-label">* Title</label>
							<div class="col-md-6">
								<input type="text" name="title" class="form-control" ></input>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">* Starts At</label>
							<div class='col-md-6' >
	                <input type='text' class="form-control" name='start' id='from'/>
	            </div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">* Ends At</label>
							<div class='col-md-6'>
									<input type='text' class="form-control" name='end' id='to'/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label"> Limited Seats?</label>
							<div class='col-md-6'>
									<input type='checkbox' class="form-control" name='limited' id='limited'/>
							</div>
						</div>

						<div class="form-group" id="divCap" style="display: none;">
							<label class="col-md-4 control-label">* Capacity</label>
							<div class='col-md-6'>
									<input type='number' class="form-control" name='capacity' id='capacity' min="5" max="5000" required disabled/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">* For</label>
							<div class='col-md-6'>
								<select id="for" name="for[]" multiple>
										@foreach(\App\Year::all() as $key1=>$year)
										<optgroup label="{{$year->name}}">
												@foreach(\App\Classroom::where('year_id', $year->id)->get() as $key2=>$class)
												<option value="{{\App\ClassDetail::where('classroom_id', $class->id)->orderBy('created_at', 'desc')->first()->id}}">{{$class->name}}</option>
												@endforeach
										</optgroup>
										@endforeach
								</select>
							</div>
						</div>



						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									{{$button_name}}
								</button>
								@yield('btn-batch')
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$('#limited').click(function(){
    if($(this).is(':checked')){
				$('#divCap').show();
        $('input[name="capacity"]').prop('disabled', false);
    } else {
				$('#divCap').hide();
        $('input[name="capacity"]').prop('disabled', true);
    }
});
</script>
<script src="{{ asset('/vendor/multiple-select/js/multiple-select.js') }}"></script>
<script type="text/javascript">
	var select = $("select").multipleSelect({
	    multiple: true,
			filter: true,
	    multipleWidth: 150,
	    width: '100%'
	});
</script>
<script src="{{ asset('/vendor/bootstrap-datetimepicker/js/moment.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        var from_d = $('#from').datetimepicker({
						defaultDate: moment().format("YYYY-MM-DD HH:mm:ss"),
						format: 'YYYY-MM-DD HH:mm:ss'
				});
        var to_d = $('#to').datetimepicker({
            useCurrent: false, //Important! See issue #1075
						defaultDate: moment().add('days', 1).format("YYYY-MM-DD HH:mm:ss"),
            format: 'YYYY-MM-DD HH:mm:ss'
        });
        $("#from").on("dp.change", function (e) {
            $('#to').data("DateTimePicker").minDate(e.date);
        });
        $("#to").on("dp.change", function (e) {
            $('#from').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
<script>
'use strict';

$(document).ready(function() {

    // Select each of the dropdowns
    var titleInput = $('input[name=title]');
    var fromInput = $('#from');
    var toInput = $('#to');


		titleInput.val('{{$title}}');
		fromInput.val('{{$start}}');
		toInput.val('{{$end}}');

		@if($capacity!=null)
		$('#limited').prop('checked', true);
		$('#divCap').show();
		$('input[name="capacity"]').prop('disabled', false);
		$('input[name="capacity"]').val('{{$capacity}}');
		@endif

		@if($for_c!=null && count($for_c)>0)
		select.multipleSelect("setSelects", [
			@foreach($for_c as $c)
			{{$c}},
			@endforeach
		]);
		@endif

});
</script>
@endpush
