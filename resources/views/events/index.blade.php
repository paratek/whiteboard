@extends('app')

@section('content')
<link href="{{ asset('/css/data_table_custom.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Events</div>
				<div class="panel-body">
					<div class="container-fluid">
            <button class="btn btn-success" type="button" id="sort-default">Default Sort</button>
						@if(!\Auth::guest())
						<a class="btn btn-primary" type="button" href="/event/create">Create Event</a>
						@endif
						<div class="pull-right">Press shift and click on columns for multi-column ordering</div>
					</div>
				</div>
        <!-- Table -->
        <table id="eventsTable" class="table">
					<thead>
						<tr>
							<th>Title</th>
							<th>Start</th>
							<th>End</th>
							<th>Organizer</th>
							<th></th>
						</tr>
					</thead>
        </table>
      </div>
    </div>
	</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables.bootstrap.js') }}"></script>

<script>
$(document).ready(function() {
	var table = $('#eventsTable').DataTable({
					ajax: '/dt/event/',
					columns: [
						{data: 'title', name: 'title'},
						{data: 'start', name: 'start'},
						{data: 'end', name: 'end'},
						{data: 'organizer', name: 'organizer'},
						{data: 'options', name: 'options', orderable: false, searchable: false}
					],
        "order": [[1, 'asc'], [0, 'asc']]
			});


	 $('#sort-default').on('click', function(){
		 table.order([1, 'asc'], [0, 'asc']).draw();
	 });

	 $('#eventsTable tbody').on( 'click', '[id^=delete-lecture]', function () {
		var row = table.row( $(this).parents('tr') );
		console.log($(this))
		console.log($(this).closest('tr'))
		$(this).closest('tr').addClass('danger');

		//row.remove().draw();
		$.ajax({
				 url: '/event/' + $(this).val(),
				 data: { "_token": "{{ csrf_token() }}" },
				 type: 'DELETE',
				 success: function(json){
						row.remove().draw();
				 },
				 error: function() {
			 			$(this).closest('tr').addClass('danger');
				 }
		 });

			 console.log($(this).val());
	 } );


});
</script>
@endsection
