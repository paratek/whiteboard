@extends('app')

@section('content')
<link href="{{ asset('/css/data_table_custom.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Attendees for {{$event->title}}</div>
				<div class="panel-body">
					<div class="container-fluid">
            <button class="btn btn-success" type="button" id="sort-default">Default Sort</button>
						<div class="pull-right">Press shift and click on columns for multi-column ordering</div>
					</div>
				</div>
        <!-- Table -->
        <table id="eventsTable" class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Type</th>
							<th>Class</th>
						</tr>
					</thead>
        </table>
      </div>
    </div>
	</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables.bootstrap.js') }}"></script>

<script>
$(document).ready(function() {
	var table = $('#eventsTable').DataTable({
					ajax: '/dt/event/{{$event->id}}/attendees',
					columns: [
						{data: 'name', name: 'name'},
						{data: 'type', name: 'type'},
						{data: 'class', name: 'class'}
					],
        "order": [[1, 'asc'], [0, 'asc']]
			});


	 $('#sort-default').on('click', function(){
		 table.order([1, 'asc'], [0, 'asc']).draw();
	 });

});
</script>
@endsection
