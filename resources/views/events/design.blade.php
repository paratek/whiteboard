@extends('app')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/content-tools/css/sandbox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/content-tools/css/content-tools.min.css') }}">
@endsection

@section('content')
<div class="container">
  <article class="article">
    <section class="[ article__content ]" >
      <div>
        <h1>{{$event->title}}</h1>
      </div>
      <div data-name="main" data-editable>
        {!! $event->main !!}
      </div>
    </section>
    <aside class="article__related">
        <div class="[ article__author ]  [ author ]" data-name="about" data-editable>

            {!! $event->about !!}

        </div>
        <br />
        <div class="[ article__author ]  [ author ]" style="background:#CE489E">

            <h3 class="author__about">List of attendees</h3>
            <table class="ce-element ce-element--type-table">

              <tbody class="ce-element ce-element--type-table-section" id="attendee_list">
                <?php $self = 0; ?>
                @if(!\Auth::guest())
                <?php $self = \Auth::id(); ?>
                @endif
                @foreach($event->users as $user)
                <tr class="ce-element ce-element--type-table-row" {{($user->id==$self)?'id=self':''}}>
                  <td class="ce-element ce-element--type-table-cell">
                    <div class="ce-element ce-element--type-table-cell-text ce-element--focused" >{{$user->name}}</div>
                  </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @if(!\Auth::guest())
            @if(\DB::table('event_user')->where('user_id', \Auth::id())->where('event_id', $event->id)->first()==null)
            <br />
            <a id="button" style="display: block; width: 100%;" class="btn btn-default btn-lg" type="button">Register</a>
            <br />
            @if($event->seats_left!=null)
            <p class="author__bio">
              @if($event->seats_left>1)
              Hurry only {{$event->seats_left}} seats left.
              @elseif($event->seats_left==1)
              Hurry only {{$event->seats_left}} seat left.
              @else
              Seats are all full.
              @endif
            </p>
            @endif
            @else
            <br />
            <a id="button" style="display: block; width: 100%;" class="btn btn-default btn-lg" type="button">Unregister</a>
            <br />
            @endif
            <p class="author__bio">
                <a href="/event/{{$event->id}}/attendees" style="color:#000000">Click here</a> for the list of all the attendees.
            </p>
            @endif
        </div>
        <div class="[ article__learn-more ]  [ learn-more ]" data-name="learn_more" data-editable>

            {!! $event->learn_more !!}

        </div>
    </aside>
  </article>
  @if(!\Auth::guest())
  @if(\Auth::id()==$event->user_id)
  <script type="text/javascript" src="{{ asset('/vendor/content-tools/js/content-tools.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/vendor/content-tools/js/sandbox.js') }}"></script>
  @endif
  @endif
</div>
@endsection

@if(!\Auth::guest())
@push('scripts')
<script type="text/javascript">
$('#button').on('click', function(){
  console.log('Hi');
  if($(this).text()=="Register")
  {
    $(this).text("Unregister");
    var button = $(this);
    $.ajax({
				 url: '/event/' + {{$event->id}} + '/subscribe',
				 data: { "_token": "{{ csrf_token() }}" },
				 type: 'POST',
				 success: function(json){
           $('#attendee_list').prepend("<tr class=\"ce-element ce-element--type-table-row\" id=\"self\"><td class=\"ce-element ce-element--type-table-cell\"><div class=\"ce-element ce-element--type-table-cell-text ce-element--focused\">{{\Auth::user()->name}}</div></td></tr>");
				 },
				 error: function() {
			 			button.text("Register");
				 }
		 });
  }
  else
  {
    $(this).text("Register");
    var button = $(this);
    $.ajax({
				 url: '/event/' + {{$event->id}} + '/unsubscribe',
				 data: { "_token": "{{ csrf_token() }}" },
				 type: 'POST',
				 success: function(json){
           $('#self').remove();
				 },
				 error: function() {
			 			button.text("Unegister");
				 }
		 });
  }
});
</script>
<script type="text/javascript">
(function() {
  var ImageUploader;

  ImageUploader = (function() {
    ImageUploader.imagePath = '/vendor/content-tools/images/image.png';

    ImageUploader.imageSize = [600, 174];

    function ImageUploader(dialog) {
      this._dialog = dialog;
      this._dialog.bind('cancel', (function(_this) {
        return function() {
          return _this._onCancel();
        };
      })(this));
      this._dialog.bind('imageUploader.cancelUpload', (function(_this) {
        return function() {
          return _this._onCancelUpload();
        };
      })(this));
      this._dialog.bind('imageUploader.clear', (function(_this) {
        return function() {
          return _this._onClear();
        };
      })(this));
      this._dialog.bind('imageUploader.fileReady', (function(_this) {
        return function(files) {
          return _this._onFileReady(files);
        };
      })(this));
      this._dialog.bind('imageUploader.mount', (function(_this) {
        return function() {
          return _this._onMount();
        };
      })(this));
      this._dialog.bind('imageUploader.rotateCCW', (function(_this) {
        return function() {
          return _this._onRotateCCW();
        };
      })(this));
      this._dialog.bind('imageUploader.rotateCW', (function(_this) {
        return function() {
          return _this._onRotateCW();
        };
      })(this));
      this._dialog.bind('imageUploader.save', (function(_this) {
        return function() {
          return _this._onSave();
        };
      })(this));
      this._dialog.bind('imageUploader.unmount', (function(_this) {
        return function() {
          return _this._onUnmount();
        };
      })(this));
    }

    ImageUploader.prototype._onCancel = function() {};

    ImageUploader.prototype._onCancelUpload = function() {
      clearTimeout(this._uploadingTimeout);
      return this._dialog.state('empty');
    };

    ImageUploader.prototype._onClear = function() {
      return this._dialog.clear();
    };

    ImageUploader.prototype._onFileReady = function(file) {
      var upload;
      this._dialog.progress(0);
      this._dialog.state('uploading');
      upload = (function(_this) {
        return function() {
          var progress;
          progress = _this._dialog.progress();
          progress += 1;
          if (progress <= 100) {
            _this._dialog.progress(progress);
            return _this._uploadingTimeout = setTimeout(upload, 25);
          } else {
            return _this._dialog.populate(ImageUploader.imagePath, ImageUploader.imageSize);
          }
        };
      })(this);
      return this._uploadingTimeout = setTimeout(upload, 25);
    };

    ImageUploader.prototype._onMount = function() {};

    ImageUploader.prototype._onRotateCCW = function() {
      var clearBusy;
      this._dialog.busy(true);
      clearBusy = (function(_this) {
        return function() {
          return _this._dialog.busy(false);
        };
      })(this);
      return setTimeout(clearBusy, 1500);
    };

    ImageUploader.prototype._onRotateCW = function() {
      var clearBusy;
      this._dialog.busy(true);
      clearBusy = (function(_this) {
        return function() {
          return _this._dialog.busy(false);
        };
      })(this);
      return setTimeout(clearBusy, 1500);
    };

    ImageUploader.prototype._onSave = function() {
      var clearBusy;
      this._dialog.busy(true);
      clearBusy = (function(_this) {
        return function() {
          _this._dialog.busy(false);
          return _this._dialog.save(ImageUploader.imagePath, ImageUploader.imageSize, {
            alt: 'Example of bad variable names'
          });
        };
      })(this);
      return setTimeout(clearBusy, 1500);
    };

    ImageUploader.prototype._onUnmount = function() {};

    ImageUploader.createImageUploader = function(dialog) {
      return new ImageUploader(dialog);
    };

    return ImageUploader;

  })();

  window.ImageUploader = ImageUploader;

  window.onload = function() {
    var editor, req;
    ContentTools.IMAGE_UPLOADER = ImageUploader.createImageUploader;
    ContentTools.StylePalette.add([new ContentTools.Style('By-line', 'article__by-line', ['p']), new ContentTools.Style('Caption', 'article__caption', ['p']), new ContentTools.Style('Example', 'example', ['pre']), new ContentTools.Style('Example + Good', 'example--good', ['pre']), new ContentTools.Style('Example + Bad', 'example--bad', ['pre'])]);
    editor = ContentTools.EditorApp.get();

    editor.init('*[data-editable]', 'data-name');
    console.log(editor)
    editor.bind('save', function (ev) {
        var name, payload, regions, xhr;
        console.log(ev);
        // Check that something changed
        regions = ev;
        if (Object.keys(regions).length == 0) {
            return;
        }

        // Set the editor as busy while we save our changes
        this.busy(true);

        // Collect the contents of each region into a FormData instance
        payload = new FormData();
        for (name in regions) {
            if (regions.hasOwnProperty(name)) {
                payload.append(name, regions[name]);
            }
        }
        console.log(ev);
        // Send the update content to the server to be saved
        function onStateChange(ev) {
            // Check if the request is finished
            if (ev.target.readyState == 4) {
                editor.busy(false);
                if (ev.target.status == '200') {
                    // Save was successful, notify the user with a flash
                    new ContentTools.FlashUI('ok');
                } else {
                    // Save failed, notify the user with a flash
                    new ContentTools.FlashUI('no');
                }
            }
        };

        xhr = new XMLHttpRequest();
        xhr.addEventListener('readystatechange', onStateChange);
        xhr.open('PUT', '/event/{{$event->id}}/design');
        var fdata = new FormData();
        for(var key in ev)
        {
          fdata.append(key, ev[key]);
        }
        xhr.send(fdata);
        console.log(fdata);
    });
    /*
    editor.bind('save', function(regions, autoSave) {
      var saved;
      console.log(regions);
      editor.busy(true);
      saved = (function(_this) {
        return function() {
          editor.busy(false);
          return new ContentTools.FlashUI('ok');
        };
      })(this);
      return setTimeout(saved, 2000);
    });
    */
    req = new XMLHttpRequest();
    req.overrideMimeType('application/json');
    req.open('GET', 'https://raw.githubusercontent.com/GetmeUK/ContentTools/master/translations/lp.json', true);
    return req.onreadystatechange = function(ev) {
      var translations;
      if (ev.target.readyState === 4) {
        translations = JSON.parse(ev.target.responseText);
        ContentEdit.addTranslations('lp', translations);
        return ContentEdit.LANGUAGE = 'lp';
      }
    };
  };

}).call(this);
</script>
@endpush
@endif
