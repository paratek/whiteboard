@extends('events/details')

@section('php-code')
<?php
	$panel_heading = 'Update Event';
  $form_method = 'PUT';
	$form_action = url('/event/'.$event->id);
  $for_c = \DB::table('classpivot_event')->where('event_id', $event->id)->lists('classpivot_id');
  $title = $event->title;
	$start = $event->start;
	$end = $event->end;
	$capacity = $event->capacity;
	$button_name = 'Update';
?>
@endsection
