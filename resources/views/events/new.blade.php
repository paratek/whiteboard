@extends('events/details')

@section('php-code')
<?php
	$panel_heading = 'Add Event';
  $form_method = 'POST';
	$form_action = url('/event');
	$for_c = Input::old('for');
	$title = Input::old('title');
	$start = Input::old('start');
	$end = Input::old('end');
	$capacity = Input::old('capacity');
	$button_name = 'Save';
?>
@endsection
