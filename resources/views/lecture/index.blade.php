@extends('app')

@section('content')
<link href="{{ asset('/css/data_table_custom.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Labs/Lectures Conducted {{session('highlight')}}</div>
				<div class="panel-body">
					<div class="container-fluid">
						<button class="btn btn-success" type="button" id="sort-default">Default Sort</button>
						<div class="pull-right">Press shift and click on columns for multi-column ordering</div>
					</div>
				</div>
        <!-- Table -->
        <table id="lectureTable" class="table">
					<thead>
						<tr>
							<th></th>
							<th>Class</th>
							<th>Subject</th>
							<th>Timeslot</th>
							<th>Date</th>
							<th></th>
						</tr>
					</thead>
        </table>
      </div>
    </div>
	</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables.bootstrap.js') }}"></script>
<script src="{{ asset('/js/handlebars.js') }}"></script>
<script>Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if (a == b) {
        return opts.fn(this);
    } else {
        return opts.inverse(this);
    }
});</script>
<script id="details-template" type="text/x-handlebars-template">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			@{{#if_eq is_lab '1'}}
			<div class="panel-heading">Lab's Attendance</div>
			@{{/if_eq}}
			@{{#if_eq is_lab '0'}}
			<div class="panel-heading">Lecture's Attendance</div>
			@{{/if_eq}}
			<div class="panel-body">
				@{{#if module_id}}
				<button class="btn btn-primary" type="button">
				  <span class="badge">@{{module_name}}</span>  @{{module_desc}}
				</button>
				@{{/if}}
				@{{#if_eq is_lab '1'}}
				<button class="btn btn-success" type="button">
				  Batch <span class="badge">@{{{batch_name}}}</span>
				</button>
			  @{{/if_eq}}

				<br>
				<br>
				<button class="btn btn-success" type="button">
				  Present <span id="total-present-@{{id}}" value="@{{present}}" class="badge">@{{present}}</span>
				</button>
				<button class="btn btn-warning" type="button">
				  Absent <span id="total-absent-@{{id}}" value="@{{absent}}" class="badge">@{{absent}}</span>
				</button>
			</div>
	    <table class="table table-striped details-table" id="attendance-@{{id}}">
	        <thead>
	        <tr>
	            <th>Roll No</th>
	            <th>Name</th>
							<th>Present<th>
	        </tr>
	        </thead>
	    </table>
		</div>
	</div>
</script>
<script>
function changeAttendance(id, parent) {
	console.log(id);
	var present = $('#present-'+id).attr('is_present');
	$('#present-'+id).parents('tr').toggleClass('info');
	var pres = $('#total-present-'+parent).attr('value');
	var abs = $('#total-absent-'+parent).attr('value');
	console.log("Pres = " + pres + ", Abs = " + abs);
	if(present==0)
	{
		console.log(id + " " + present + " -> Present");
		$('#present-'+id).html('<span class="glyphicon glyphicon-ok gi-2x" aria-hidden="true" style="color:green"></span>');
		$('#present-'+id).attr('is_present', 1);
		pres++;
		abs--;
		present=1;
	}
	else
	{
			$('#present-'+id).html('<span class="glyphicon glyphicon-remove gi-2x" aria-hidden="true" style="color:red"></span>');
			$('#present-'+id).attr('is_present', 0);
			pres--;
			abs++;
			present=0;
	}
	$('#total-present-'+parent).attr('value', pres);
	$('#total-absent-'+parent).attr('value', abs);
	$('#total-present-'+parent).text(pres);
	$('#total-absent-'+parent).text(abs);
	$.ajax({
			 url: '/attendance/' + id +'/edit',
			 data: { "_token": "{{ csrf_token() }}" },
			 type: 'GET',
			 success: function(json){
							//perform operation
					$('#present-'+id).parents('tr').toggleClass('info');
					$('#present-'+id).parent('tr').toggleClass('success');
			 },
			 error: function(json) {
				 	console.log("hi");
	 				console.log("json");
					$('#present-'+id).parents('tr').toggleClass('info')
				 	//$('#lectureTable').DataTable().ajax.reload();
					if(present===0)
					{
							$('#present-'+id).html('<span class="glyphicon glyphicon-ok gi-2x" aria-hidden="true" style="color:green"></span>');
							$('#present-'+id).attr('is_present', 1);
							pres++;
							abs--;
					}
					else
					{
							$('#present-'+id).html('<span class="glyphicon glyphicon-remove gi-2x" aria-hidden="true"></span>');
							$('#present-'+id).attr('is_present', 0);
							pres--;
							abs++;
					}
					$('#total-present-'+parent).attr('value', pres);
					$('#total-absent-'+parent).attr('value', abs);
					$('#total-present-'+parent).text(pres);
					$('#total-absent-'+parent).text(abs);
			 }
	 });
}
</script>
<script>
$(document).ready(function() {
	var template = Handlebars.compile($("#details-template").html());
	var table = $('#lectureTable').DataTable({
					ajax: '/dt/lecture',
					columns: [
						{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
						{data: 'class', name: 'class'},
						{data: 'subject_name', name: 'subject'},
						{data: 'timeslot_time', name: 'timeslot'},
						{data: 'date', name: 'date'},
						{data: 'options', name: 'options', orderable: false, searchable: false}
					],
        "order": [[4, 'desc'], [3, 'desc']]
			});

		// Add event listener for opening and closing details
	 $('#lectureTable tbody').on('click', 'td.details-control', function () {
			 var tr = $(this).closest('tr');
			 var row = table.row(tr);
			 var tableId = 'attendance-' + row.data().id;

			 if (row.child.isShown()) {
					 // This row is already open - close it
					 row.child.hide();
					 tr.removeClass('shown');
			 } else {
					 // Open this row
					 row.child(template(row.data())).show();
					 initTable(tableId, row.data());
					 tr.addClass('shown');
					 tr.next().find('td').addClass('no-padding bg-gray');
			 }
	 });

	 $('#sort-default').on('click', function(){
		 table.order([5, 'desc'], [4, 'desc']).draw();
	 });

	 $('#lectureTable tbody').on( 'click', '[id^=delete-lecture]', function () {
		var row = table.row( $(this).parents('tr') );
		console.log($(this))
		console.log($(this).closest('tr'))
		$(this).closest('tr').addClass('danger');

		//row.remove().draw();
		$.ajax({
				 url: '/lecture/' + $(this).val(),
				 data: { "_token": "{{ csrf_token() }}" },
				 type: 'DELETE',
				 success: function(json){
						row.remove().draw();
				 },
				 error: function() {
			 			$(this).closest('tr').addClass('danger');
				 }
		 });

			 console.log($(this).val());
	 } );

	 function initTable(tableId, data) {
			 $('#' + tableId).DataTable({
					 processing: true,
					 serverSide: true,
					 ajax: data.details_url,
					 columns: [
							 { data: 'roll_no', name: 'roll_no' },
							 { data: 'name', name: 'name' },
							 { data: 'present', name: 'present' },
							 { data: 'action', name: 'action', orderable: false, searchable: false}
					 ],
					 createdRow: function ( row, data, index ) {
				      $('td', row).eq(1).attr('id', 'name-' + data.id);
 				      $('td', row).eq(2).attr('id', 'present-' + data.id);
 				      $('td', row).eq(2).attr('is_present', data.is_present);
							if(data.is_present==1)
								$(row).addClass('success');
							else
								$(row).addClass('error');
				   }
			 });
	 }


});
</script>
@endsection
