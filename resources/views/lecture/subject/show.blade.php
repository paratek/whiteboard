@extends('app')

@yield('php-code')

@section('content')
<link href="{{ asset('/css/jquery.dropdown.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="{{ asset('/js/dropdown-plugin.js') }}"></script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{$panel_heading}}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					@if (session('success'))
						<div class="alert alert-success">
							<strong>{{session('success')}}</strong>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ $form_action }}">
						{{ method_field($form_method) }}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">

							<label class="col-md-4 control-label">* Select Class</label>
							<div class="col-md-6">
								<select name="classpivot_id" id="first" class="form-control" role="listbox">

									@foreach($temp["classes"] as $class)
								  <option value="{{$class['id']}}">{{ $class->classroom->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">

							<label class="col-md-4 control-label">* Select Subject</label>
							<div class="col-md-6">
								<select name="subject_id" id="second" class="form-control" role="listbox" >
									@foreach($temp["subjects"] as $subject)
								  <option value="{{$subject['id']}}">{{ $subject->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">

							<label class="col-md-4 control-label">* Select Faculty</label>
							<div class="col-md-6">
								<select name="faculty_id" id="third" class="form-control" role="listbox" >
									@foreach($temp["faculty"] as $fac)
								  <option value="{{$fac['id']}}">{{ $fac->employee->user->name }}</option>
									@endforeach
								</select>
							</div>
						</div>



						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									{{$button_name}}
								</button>
								@yield('btn-batch')
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
'use strict';

$(document).ready(function() {

    // Select each of the dropdowns
    var firstDropDown = $('#first');
    var secondDropDown = $('#second');
    var thirdDropDown = $('#third');

    // Hold selected option
    var firstSelection = '';
    var secondSelection = '';
    var thirdSelection = '';

    // Hold selection
    var selection = '';



		 firstDropDown.val({{$classpivot_id}});
		 secondDropDown.val({{$subject_id}});
		 thirdDropDown.val({{$faculty_id}});

});
</script>
@endsection
