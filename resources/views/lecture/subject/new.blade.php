@extends('lecture/subject/show')

@section('php-code')
<?php
	$panel_heading = 'Add Lecture';
  $form_method = 'POST';
	$form_action = url('/lecture/subject');
	$faculty_id = Input::old('faculty_id');
	$classpivot_id = Input::old('classpivot_id');
	$subject_id = Input::old('subject_id');
	$button_name = 'Save';
?>
@endsection
