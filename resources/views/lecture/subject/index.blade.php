@extends('app')

@section('content')
<link href="{{ asset('/css/data_table_custom.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Assign Subject</div>
				<div class="panel-body">
					<div class="container-fluid">
						<button class="btn btn-success" type="button" id="sort-default">Default Sort</button>
						<div class="pull-right">Press shift and click on columns for multi-column ordering</div>
					</div>
				</div>
        <!-- Table -->
        <table id="subAllocTable" class="table">
					<thead>
						<tr>
							<th>Class</th>
							<th>Subject</th>
							<th>Faculty</th>
							<th></th>
						</tr>
					</thead>
        </table>
      </div>
    </div>
	</div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/datatables.bootstrap.js') }}"></script>

<script>
$(document).ready(function() {
	var table = $('#subAllocTable').DataTable({
					ajax: '/dt/lecture/subject',
					columns: [
						{data: 'class', name: 'class'},
						{data: 'subject_name', name: 'subject'},
						{data: 'faculty_name', name: 'faculty'},
						{data: 'options', name: 'options', orderable: false, searchable: false}
					],
        "order": [[0, 'asc'], [1, 'asc']]
			});


	 $('#sort-default').on('click', function(){
		 table.order([0, 'asc'], [1, 'asc']).draw();
	 });

	 $('#subAllocTable tbody').on( 'click', '[id^=delete-lecture]', function () {
		var row = table.row( $(this).parents('tr') );
		console.log($(this))
		console.log($(this).closest('tr'))
		$(this).closest('tr').addClass('danger');

		//row.remove().draw();
		$.ajax({
				 url: '/lecture/subject/' + $(this).val(),
				 data: { "_token": "{{ csrf_token() }}" },
				 type: 'DELETE',
				 success: function(json){
						row.remove().draw();
				 },
				 error: function() {
			 			$(this).closest('tr').addClass('danger');
				 }
		 });

			 console.log($(this).val());
	 } );


});
</script>
@endsection
