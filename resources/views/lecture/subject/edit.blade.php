@extends('lecture/subject/show')

@section('php-code')
<?php
	$panel_heading = 'Edit Allocation';
  $form_method = 'PATCH';
	$form_action = url('/lecture/subject/'.$id);
	$button_name = 'Update';
?>
@endsection

@section('btn-attendance')
<a href={{ url('attendance/create?fl_id='.$id) }} class="btn btn-warning pull-right">
	Edit Batches
</a>
@endsection
