@extends('app')

@section('content')
<div class="container">
  <div class="row">
    <div class="center-block">
      <div id="lectures_div"></div>
      @calendarchart('Lectures', 'lectures_div')
      <hr></hr>
      <h4><p class="text-center">Lectures Conducted (Department-wise)</p></h4>
      <div id="pie_dept"></div>
      @piechart('DeptLectures', 'pie_dept')
      <hr></hr>
      <h4><p class="text-center">Lectures Conducted (Year-wise)</p></h4>
      <div id="pie_year"></div>
      @piechart('YearLectures', 'pie_year')
      <hr></hr>
      <h4><p class="text-center">Average Attendance</p></h4>
      <div id="avg_att"></div>
      @gaugechart('AvgAtt', 'avg_att')
      <div id="avg_attc"></div>
      @gaugechart('AvgAttClass', 'avg_attc')
      <hr></hr>
    </div>
  </div>
</div>
@endsection
