@extends('app')

@section('content')
<div class="container-fluid">
  <div class="col-md-6 col-md-offset-3">
    <form role="form" action="/lecture/reports/dept/export/0" accept-charset="UTF-8" method="GET">

      <div class="form-group">
        <label for="from">From:</label>
        <select name="from" class="form-control">
          @foreach(range(1,12) as $month)
          <option value="{{$month}}">{{date("F", mktime(0, 0, 0, $month, 10))}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label for="to">To:</label>
        <select name="to" class="form-control">
          @foreach(range(1,12) as $month)
          <option value="{{$month}}" {{\Carbon\Carbon::now()->month==$month?'selected':''}}>{{date("F", mktime(0, 0, 0, $month, 10))}}</option>
          @endforeach
        </select>
      </div>

      <input type="submit" name="commit" value="Generate Report" class="btn btn-default">

    </form>
  </div>
</div>
@endsection
