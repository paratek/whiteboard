@extends('lecture/show')

@section('php-code')
<?php
	$panel_heading = 'Edit Lecture';
  $form_method = 'PUT';
	$form_action = url('/lecture/'.$id);
	$button_name = 'Update';
?>
@endsection

@section('btn-attendance')
<a href={{ url('attendance/create?fl_id='.$id) }} class="btn btn-warning pull-right">
	Edit Attendance
</a>
@endsection
