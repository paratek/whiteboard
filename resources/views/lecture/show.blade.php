@extends('app')

@yield('php-code')

@section('content')
<link href="{{ asset('/css/jquery.dropdown.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="{{ asset('/js/dropdown-plugin.js') }}"></script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{$panel_heading}}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					@if (session('success'))
						<div class="alert alert-success">
							<strong>{{session('success')}}</strong>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ $form_action }}">
						{{ method_field($form_method) }}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">

							<label class="col-md-4 control-label">* Select Class</label>
							<div class="col-md-6">
								<select name="class_id" id="first" class="form-control" role="listbox">

									@foreach($temp["classes"] as $class)
								  <option value="{{$class['id']}}">{{ $class['name'] }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">

							<label class="col-md-4 control-label">* Select Subject</label>
							<div class="col-md-6">
								<select name="subject_id" id="second" class="form-control" role="listbox" disabled="disabled">

								</select>
							</div>
						</div>

						<div class="form-group">

							<label class="col-md-4 control-label">* Select Type</label>
							<div class="col-md-6">
								<select name="is_lab" id="third" class="form-control" role="listbox" disabled="disabled">
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Select Module</label>
							<div class="col-md-6">
								<select name="module_id" id="fourth" class="form-control" role="listbox" disabled="disabled">

								</select>
							</div>
						</div>

						<div class="form-group">

							<label class="col-md-4 control-label">Select Batch</label>
							<div class="col-md-6">
								<select name="batch_no" id="fifth" class="form-control" role="listbox" disabled="disabled">
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">* Select Timeslot</label>
							<div class="col-md-6">
								<select name="timeslot_id" id="sixth" class="form-control">

									@foreach($temp["timeslots"] as $timeslot)
								  <option value="{{$timeslot->id}}">{{ $timeslot->start_time }}-{{ $timeslot->end_time }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									{{$button_name}}
								</button>
								@yield('btn-attendance')
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
'use strict';

$(document).ready(function() {

    // Default option
    var option = '';
    // Method to clear dropdowns down to a given level
    var clearDropDown = function(arrayObj, startIndex, endIndex) {
        $.each(arrayObj, function(index, value) {
            if(index >= startIndex && index <= endIndex && index != 5) {
                $(value).html(option);
            }
        });
    };

    // Method to disable dropdowns down to a given level
    var disableDropDown = function(arrayObj, startIndex, endIndex) {
        $.each(arrayObj, function(index, value) {
            if(index >= startIndex && index <= endIndex && index != 5) {
                $(value).attr('disabled', 'disabled');
            }
        });
    };

    // Method to disable dropdowns down to a given level
    var enableDropDown = function(that) {
        that.removeAttr('disabled');
    };

    // Methods to generate and append options

    var generateSubjects = function(element, class_id) {
        var options = '';
				var temp = data.classes[class_id].subjects;
        for (var sub in temp) {
            options += '<option value="' + temp[sub]['id'] + '">' + temp[sub]['name'] + '</option>';
        }
        element.append(options);
    };

    var generateModules = function(element, class_id, subject_id, is_lab) {
        var options = '';
				console.log("generate modules");
				var temp = data.classes[class_id].subjects[subject_id].modules;
				options += '<option value="-1"> </option>';
        for(var mod in temp) {
						if((is_lab == 1) && (temp[mod]['name'].indexOf('Module-')==0))
							continue;
						if((is_lab == 0) && (temp[mod]['name'].indexOf('Exp-')==0))
							continue;
						var str_tmp = '';
						if(temp[mod]['desc']!=null)
						{
							str_tmp = temp[mod]['desc'];
								str_tmp = ' (' + str_tmp + ')';
						}
            options += '<option value="' + temp[mod]['id'] + '">' + temp[mod]['name'] + str_tmp + '</option>';
        }
        element.append(options);
    };

    var generateLab = function(element, class_id, subject_id) {
        var options = '';
				console.log('subject_id = ' + subject_id);
				var temp = data.classes[class_id].subjects[subject_id];
				if(temp['has_theory'])
        options += '<option value="0">Lecture</option>';
				if(temp['has_prac']==1)
					options += '<option value="1">Lab</option>';
        element.append(options);
    };

    var generateBatch = function(element, class_id, subject_id, is_lab) {
        var options = '';
				var temp = '';
				if(is_lab==1)
					temp = data.classes[class_id].batches;
				else if(data.classes[class_id].subjects[subject_id].is_elective==1)
					temp = data.classes[class_id].subjects[subject_id].batches;
				for(var batch in temp) {
            options += '<option value="' + batch + '">' + temp[batch]['name'] + '</option>';
        }
        element.append(options);
    };

    // Select each of the dropdowns
    var firstDropDown = $('#first');
    var secondDropDown = $('#second');
    var thirdDropDown = $('#third');
		var fourthDropDown = $('#fourth')
		var fifthDropDown = $('#fifth')

    // Hold selected option
    var firstSelection = '';
    var secondSelection = '';
    var thirdSelection = '';
    var fourthSelection = '';
    var fifthSelection = '';

    // Hold selection
    var selection = '';

    // Selection handler for first level dropdown
    firstDropDown.on('change', function() {

        // Get selected option
        firstSelection = firstDropDown.val();

        // Clear all dropdowns down to the hierarchy
        clearDropDown($('select'), 1, 5);

        // Disable all dropdowns down to the hierarchy
        disableDropDown($('select'), 1, 5);

        // Check current selection
        if(firstSelection === '-1') {
            return;
        }

        // Enable second level DropDown
        enableDropDown(secondDropDown);

        // Generate and append options
        selection = firstSelection;
        generateSubjects(secondDropDown, firstSelection);
				secondDropDown.trigger('change');
    });

    // Selection handler for second level dropdown
    secondDropDown.on('change', function() {
        secondSelection = secondDropDown.val();

        // Clear all dropdowns down to the hierarchy
        clearDropDown($('select'), 2, 5);

        // Disable all dropdowns down to the hierarchy
        disableDropDown($('select'), 2, 5);

        // Check current selection
        if(secondSelection === '-1') {
             return;
        }

        // Enable third level DropDown
        enableDropDown(thirdDropDown);

        // Generate and append options
				selection = secondSelection;
        generateLab(thirdDropDown, firstSelection, secondSelection);
				thirdDropDown.trigger('change');
    });

		// Selection handler for third level dropdown
		thirdDropDown.on('change', function() {
        thirdSelection = thirdDropDown.val();

				clearDropDown($('select'), 3, 5);

				// Disable all dropdowns down to the hierarchy
				disableDropDown($('select'), 3, 5);

				// Check current selection
				if(thirdSelection === '-1') {
						 return;
				}

			 	enableDropDown(fourthDropDown);

				generateModules(fourthDropDown, firstSelection, secondSelection, thirdSelection);
				if(thirdSelection==1)
				{
					enableDropDown(fifthDropDown);
					generateBatch(fifthDropDown, firstSelection, secondSelection, thirdSelection);
				}

		 });

    // Selection handler for third level dropdown
    fourthDropDown.on('change', function() {

				fourthSelection = fourthDropDown.val();


     });

		 firstDropDown.val({{$class_id}});
		 firstDropDown.trigger('change');
		 secondDropDown.val({{$subject_id}});
		 secondDropDown.trigger('change');
		 thirdDropDown.val({{$is_lab}});
		 thirdDropDown.trigger('change');
		 fourthDropDown.val({{$module_id}});
		 fourthDropDown.trigger('change');
		 @if($is_lab==1)
		 fifthDropDown.val({{$batch_no}});
		 fifthDropDown.trigger('change');
		 @endif
		 $('#sixth').val({{$timeslot_id}});

});
</script>
@endsection
