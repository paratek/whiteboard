@extends('lecture/show')

@section('php-code')
<?php
	$panel_heading = 'Add Lecture';
  $form_method = 'POST';
	$form_action = url('/lecture');
	$class_id = Input::old('class_id');
	$subject_id = Input::old('subject_id');
	$module_id = Input::old('module_id');
	$timeslot_id = Input::old('timeslot_id');
	$is_lab = Input::old('is_lab');
	$batch_no = Input::old('batch_no');
	$button_name = 'Save';
?>
@endsection
