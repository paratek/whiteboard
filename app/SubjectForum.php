<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SubjectForum
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @property-read mixed $last_post
 * @property-read \App\Faculty $faculty
 * @property-read \App\Subject $subject
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FacultyLecture[] $faculty_lectures
 * @property-read \App\ClassDetail $class_detail
 * @property integer $id
 * @property integer $faculty_id
 * @property integer $subject_id
 * @property integer $classpivot_id
 * @property integer $post_count
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $has_batches
 */
class SubjectForum extends SubjectAllocation
{
	protected $table = 'subject_allocations';

	protected $appends = ['last_post'];

	public static $rules = array(
      //  'message' => 'required'
    );

	public function posts()
	{
		return $this->morphMany('App\Post', 'postable');
	}

	public function getLastPostAttribute()
	{
		return \App\Post::where('forum_id', $this->id)->orderBy('updated_at', 'desc')->first();
	}

}
