<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Semester
 *
 * @property integer $id
 * @property string $name
 * @property integer $semester_categories_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Semester extends Model {

		//

}
