<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * App\Post
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $message
 * @property integer $flags
 * @property integer $upvotes
 * @property integer $postable_id
 * @property string $postable_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read mixed $author
 * @property-read mixed $created_at_human
 * @property-read \App\ClasspivotUser $classp
 * @property-read mixed $user_name
 * @property-read \App\Post $postable
 */
class Post extends Model
{

	protected $fillable = ['message', 'views', 'user_id', 'forum_id'];

	protected $appends = array('created_at_human', 'user_name');

	public static $rules = array(
        'message' => 'required'
    );

	public function getAuthorAttribute()
	{
		return \App\ClasspivotUser::find($this->user_id)->user->name;
	}

	public function getCreatedAtHumanAttribute()
	{
		return Carbon::parse($this->created_at)->diffForHumans();
	}

	public function classp()
	{
		return $this->belongsTo('App\ClasspivotUser', 'user_id');
	}

	public function getUserNameAttribute()
	{
		return \App\ClasspivotUser::find($this->user_id)->user->name;
	}

  public function postable()
  {
    return $this->morphTo();
  }

}
