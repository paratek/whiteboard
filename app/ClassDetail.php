<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ClassDetail
 *
 * @property-read \App\Classroom $classroom
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subject[] $subjects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClasspivotUser[] $users
 * @property integer $id
 * @property integer $classroom_id
 * @property integer $semester_id
 * @property integer $academic_year_id
 * @property integer $post_count
 * @property integer $class_incharge_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ClassDetail extends Model {

		protected $table = 'classpivot';

		public function classroom()
		{
			return $this->belongsTo('App\Classroom');
		}

		public function subjects()
		{
			return $this->hasMany('App\SubjectAllocation', 'classpivot_id');
		}

		public function users()
		{
			return $this->hasMany('App\ClasspivotUser', 'classpivot_id');
		}

}
