<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\Classroom
 *
 * @property integer $id
 * @property string $name
 * @property integer $year_id
 * @property integer $department_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Year $year
 * @property-read \App\Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClassDetail[] $details
 */
class Classroom extends Model
{
  use Rememberable;

	public function year()
	{
		return $this->belongsTo('App\Year');
	}

	public function department()
	{
		return $this->belongsTo('App\Department');
	}

	public function details()
	{
		return $this->hasMany('App\ClassDetail');
	}

}
