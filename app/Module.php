<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\Module
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $subject_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Subject $subject
 * @property string $desc
 * @property integer $hours
 */
class Module extends Model
{
  use Rememberable;

	public function subject()
	{
		return $this->belongsTo('App\Subject');
	}

}
