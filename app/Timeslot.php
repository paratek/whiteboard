<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\Timeslot
 *
 * @property integer $id
 * @property string $start_time
 * @property string $end_time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Timeslot extends Model
{
  use Rememberable;

}
