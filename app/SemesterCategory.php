<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SemesterCategory
 *
 * @property integer $id
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class SemesterCategory extends Model {

		protected $table = 'semester_categories';

}
