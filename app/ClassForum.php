<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ClassForum
 *
 * @property integer $id
 * @property integer $classroom_id
 * @property integer $semester_id
 * @property integer $academic_year_id
 * @property integer $post_count
 * @property integer $class_incharge_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @property-read mixed $last_post
 * @property-read \App\Classroom $classroom
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subject[] $subjects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClasspivotUser[] $users
 */
class ClassForum extends ClassDetail
{
	protected $table = 'classpivot';

	protected $appends = ['last_post'];

	public static $rules = array(
      //  'message' => 'required'
    );

	public function posts()
	{
		return $this->morphMany('App\Post', 'postable');
	}

	public function getLastPostAttribute()
	{
		return \App\Post::where('forum_id', $this->id)->orderBy('updated_at', 'desc')->first();
	}

}
