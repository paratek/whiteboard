<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;


/**
 * App\Faculty
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subject[] $subjects
 * @property-read \App\Employee $employee
 * @property integer $id
 * @property integer $employee_id
 * @property string $designation
 * @property boolean $is_approved
 * @property integer $category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Faculty extends Model
{
  use Rememberable;

  public function subjects()
  {
    return $this->belongsToMany('App\Subject', 'subject_allocations', 'subject_id', 'faculty_id');
  }

  public function employee()
  {
    return $this->belongsTo('App\Employee');
  }

  public function lectures()
  {
    return $this->hasManyThrough('App\FacultyLecture', 'App\SubjectAllocation');
  }

}
