<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\FacultyLecture
 *
 * @property integer $id
 * @property integer $subject_allocation_id
 * @property integer $timeslot_id
 * @property integer $module_id
 * @property boolean $is_lab
 * @property string $date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\SubjectAllocation $subject_allocation
 * @property-read \App\Timeslot $timeslot
 * @property-read \App\Module $module
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\StudentAttendance[] $attendance
 * @property-read mixed $class
 * @property-read mixed $dept
 * @property-read mixed $year
 * @property-read mixed $subject_name
 * @property-read mixed $module_name
 * @property-read mixed $module_desc
 * @property-read mixed $timeslot_time
 * @property-read mixed $details_url
 * @property-read mixed $total
 * @property-read mixed $present
 * @property-read mixed $absent
 * @property boolean $batch_no
 * @property boolean $type
 */
class FacultyLecture extends Model
{
  use Rememberable;

  protected $fillable = array('subject_allocation_id', 'timeslot_id', 'module_id', 'date', 'is_lab', 'created_at', 'batch_no');

  protected $appends = ['class', 'dept', 'year', 'subject_name', 'module_name', 'module_desc', 'timeslot_time', 'details_url', 'total', 'present', 'absent', 'batch_name'];

  public function subject_allocation()
  {
    return $this->belongsTo('App\SubjectAllocation');
  }

  public function timeslot()
  {
    return $this->belongsTo('App\Timeslot');
  }

  public function module()
  {
    return $this->belongsTo('App\Module');
  }

  public function attendance()
  {
    return $this->hasMany('App\StudentAttendance', 'faculty_lecture_id', 'id');
  }

  public function getClassAttribute()
  {
    return \App\SubjectAllocation::remember(60)->find($this->subject_allocation_id)->class_detail->classroom->name;
  }

  public function getDeptAttribute()
  {
    return \App\SubjectAllocation::remember(60)->find($this->subject_allocation_id)->class_detail->classroom->department->name;
  }

  public function getYearAttribute()
  {
    return \App\SubjectAllocation::remember(60)->find($this->subject_allocation_id)->class_detail->classroom->year->name;
  }

  public function getSubjectNameAttribute()
  {
    return \App\SubjectAllocation::remember(60)->find($this->subject_allocation_id)->subject->name;
  }

  public function getModuleNameAttribute()
  {
    if($this->module_id==null)
      return;
    return \App\Module::remember(60)->find($this->module_id)->name;
  }

  public function getModuleDescAttribute()
  {
    if($this->module_id==null)
      return;
    return \App\Module::remember(60)->find($this->module_id)->desc;
  }

  public function getTimeslotTimeAttribute()
  {
    $t = \App\Timeslot::remember(60)->find($this->timeslot_id);
    return $t->start_time.' - '.$t->end_time;
  }

  public function getDetailsUrlAttribute()
  {
    return url('dt/lecture/data/' . $this->id);
  }

  public function getTotalAttribute()
  {
    return \App\StudentAttendance::where('faculty_lecture_id', $this->id)->count();
  }

  public function getPresentAttribute()
  {
    return \App\StudentAttendance::where('faculty_lecture_id', $this->id)->where('is_present', true)->count();
  }

  public function getAbsentAttribute()
  {
    return \App\StudentAttendance::where('faculty_lecture_id', $this->id)->where('is_present', false)->count();
  }

  public function getBatchNameAttribute()
  {
    if($this->is_lab==0)
      return '';
    $class = \App\SubjectAllocation::find($this->subject_allocation_id)->class_detail->classroom->name;
    return explode(' ', $class)[2].($this->batch_no+1);
  }

}
