<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\Subject
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Faculty[] $faculties
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Module[] $modules
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SubjectAllocation[] $subject_allocations
 * @property integer $id
 * @property string $name
 * @property string $shortform
 * @property boolean $has_lab
 * @property boolean $is_elective
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $code
 * @property float $credits
 * @property boolean $has_theory
 * @property boolean $has_prac
 */
class Subject extends Model
{
  use Rememberable;

	public function modules()
	{
		return $this->hasMany('App\Module');
	}

	public function subject_allocations()
	{
		return $this->hasMany('App\SubjectAllocation');
	}

}
