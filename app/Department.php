<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\Department
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classroom[] $classrooms
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Department extends Model
{
  use Rememberable;

	public function classrooms()
	{
		return $this->hasMany('App\Classroom');
	}

  public function classp()
  {
      return $this->hasManyThrough('App\ClassDetail', 'App\Classroom', 'department_id', 'classroom_id');
  }

}
