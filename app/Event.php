<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $fillable = ['title', 'start', 'end', 'capacity', 'seats_left', 'main', 'about', 'learn_more'];

  protected $appends = ['organizer'];

  public function user()
  {
    return $this->belongsToMany('App\User');
  }

  public function users()
  {
    return $this->belongsToMany('App\User', 'event_user')->withTimestamps();
  }

  public function classpivot()
  {
    return $this->belongsToMany('App\ClassDetail', 'classpivot_event', 'event_id', 'classpivot_id');
  }

  public function getOrganizerAttribute()
  {
    return User::find($this->user_id)->name;
  }
}
