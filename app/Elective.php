<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\Elective
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $subject_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $subject_allocation_id
 * @property boolean $batch
 */
class Elective extends Model
{
  use Rememberable;

}
