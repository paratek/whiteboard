<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AcademicYear
 *
 * @property integer $id
 * @property integer $start_year
 * @property integer $end_year
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class AcademicYear extends Model {

		//

}
