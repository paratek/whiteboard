<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\SubjectAllocation
 *
 * @property integer $id
 * @property integer $faculty_id
 * @property integer $subject_id
 * @property integer $classpivot_id
 * @property integer $post_count
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Faculty $faculty
 * @property-read \App\Subject $subject
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FacultyLecture[] $faculty_lectures
 * @property-read \App\ClassDetail $class_detail
 * @property boolean $has_batches
 */
class SubjectAllocation extends Model
{
  use Rememberable;

  protected $fillable = ['subject_id', 'faculty_id', 'classpivot_id'];

  protected $appends = ['class', 'subject_name', 'faculty_name'];

  public function faculty()
  {
    return $this->belongsTo('App\Faculty');
  }

  public function subject()
  {
    return $this->belongsTo('App\Subject');
  }

	public function faculty_lectures()
	{
		return $this->hasMany('App\FacultyLecture');
	}

	public function class_detail()
	{
		return $this->belongsTo('App\ClassDetail', 'classpivot_id');
	}

	public function elective_students()
	{
		return $this->hasMany('App\Elective');
	}

  public function att()
  {
      return $this->hasManyThrough('App\StudentAttendance', 'App\FacultyLecture', 'subject_allocation_id', 'faculty_lecture_id');
  }

  public function getClassAttribute()
  {
    return \App\ClassDetail::where('id', $this->classpivot_id)->first()->classroom->name;
  }

  public function getSubjectNameAttribute()
  {
    return \App\Subject::where('id', $this->subject_id)->first()->name;
  }

  public function getFacultyNameAttribute()
  {
    return \App\Faculty::where('id', $this->faculty_id)->first()->employee->user->name;
  }
}
