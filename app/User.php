<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Watson\Rememberable\Rememberable;

/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClassDetail[] $classp
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClasspivotUser[] $classu
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read \App\Employee $emp
 * @property string $username
 * @method static \Illuminate\Database\Query\Builder|\App\User whereSlug($slug)
 */
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EntrustUserTrait, SluggableTrait, Rememberable;

    //use Authorizable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'slug'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
      protected $hidden = ['password', 'remember_token'];

      protected $appends = ['class', 'type'];

      protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'username',
      ];

      public function setNameAttribute($value)
      {
          $this->attributes['username'] = \App\User::createSlug($value);
          $this->attributes['name'] = $value;
      }

      public function events()
      {
        return $this->belongsToMany('App\Event', 'event_user')->withTimestamps();
      }

      public function classp()
      {
        return $this->belongsToMany('App\ClassDetail', 'classpivot_user', 'user_id', 'classpivot_id');
    	}

      public function classu()
      {
        return $this->hasMany('App\ClasspivotUser', 'user_id');
    	}

    	public function courses()
    	{
    		return $this->hasMany('App\Course');
    	}

    	public function roles()
      {
        return $this->belongsToMany('App\Role', 'role_user');
      }

    	public function emp()
    	{
    		return $this->hasOne('App\Employee');
    	}

      public function getTypeAttribute()
      {
        $role = \DB::table('role_user')->where('user_id', $this->id)->first();
        if($role==null)
          return '';
        $role = \App\Role::find($role->role_id);
        if($role==null)
          return '';
        return $role->display_name;
      }

      public function getClassAttribute()
      {

        $role = \DB::table('role_user')->where('user_id', $this->id)->first();
        if($role==null)
          return '';
        $role = \App\Role::find($role->role_id);
        if($role==null)
          return '';
        if($role->name=='stud')
        {
          $c = \DB::table('classpivot_user')->where('user_id', $this->id)->first();
          if($c==null)
            return '';
          $class = \App\ClassDetail::find($c->classpivot_id);
          if($class == null)
            return '';
          else
            return $class->classroom->name;
        }
        else
          return '';
      }

}
