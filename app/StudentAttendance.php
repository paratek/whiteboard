<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\StudentAttendance
 *
 * @property integer $id
 * @property integer $faculty_lecture_id
 * @property integer $classpivot_user_id
 * @property boolean $is_present
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\FacultyLecture $faculty_lecture
 * @property-read \App\ClasspivotUser $classpivot_user
 * @property-read mixed $roll_no
 * @property-read mixed $name
 * @property-read mixed $present
 * @property-read mixed $subject_name
 * @property-read mixed $value
 */
class StudentAttendance extends Model
{
  protected $table = 'student_attendance';

  protected $fillable = array('faculty_lecture_id', 'classpivot_user_id', 'is_present', 'created_at');

  protected $appends = ['roll_no', 'name', 'present'];

  public function faculty_lecture()
  {
    return $this->belongsTo('App\FacultyLecture');
  }

  public function classpivot_user()
  {
    return $this->belongsTo('App\ClasspivotUser');
  }

  public function getRollNoAttribute()
  {
     return \DB::table('classpivot_user')->where('id', $this->classpivot_user_id)->first()->roll_no;
  }

  public function getNameAttribute()
  {
    $user_id = \DB::table('classpivot_user')->where('id', $this->classpivot_user_id)->first()->user_id;
    return \App\User::find($user_id)->name;
  }

  public function getPresentAttribute()
  {
    if($this->is_present)
      return '<span class="glyphicon glyphicon-ok gi-2x" aria-hidden="true" style="color:green"></span>';
    else
     return '<span class="glyphicon glyphicon-remove gi-2x" aria-hidden="true" style="color:red"></span>';
  }

  public function getSubjectNameAttribute()
  {
    return \App\FacultyLecture::find($this->faculty_lecture_id)->subject_name;
  }

  public function getValueAttribute()
  {
    if($this->is_present)
      return 1;
    else
      return 0;
  }

}
