<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Watson\Rememberable\Rememberable;

/**
 * App\ClasspivotUser
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $classpivot_id
 * @property integer $roll_no
 * @property-read \App\FacultyLecture $faculty_lecture
 * @property-read \App\User $user
 * @property boolean $batch
 */
class ClasspivotUser extends Model
{
  use Rememberable;
  protected $table = 'classpivot_user';

  public function faculty_lecture()
  {
    return $this->belongsTo('App\FacultyLecture');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

}
