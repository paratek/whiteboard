<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return dd(\App\FacultyLecture::first());
});

Route::get('/dd/users', function () {
  foreach(\App\ClassDetail::all() as $classp)
  {
    $ss = \App\Subject::where('is_elective', 1)->lists('id');
    $subs = \App\SubjectAllocation::where('classpivot_id', $classp->id)->whereIn('id', $ss)->get();
    $subs_no = count($subs);
    foreach($subs as $sub_all)
    {
      foreach(\App\ClasspivotUser::where('classpivot_id', $classp->id)->where('roll_no', '!=', '0')->get() as $key=>$user)
      {
        if(($user->roll_no % $subs_no)==$key)
        {
          return dd($key);
          if($user->roll_no < 41 || !$sub_all->has_batches)
            $batch = 0;
          else
            $batch = 1;
          \App\Elective::create(['classpivot_user_id'=>$user->id, 'subject_allocation_id'=>$sub_all->id, 'batch_no'=>$batch]);
        }
      }
    }
  }
  return dd("Hi");
    //return dd(\App\User::all()->toArray());
});

Route::get('/lecture/reports', function() {

  return view('lecture.reports');
  //return dd($ex);
  //return response()->download();
});

Route::get('/lecture/subject', function() {

  return view('lecture.subject.index');
  //return dd($ex);
  //return response()->download();
});

Route::get('/lecture/subject/new', function() {
  $temp = '';
  $dept_id = \Auth::user()->emp->fac->department_id;
  $cr_id = \App\Classroom::where('department_id', $dept_id)->lists('id');
  $temp['classes'] = \App\ClassDetail::whereIn('classroom_id', $cr_id)->get();
  $temp['subjects'] = \App\Subject::all()->sortBy('name');
  $temp['faculty'] = \App\Faculty::all()->sortBy(function($fac, $key){
    return $fac->employee->user->name;
  });
  return view('lecture.subject.new')->withTemp($temp);
  //return dd($ex);
  //return response()->download();
});


Route::get('/lecture/subject/{id}', function($id) {
  $temp = '';
  $dept_id = \Auth::user()->emp->fac->department_id;
  $cr_id = \App\Classroom::where('department_id', $dept_id)->lists('id');
  $temp['classes'] = \App\ClassDetail::whereIn('classroom_id', $cr_id)->get();
  $temp['subjects'] = \App\Subject::all()->sortBy('name');
  $temp['faculty'] = \App\Faculty::all()->sortBy(function($fac, $key){
    return $fac->employee->user->name;
  });
  $sa = \App\SubjectAllocation::find($id);
  $faculty_id = $sa->faculty_id;
	$classpivot_id = $sa->classpivot_id;
	$subject_id = $sa->subject_id;
  return view('lecture.subject.edit')->withId($id)->withTemp($temp)->withFacultyId($faculty_id)->withClasspivotId($classpivot_id)->withSubjectId($subject_id);
  //return dd($ex);
  //return response()->download();
});

Route::patch('/lecture/subject/{id}', function($id) {
  $sub = \App\SubjectAllocation::where('subject_id', Input::get('subject_id'))->where('faculty_id', Input::get('faculty_id'))->where('classpivot_id', Input::get('classpivot_id'))->get();
  if($sub->count()!=0)
    return redirect()->back()->withInput()->withErrors('Allocation already exists');
  $sa = \App\SubjectAllocation::find($id);
  $sa->update(Input::only('subject_id', 'faculty_id', 'classpivot_id'));
  return redirect('/lecture/subject/'.$id);
  //return dd($ex);
  //return response()->download();
});

Route::post('/lecture/subject', function() {
  $sub = \App\SubjectAllocation::where('subject_id', Input::get('subject_id'))->where('faculty_id', Input::get('faculty_id'))->where('classpivot_id', Input::get('classpivot_id'))->get();
  //return dd($sub);
  if($sub->count()!=0)
    return redirect()->back()->withInput()->withErrors('Allocation already exists');
  $sa = \App\SubjectAllocation::create(Input::only('subject_id', 'faculty_id', 'classpivot_id'));
  DB::table('classpivot_user')->insert(array('classpivot_id'=>Input::get('classpivot_id'), 'user_id'=>\App\Faculty::find(Input::get('faculty_id'))->employee->user->id, 'roll_no'=>0, 'batch_no'=>0));

  return redirect('/lecture/subject/'.$sa->id);
  //return dd($ex);
  //return response()->download();
});

Route::delete('/lecture/subject/{id}', function($id) {
  $lec = \App\SubjectAllocation::find($id);
  $lec->delete();
  return response()->json(array('success'=>true));
});

Route::get('/lecture/reports/dept/export/{id}', function($id) {
  $fname = 'Department Attendance -';
  $from = date('M', mktime(0, 0, 0, \Input::get('from'), 10));
  $to = date('M', mktime(0, 0, 0, \Input::get('to'), 10));
  $fname = $fname." ".$from." to ".$to;
  Excel::create($fname, function($excel) {
    $clps = \App\Department::find(\Auth::user()->emp->fac->department_id)->classp;

    foreach($clps as $clp) {

      $excel->sheet($clp->classroom->name, function($sheet) use($clp) {
          $users = $clp->users->sortBy('roll_no')->filter(function ($f) {
            return $f->roll_no!=0;
          });
          $uinfo='';
          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:A5');
          $sheet->setCellValue('A1', 'Roll No.');
          $sheet->setCellValue('B1', 'Subjects');
          $sheet->setCellValue('B2', 'Name of Faculty');
          $sheet->setCellValue('B3', 'Total Number of Lectures');
          $sheet->mergeCells('B4:B5');
          $sheet->setCellValue('B4', 'Name of the Student');
          //return dd($users->toArray());

          foreach($users as $key=>$user)
          {
            $uinfo[$user->roll_no]['cum']=0;
            $uinfo[$user->roll_no]['cum_tot']=0;
            $uinfo[$user->roll_no]['monthly']=0;
            $uinfo[$user->roll_no]['monthly_tot']=0;
            $sheet->setCellValue('A'.($key+6), $user->roll_no);
            $sheet->setCellValue('B'.($key+6), $user->user->name);
          }

          $subs = $clp->subjects;

          foreach($subs as $key=>$subject)
          {


            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'1'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$key+5).'1');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'1', $subject->subject->shortform);
            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'2'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$key+5).'2');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'2', $subject->faculty->employee->user->name);

            $fl = $subject->faculty_lectures->sortBy('date')->filter(function ($f) {
              $mon = \Carbon\Carbon::parse($f->date)->month;
              return $mon >= \Input::get('from') && $mon <= \Input::get('to');
            });

            $facl = '';
            foreach($fl as $fa)
            {
              $facl[$fa->id] = $fa;
            }
            $cum_total = count($fl);
            //return dd($fl->toArray());
            $mon = $fl->filter(function($f) use($fl){
              $tp = \Carbon\Carbon::parse($fl->last()->date);
              return \Carbon\Carbon::parse($f->date)->gte($tp->startOfMonth());
            });

            $fam=array();
            foreach($mon as $ma)
            {
              $fam[$ma->id] = $ma;
            }
            //return dd($mon->toArray());
            $mon_total = count($mon);

            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'3'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$key+3).'3');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'3', $mon_total);
            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$key+4).'3'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$key+5).'3');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+4).'3', $cum_total);

            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'4'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$key+3).'4');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'4', 'Mly');
            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$key+4).'4'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$key+5).'4');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+4).'4', 'Cum');

            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).'5', 'Att');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+3).'5', '%');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+4).'5', 'Att');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+5).'5', '%');

            $atts = $subject->att;

            foreach($users as $key1=>$user)
            {

              $temp_att = $atts->filter(function ($f) use($fam, $user){
                return $user->id==$f->classpivot_user_id && (array_key_exists($f->faculty_lecture_id, $fam));
              });



              if(count($temp_att)==0 || $cum_total==0)
                continue;

              $temp_att = $temp_att->filter(function ($f) {
                return $f->is_present==1;
              });

              $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+4).($key1+6), count($temp_att));

              if($cum_total!=0)
                $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+5).($key1+6), round(100*count($temp_att)/$cum_total));

              $uinfo[$user->roll_no]['cum']=$uinfo[$user->roll_no]['cum']+count($temp_att);
              $uinfo[$user->roll_no]['cum_tot']=$uinfo[$user->roll_no]['cum_tot']+$cum_total;

              if($mon_total==0)
                continue;

              $temp_att = $temp_att->filter(function ($f) use($fam) {
                if(!array_key_exists($f->faculty_lecture_id, $fam))
                  return false;
                else
                  return true;
              });

              $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+2).($key1+6), count($temp_att));
              if($mon_total!=0)
                $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$key+3).($key1+6), round(100*count($temp_att)/$mon_total));

              $uinfo[$user->roll_no]['monthly'] = $uinfo[$user->roll_no]['monthly'] + count($temp_att);
              $uinfo[$user->roll_no]['monthly_tot'] = $uinfo[$user->roll_no]['monthly_tot'] + $mon_total;

            }



            $sub_count = count($subs);
            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+2).'1'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+5).'2');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+2).'1', 'Total Attendance');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+2).'5', 'Att');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+3).'5', '%');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+4).'5', 'Att');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+5).'5', '%');
            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+2).'4'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+3).'4');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+2).'4', 'Mly');
            $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+4).'4'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+5).'4');
            $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+4).'4', 'Cum');
            //return dd($atts->toArray());
            foreach($users as $key1=>$user)
            {
              $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+2).'3'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+3).'3');
              $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+2).'3', $uinfo[$user->roll_no]['monthly_tot']);
              $sheet->mergeCells(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+4).'3'.':'.\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+5).'3');
              $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+4).'3', $uinfo[$user->roll_no]['cum_tot']);
              $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+4).($key1+6), $uinfo[$user->roll_no]['monthly']);
              if($uinfo[$user->roll_no]['monthly_tot']!=0)
                $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+5).($key1+6), round(100*$uinfo[$user->roll_no]['monthly']/$uinfo[$user->roll_no]['monthly_tot']));
              $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+2).($key1+6), $uinfo[$user->roll_no]['cum']);
              if($uinfo[$user->roll_no]['cum_tot']!=0)
                $sheet->setCellValue(\PHPExcel_Cell::stringFromColumnIndex(4*$sub_count+3).($key1+6), round(100*$uinfo[$user->roll_no]['cum']/$uinfo[$user->roll_no]['cum_tot']));
            }

          }

      });
    }

  })->download('xlsx');

  //return dd($ex);
  //return response()->download();
});

Route::get('/dd/user/{id}', function ($id) {
    $user = \App\User::findBySlug($id);
    if(is_null($user))
      $user= new \App\User;
    return dd($user->toArray());
});

Route::get('test-e', function () {
  $fname = 'Department Attendance -';
  $from = date('M', mktime(0, 0, 0, \Input::get('from'), 10));
  $to = date('M', mktime(0, 0, 0, \Input::get('to'), 10));
  $fname = $fname." ".$from." to ".$to;
  $clps = \App\Department::find(\Auth::user()->emp->fac->department_id)->classp;

  foreach($clps as $clp) {

    $users = $clp->users->sortBy('roll_no')->filter(function ($f) {
      return $f->roll_no!=0;
    });
    $uinfo='';
    //return dd($users->toArray());

    foreach($users as $key=>$user)
    {
      $uinfo[$user->roll_no]['cum']=0;
      $uinfo[$user->roll_no]['cum_tot']=0;
      $uinfo[$user->roll_no]['monthly']=0;
      $uinfo[$user->roll_no]['monthly_tot']=0;
    }

    $subs = $clp->subjects;

    foreach($subs as $key=>$subject)
    {

      $fl = $subject->faculty_lectures->sortBy('date')->filter(function ($f) {
        $mon = \Carbon\Carbon::parse($f->date)->month;
        return $mon >= \Input::get('from') && $mon <= \Input::get('to');
      });

      $facl = '';
      foreach($fl as $fa)
      {
        $facl[$fa->id] = $fa;
      }
      $cum_total = count($fl);
      //return dd($fl->toArray());
      $mon = $fl->filter(function($f) use($fl){
        $tp = \Carbon\Carbon::parse($fl->last()->date);
        return \Carbon\Carbon::parse($f->date)->gte($tp->startOfMonth());
      });

      $fam='';
      foreach($mon as $ma)
      {
        $fam[$ma->id] = $ma;
      }
      //return dd($mon->toArray());
      $mon_total = count($mon);

      $atts = $subject->att;

      foreach($users as $key1=>$user)
      {

        $temp_att = $atts->filter(function ($f) use($fam, $user){
          return $user->id==$f->classpivot_user_id && (array_key_exists($f->faculty_lecture_id, $fam));
        });



        if(count($temp_att)==0 || $cum_total==0)
          continue;

        $temp_att = $temp_att->filter(function ($f) {
          return $f->is_present==1;
        });


        $uinfo[$user->roll_no]['cum']=$uinfo[$user->roll_no]['cum']+count($temp_att);
        $uinfo[$user->roll_no]['cum_tot']=$uinfo[$user->roll_no]['cum_tot']+$cum_total;

        if($mon_total==0)
          continue;

        $temp_att = $temp_att->filter(function ($f) use($fam) {
          if(!array_key_exists($f->faculty_lecture_id, $fam))
            return false;
          else
            return true;
        });

        $uinfo[$user->roll_no]['monthly'] = $uinfo[$user->roll_no]['monthly'] + count($temp_att);
        $uinfo[$user->roll_no]['monthly_tot'] = $uinfo[$user->roll_no]['monthly_tot'] + $mon_total;

      }

      //return dd($atts->toArray());


    }

  }
  return view('welcome');
});

Route::get('/dd/user/{id}/class', function ($id) {
    return dd(\App\User::find($id)->classrooms->toArray());
});

Route::get('/dd/class', function () {
    $ss = \App\Subject::where('is_elective', 1)->lists('id');
    $temp = '';
    foreach(\App\ClassDetail::all() as $classp)
    {
      $temp[$classp->id] = $classp->toArray();
      $subs = \App\SubjectAllocation::where('classpivot_id', $classp->id)->whereIn('subject_id', $ss)->get();
      $subs_no = count($subs);
      foreach($subs as $key=>$sub_all)
      {
        $temp[$classp->id]['sub'][$sub_all->id] = $sub_all->toArray();
        foreach(\App\ClasspivotUser::where('classpivot_id', $classp->id)->where('roll_no', '!=', '0')->get() as $user)
        {
          $tmp_calc = $user->roll_no % $subs_no;
          //return dd(array('roll_no'=>$user->roll_no, 'subs_no'=>$subs_no, 'key'=>$key, 'calc'=>$tmp_calc, 'bool'=>$tmp_calc==$key+1));
          if(($user->roll_no % $subs_no)==($key+1))
          {
            if($user->roll_no < 41 || !$sub_all->has_batches)
            {
              $batch = 0;
              $temp[$classp->id]['sub'][$sub_all->id]['batch']['one'] = $user->roll_no;
            }
            else
            {
              $batch = 1;
              $temp[$classp->id]['sub'][$sub_all->id]['batch']['two'] = $user->roll_no;
            }
          }
        }
      }
    }
    return dd($temp);
    //return dd(\App\Classroom::all()->toArray());
});

Route::get('/dd/class/{id}', function ($id) {
    return dd(\App\Classroom::find($id)->toArray());
});

Route::get('/dd/class/{id}/users', function ($id) {
    return dd(\App\Classroom::find($id)->users->toArray());
});

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/fact/{id}', function ($id) {
    return dd(\App\Faculty::find($id)->emp);
});

Route::get('dt/event', [
    'as' => 'dt_eve', 'uses' => 'EventController@getDtData'
]);

Route:get('event/{id}/attendees', function ($id) {
  $event = \App\Event::find($id);
  if($event==null)
    return redirect('/event');
  else
    return view('events.attendees')->withEvent($event);
});

Route::get('dt/event/{id}/attendees', [
    'as' => 'dt_att', 'uses' => 'EventController@getAttendeeData'
]);

Route::get('dt/lecture', [
    'as' => 'dt_lec', 'uses' => 'LectureController@getDtData'
]);

Route::get('dt/lecture/data/{id}', [
    'as' => 'dt_lec', 'uses' => 'LectureController@getDtDetailsData'
]);

Route::get('dt/forum/class/', [
    'as' => 'dt_lec', 'uses' => 'ForumController@getDtClassForumData'
]);

Route::get('dt/lecture/subject', [
    'as' => 'dt_lec', 'uses' => 'LectureController@getDtSubjectData'
]);

Route::get('/lecture/analytics', function () {
  //return dd(array(\Auth::user()->emp->fac->lectures->groupBy('dept')->sum('present')));
  //return dd(array(\App\SubjectAllocation::where('faculty_id', \Auth::user()->emp->fac->id)->lists('id')));
  $lectures = \App\Faculty::remember(60)->find(\Auth::user()->emp->fac->id)->lectures;
  $dates = array();
  $min = 100;
  $max = 0;
  foreach($lectures as $lecture) {
    if(!array_key_exists($lecture->date, $dates))
      $dates[$lecture->date]=1;
    else
      $dates[$lecture->date]++;
    if($min>$dates[$lecture->date])
      $min = $dates[$lecture->date];
    if($max<$dates[$lecture->date])
      $max = $dates[$lecture->date];
  }

  $lecs = Lava::DataTable();

  $lecs->addDateColumn('Date')
        ->addNumberColumn('Lectures');

  foreach ($dates as $date=>$value) {
      $lecs->addRow([$date, $value]);
  }

  Lava::CalendarChart('Lectures', $lecs, [
      'title' => 'Lectures Conducted',
      'unusedMonthOutlineColor' => [
          'stroke'        => '#ECECEC',
          'strokeOpacity' => 0.75,
          'strokeWidth'   => 1
      ],
      'dayOfWeekLabel' => [
          'color'    => '#4f5b0d',
          'fontSize' => 16,
          'italic'   => true
      ],
      'noDataPattern' => [
          'color' => '#DDD',
          'backgroundColor' => '#11FFFF'
      ],
      'colorAxis' => [
          'values' => [$min, $max],
          'colors' => ['red', 'green']
      ]
  ]);

  $depts = Lava::DataTable();

  $depts->addStringColumn('Department')
          ->addNumberColumn('Lectures');

  $deps = $lectures->groupBy('dept');
  foreach($deps as $key=>$value) {
    $depts->addRow([$key, $deps[$key]->count()]);
  }

  Lava::PieChart('DeptLectures', $depts, [
      'title' => 'Departments',
      'is3D'   => true,
      'slices' => [
        ['offset' => 0.2],
        ['offset' => 0.25],
        ['offset' => 0.3]
    ]
  ]);

  $years = Lava::DataTable();

  $years->addStringColumn('Year')
          ->addNumberColumn('Lectures');

  $ys = $lectures->groupBy('year');
  foreach($ys as $key=>$value) {
    $years->addRow([$key, $ys[$key]->count()]);
  }

  Lava::PieChart('YearLectures', $years, [
      'title' => 'Years',
      'is3D'   => true,
      'slices' => [
        ['offset' => 0.2],
        ['offset' => 0.25],
        ['offset' => 0.3]
    ]
  ]);

  $total_o = $lectures->sum('total');
  $present_o = $lectures->sum('present');

  $att = Lava::DataTable();
  $att->addStringColumn('Avg Attendance')
      ->addNumberColumn('Value');
  if($total_o!=0)
      $att->addRow(['Overall', round(100*$present_o/$total_o)]);
  $lecs_d = $lectures->groupBy('dept');
  foreach($lecs_d as $key=>$value)
  {
    $temp_tot=$lecs_d[$key]->sum('total');
    if($temp_tot!=0)
      $att->addRow([$key, round(100*$lecs_d[$key]->sum('present')/$temp_tot)]);
  }

  Lava::GaugeChart('AvgAtt', $att, [
      'width'      => 1000,
      'greenFrom'  => 90,
      'greenTo'    => 100,
      'yellowFrom' => 60,
      'yellowTo'   => 89,
      'redFrom'    => 0,
      'redTo'      => 59,
      'majorTicks' => [
          'Low',
          'High'
      ]
  ]);

  $attc = Lava::DataTable();
  $attc->addStringColumn('Avg Attendance')
      ->addNumberColumn('Value');
  $lecs_d = $lectures->groupBy('class');
  foreach($lecs_d as $key=>$value)
  {
    $total = $lecs_d[$key]->sum('total');
    if($total!=0)
      $attc->addRow([$key, round(100*$lecs_d[$key]->sum('present')/$total)]);
  }

  Lava::GaugeChart('AvgAttClass', $attc, [
      'width'      => 1000,
      'greenFrom'  => 90,
      'greenTo'    => 100,
      'yellowFrom' => 60,
      'yellowTo'   => 89,
      'redFrom'    => 0,
      'redTo'      => 59,
      'majorTicks' => [
          'Low',
          'High'
      ]
  ]);

  return view("lecture/analytics");
});

Route::get('/attendance/analytics', function () {
  //return dd(array(\Auth::user()->emp->fac->lectures->groupBy('dept')->sum('present')));
  //return dd(array(\App\SubjectAllocation::where('faculty_id', \Auth::user()->emp->fac->id)->lists('id')));
  $sub_all = \App\SubjectAllocation::remember(60)->where('classpivot_id', \Auth::user()->classp[0]->id)->lists('id');
  $lectures = \App\FacultyLecture::remember(10)->whereIn('subject_allocation_id', $sub_all)->get();
  $dates = array();
  $min = 100;
  $max = 0;
  foreach($lectures as $lecture) {
    if(!array_key_exists($lecture->date, $dates))
      $dates[$lecture->date]=1;
    else
    {
      $dates[$lecture->date]++;
    }
    if($min>$dates[$lecture->date])
      $min = $dates[$lecture->date];
    if($max<$dates[$lecture->date])
      $max = $dates[$lecture->date];
  }

  $lecs = Lava::DataTable();

  $lecs->addDateColumn('Date')
        ->addNumberColumn('Lectures');

  foreach ($dates as $date=>$value) {
      $lecs->addRow([$date, $value]);
  }

  Lava::CalendarChart('Lectures', $lecs, [
      'title' => 'Lectures Conducted',
      'unusedMonthOutlineColor' => [
          'stroke'        => '#ECECEC',
          'strokeOpacity' => 0.75,
          'strokeWidth'   => 1
      ],
      'dayOfWeekLabel' => [
          'color'    => '#4f5b0d',
          'fontSize' => 16,
          'italic'   => true
      ],
      'noDataPattern' => [
          'color' => '#DDD',
          'backgroundColor' => '#11FFFF'
      ],
      'colorAxis' => [
          'values' => [$min, $max],
          'colors' => ['red', 'green']
      ]
  ]);

  $subsl = Lava::DataTable();

  $subsl->addStringColumn('Subjects')
          ->addNumberColumn('Lectures');

  $sbsl = $lectures->groupBy('subject_name');
  foreach($sbsl as $key=>$value) {
    $subsl->addRow([$key, $sbsl[$key]->count()]);
  }

  Lava::PieChart('SubTotLectures', $subsl, [
      'title' => 'Subjects',
      'is3D'   => true,
      'slices' => [
        ['offset' => 0.2],
        ['offset' => 0.25],
        ['offset' => 0.3]
    ]
  ]);

  $att = \App\StudentAttendance::where('classpivot_user_id', \Auth::user()->classu[0]->id)->get();

  $att_sub = Lava::DataTable();

  $att_sub->addStringColumn('Avg Attendance')
          ->addNumberColumn('Lectures');

  $as = $att->groupBy('subject_name');
  foreach($as as $key=>$value) {
    $att_sub->addRow([$key, round($as[$key]->sum('value')*100/$as[$key]->count())]);
  }

  Lava::GaugeChart('AvgAttSub', $att_sub, [
    'width'      => 1000,
    'greenFrom'  => 90,
    'greenTo'    => 100,
    'yellowFrom' => 60,
    'yellowTo'   => 89,
    'redFrom'    => 0,
    'redTo'      => 59,
    'majorTicks' => [
        'Low',
        'High'
    ]
  ]);

  $attc = Lava::DataTable();
  $attc->addStringColumn('Avg Attendance')
      ->addNumberColumn('Value');
  $lecs_d = $att->groupBy(function($date) {
        return \Carbon\Carbon::parse($date->$date)->format('Y-m'); // grouping by years
        //return Carbon::parse($date->created_at)->format('m'); // grouping by months
    });
  foreach($lecs_d as $key=>$value)
  {
    $attc->addRow([ \Carbon\Carbon::parse($key)->format('M'), round(100*$lecs_d[$key]->sum('value')/$lecs_d[$key]->count())]);
  }

  Lava::GaugeChart('AvgAttClass', $attc, [
      'width'      => 1000,
      'greenFrom'  => 90,
      'greenTo'    => 100,
      'yellowFrom' => 60,
      'yellowTo'   => 89,
      'redFrom'    => 0,
      'redTo'      => 59,
      'majorTicks' => [
          'Low',
          'High'
      ]
  ]);

  return view("attendance/analytics");
});

Route::post('oauth/access_token', function() {
  $token = Authorizer::issueAccessToken();
	$user=\App\User::where('email', Input::get('username'))->first();// get the user data from database
  if (!Auth::attempt(['email' => Input::get('username'), 'password' => Input::get('password')]))
  {
      $user = '';
  }

  $temp["token"]=$token;
  $temp["user"]["name"]=$user->name;
  $temp["user"]["email"]=$user->email;
  $emp_id = \App\Employee::where('user_id', $user->id)->first()->id;
  $fac = \App\Faculty::where('employee_id', $emp_id)->first();
  $fac_id = $fac->id;
  $temp["user"]["faculty_id"]=$fac_id;
  $temp["timeslots"]=DB::table('timeslots')->select('id', 'start_time', 'end_time')->get();

  $classrooms = $user->classp;
  //return Response::json(array('data' => $classrooms));
  $ctemp = 0;
  for($i=0; $i<count($classrooms); $i++)
  {
    $sub_all = \App\SubjectAllocation::where('classpivot_id', $classrooms[$i]->id)->where('faculty_id', $fac_id)->get();
    if(count($sub_all)==0)
      countinue;

    $bname = explode(' ', $classrooms[$i]->classroom->name)[2];
    $temp["classes"][$ctemp]["id"]=$classrooms[$i]->id;
    $temp["classes"][$ctemp]["name"]=$classrooms[$i]->classroom->name;
    $temp["classes"][$ctemp]["dept"]=$classrooms[$i]->classroom->department->name;
    $temp["classes"][$ctemp]["capacity"]='80';
    $temp["classes"][$ctemp]["batches"][0]["name"]=$bname.'1';
    $temp["classes"][$ctemp]["batches"][0]["students"]=DB::table('classpivot_user')->select('id as user_id', 'roll_no')->where('classpivot_id', $classrooms[$i]->id)->where('roll_no', '!=', 0)->where('batch_no', 0)->orderBy('roll_no')->get();
    $temp["classes"][$ctemp]["batches"][1]["name"]=$bname.'2';
    $temp["classes"][$ctemp]["batches"][1]["students"]=DB::table('classpivot_user')->select('id as user_id', 'roll_no')->where('classpivot_id', $classrooms[$i]->id)->where('roll_no', '!=', 0)->where('batch_no', 1)->orderBy('roll_no')->get();

    $subs = '';
    for($j=0; $j<count($sub_all); $j++)
    {
      $sid = $sub_all[$j]["id"];
      $subs[$j]["id"] = $sid;
      $subs[$j]["name"] = $sub_all[$j]->subject->name;
      $subs[$j]["shortform"] = $sub_all[$j]->subject->shortform;
      $subs[$j]["is_elective"] = $sub_all[$j]->subject->is_elective;
      $subs[$j]["has_lab"] = $sub_all[$j]->subject->has_prac;
      if($subs[$j]["is_elective"])
      {
        $ids = DB::table('electives')->where('subject_allocation_id', $sid)
                ->where('batch_no', 0)->lists('classpivot_user_id');
        $subs[$j]["batches"][0]["name"]=$bname.'1';
        $subs[$j]["batches"][0]["students"]= \App\ClasspivotUser::select('id as user_id', 'roll_no')->whereIn('user_id', $ids)->where('roll_no', '!=', 0)->orderBy('roll_no')->get();
        if($sub_all[$j]["has_batches"])
        {
          $ids = DB::table('electives')->where('subject_allocation_id', $sid)
                  ->where('batch_no', 1)->lists('classpivot_user_id');
          $subs[$j]["batches"][1]["name"]=$bname.'2';
          $subs[$j]["batches"][1]["students"]= \App\ClasspivotUser::select('id as user_id', 'roll_no')->whereIn('user_id', $ids)->where('roll_no', '!=', 0)->orderBy('roll_no')->get();
        }
      }
      $subs[$j]["modules"] = \App\Module::where('subject_id', $sub_all[$j]->subject->id)->select('id', 'name','desc')->get()->toArray();
      break;
    }
    $temp["classes"][$ctemp]["subjects"] = $subs;
    $ctemp = $ctemp + 1;
  }
  return Response::json(array('data' => $temp));
});


Route::get('api', ['before' => 'oauth', function() {
	// return the protected resource
	//echo “success authentication”;
	$user_id=Authorizer::getResourceOwnerId(); // the token user_id
	$user=\App\User::find($user_id);// get the user data from database
	return Response::json(array('data' => array('user'=>$user)));
}]);

Route::put('event/{id}/design', function(Request $request, $id) {
  $arr = array();
  if(\Input::has('main'))
    $arr = array_merge($arr, ['main' => \Input::get('main')]);
  if(\Input::has('about'))
    $arr = array_merge($arr, ['about' => \Input::get('about')]);
  if(\Input::has('learn_more'))
    $arr = array_merge($arr, ['learn_more' => \Input::get('learn_more')]);
  $e = \App\Event::find($id)->update($arr);
  if($e)
    return response()->json(array('success'=>true));
  else
  {
    return response()->json(array('success'=>false), 500);
  }
});

Route::post('event/{id}/subscribe', function($id) {
  $event = \App\Event::find($id);
  $event->users()->attach(\Auth::id());
  $event->decrement('seats_left');
  return response()->json(array('success'=>true));
});

Route::post('event/{id}/unsubscribe', function($id) {
  $event = \App\Event::find($id);
  $event->users()->detach(\Auth::id());
  $event->increment('seats_left');
  return response()->json(array('success'=>true));
});

Route::group(['middleware' => 'oauth'], function () {
  Route::post('api/attendance/update', function() {

    return Response::json(array('present'=>Input::get('present'),'absent'=>Input::get('absent')));

    $user_id=Authorizer::getResourceOwnerId(); // the token user_id
  	$user=\App\User::find($user_id);// get the user data from database
    $emp_id = \App\Employee::where('user_id', $user->id)->first()->id;
    $fac = \App\Faculty::where('employee_id', $emp_id)->first();

    //return dd($sub_all);
    $tid = Input::get('timeslot_id');
    $mid = Input::get('module_id');
    $lab = Input::get('is_lab');
    $cr_at = Input::get('timestamp');
    $flid = Input::get('flid');
    $date = Carbon\Carbon::parse(Input::get('date'))->format('Y-m-d');

    if($flid == null)
      return Response::json(array('success' => false));
    $fl = \App\FacultyLecture::find($flid);
    $fl->update(['subject_allocation_id'=>$sub_all->id,
            'timeslot_id'=>$tid, 'module_id'=>$mid, 'is_lab'=>$lab, 'date'=>$date, 'created_at'=>$cr_at]);

    $pre_arr = Input::get('present');
    foreach($pre_arr as $att)
    {
      \App\StudentAttendance::create(['faculty_lecture_id'=>$flid->id,
                      'classpivot_user_id'=>$att, 'is_present'=>true, 'created_at'=>$cr_at]);
    }

    $abs_arr = Input::get('absent');
    foreach($abs_arr as $att)
    {
      \App\StudentAttendance::create(['faculty_lecture_id'=>$flid->id,
                      'classpivot_user_id'=>$att, 'is_present'=>false, 'created_at'=>$cr_at]);
    }

    return Response::json(array('success' => true));
  });

  Route::post('api/attendance', function() {


    $user_id=Authorizer::getResourceOwnerId(); // the token user_id
  	$user=\App\User::find($user_id);// get the user data from database

    $emp_id = \App\Employee::where('user_id', $user->id)->first()->id;
    $fac = \App\Faculty::where('employee_id', $emp_id)->first();


    $sub_all = Input::get('subject_id');
    //return dd($sub_all);
    $batch_no = null;
    if(\App\SubjectAllocation::find($sub_all)->has_batches)
      $batch_no = Input::get('batch_no');
    $tid = Input::get('timeslot_id');
    $mid = Input::get('module_id');
    $lab = Input::get('is_lab');
    $cr_at = Input::get('timestamp');
    $date = Carbon\Carbon::parse(Input::get('date'))->format('Y-m-d');

    if($sub_all == null || $tid == null || $date == null)
      return Response::json(array('$sub_all' => $sub_all, '$tid'=>$tid, '$mid'=>$mid, '$date'=>$date));
    $flid = \App\FacultyLecture::create(['subject_allocation_id'=>$sub_all, 'batch_no'=>$batch_no,
            'timeslot_id'=>$tid, 'module_id'=>$mid, 'is_lab'=>$lab, 'date'=>$date, 'created_at'=>$cr_at]);

    if($flid->id==null)
      return Response::json(array('success' => false));

    $pre_arr = Input::get('present');
    foreach($pre_arr as $att)
    {
      \App\StudentAttendance::create(['faculty_lecture_id'=>$flid->id,
                      'classpivot_user_id'=>$att, 'is_present'=>true, 'created_at'=>$cr_at]);
    }

    $abs_arr = Input::get('absent');
    foreach($abs_arr as $att)
    {
      \App\StudentAttendance::create(['faculty_lecture_id'=>$flid->id,
                      'classpivot_user_id'=>$att, 'is_present'=>false, 'created_at'=>$cr_at]);
    }

    return Response::json(array('success' => true));
  });
});

Route::resource('user', 'UserController');

Route::resource('event', 'EventController');


Route::resource('classroom', 'ClassroomController');

Route::resource('forum', 'ForumController');

Route::resource('forum/{fid}/topic', 'TopicController');

Route::resource('forum/{fid}/topic/{tid}/post', 'PostController');

Route::resource('attendance', 'AttendanceController');

Route::resource('lecture', 'LectureController');
