<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class LectureController extends Controller
{

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Provide Lecture data in json
     *
     * @return \Illuminate\Http\Response
     */
    public function getDtData(Request $request)
    {
      $sub_all_in = \App\SubjectAllocation::where('faculty_id', \Auth::user()->emp->fac->id)->lists('id');
      $lectures = \App\FacultyLecture::select([
        'id',
        'subject_allocation_id',
        'timeslot_id',
        'module_id',
        'is_lab',
        'date'
        ])
        ->whereIn('subject_allocation_id', $sub_all_in);

      $dtables = \Datatables::of($lectures);

      if ($keyword = $request->get('search')['value']) {
            $dtables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

      return $dtables
      ->addColumn('options', function ($lecture) {
        return "<div class=\"btn-group\" role=\"group\" >
          <a href=\"/lecture/".$lecture['id']."\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-pencil\"></span></a>
          <a type=\"button\" class=\"btn btn-danger\" data-backdrop=\"false\" data-toggle=\"modal\" data-target=\"#modal".$lecture['id']."\"><span class=\"glyphicon glyphicon-trash\"></a>
        </div>
        <div id=\"modal".$lecture['id']."\" class=\"modal fade\" role=\"dialog\">
          <div class=\"modal-dialog\">

            <!-- Modal content-->
            <div class=\"modal-content\">
              <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
                <h4 class=\"modal-title\">Warning</h4>
              </div>
              <div class=\"modal-body\">
                <p>Are you sure you want to delete this lecture along with the <b><u>attendance data</u></b>?</p>
              </div>
              <div class=\"modal-footer\">
                <button id=\"delete-lecture".$lecture['id']."\" type=\"button\"  value=\"".$lecture['id']."\" class=\"btn btn-danger\" data-dismiss=\"modal\">Delete</button>
              </div>
            </div>

          </div>
        </div>";
      })
      ->make(true);
    }


    /**
     * Provide Subject Allocation data in json
     *
     * @return \Illuminate\Http\Response
     */
    public function  getDtSubjectData(Request $request)
    {
      $classroom_id = \App\Classroom::where('department_id', \Auth::user()->emp->fac->department_id)->lists('id');
      $classp_id = \App\ClassDetail::whereIn('classroom_id', $classroom_id)->lists('id');
      $lectures = \App\SubjectAllocation::whereIn('classpivot_id', $classroom_id);

      $dtables = \Datatables::of($lectures);

      if ($keyword = $request->get('search')['value']) {
            $dtables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

      return $dtables
      ->addColumn('options', function ($lecture) {
        return "<div class=\"btn-group\" role=\"group\" >
          <a href=\"/lecture/subject/".$lecture['id']."\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-pencil\"></span></a>
          <a type=\"button\" class=\"btn btn-danger\" data-backdrop=\"false\" data-toggle=\"modal\" data-target=\"#modal".$lecture['id']."\"><span class=\"glyphicon glyphicon-trash\"></a>
        </div>
        <div id=\"modal".$lecture['id']."\" class=\"modal fade\" role=\"dialog\">
          <div class=\"modal-dialog\">

            <!-- Modal content-->
            <div class=\"modal-content\">
              <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
                <h4 class=\"modal-title\">Warning</h4>
              </div>
              <div class=\"modal-body\">
                <p>Are you sure you want to delete this allocation along with the <b><u>attendance data</u></b>?</p>
              </div>
              <div class=\"modal-footer\">
                <button id=\"delete-lecture".$lecture['id']."\" type=\"button\"  value=\"".$lecture['id']."\" class=\"btn btn-danger\" data-dismiss=\"modal\">Delete</button>
              </div>
            </div>

          </div>
        </div>";
      })
      ->make(true);
    }

    public function getDtDetailsData($id)
    {
      $students = \App\FacultyLecture::find($id)->attendance;
      return \Datatables::of($students)
      ->addColumn('action', function ($student) {
                return "<div class=\"btn-group\" role=\"group\">
          <button id=\"change-attendance".$student->id."\" onClick=\"changeAttendance('".$student->id."', '".$student->faculty_lecture_id."')\" class=\"btn btn-primary pull-right\"><i class=\"glyphicon glyphicon-refresh\"></i></a></button>";
            })
      ->make(true);
    }

    /**
     * Display a listing of all Subect Allocation.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sub_all_in = \App\SubjectAllocation::where('faculty_id', \Auth::user()->emp->fac->id)->lists('id');
      return view('lecture/index')->withFls(\App\FacultyLecture::whereIn('subject_allocation_id',$sub_all_in)->orderBy('date','DESC')->get());
    }

    /**
     * Show the form for creating a new lecture.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $fac_id = \Auth::user()->emp->fac->id;
      $classrooms = \App\User::find(\Auth::user()->id)->classp;
      $temp["timeslots"]=\App\Timeslot::select('id', 'start_time', 'end_time')->remember(60)->get();
      for($i=0; $i<count($classrooms); $i++)
      {
        $croom = \App\Classroom::remember(60)->find($classrooms[$i]->classroom_id);
        $cname = $croom->name;
        $bname = explode(' ', $cname)[2];
        $cid = $classrooms[$i]->id;
        $temp["classes"][$cid]["id"]= $cid;
        $temp["classes"][$cid]["name"]=$cname;
        $dept = \App\Department::remember(60)->find($croom->department_id);
        $temp["classes"][$cid]["dept"]=$dept->name;
        $temp["classes"][$cid]["capacity"]='80';
        $temp["classes"][$cid]["batches"][0]["name"]=$bname.'1';
        $temp["classes"][$cid]["batches"][1]["name"]=$bname.'2';
        $subs = array();
        $sub_all = \App\SubjectAllocation::where('classpivot_id', $classrooms[$i]->id)->where('faculty_id', $fac_id)->get();
        for($j=0; $j<count($sub_all); $j++)
        {
          $sid = $sub_all[$j]["id"];
          $sub = \App\Subject::remember(60)->find($sub_all[$j]->subject_id);
          $subs[$sid]["id"] = $sid;
          $subs[$sid]["name"] = $sub->name;
          $subs[$sid]["shortform"] = $sub->shortform;
          $subs[$sid]["is_elective"] = $sub->is_elective;
          $subs[$sid]["has_prac"] = $sub->has_prac;
          $subs[$sid]["has_theory"] = $sub->has_theory;

          if($subs[$sid]["is_elective"])
          {
            $ids = \App\Elective::where('subject_allocation_id', $sid)->remember(60)->where('batch_no', 0)->lists('classpivot_user_id');

            $subs[$sid]["batches"][0]["name"]=$bname.'1';
            if($sub_all[$j]["has_batches"])
            {
              $ids = \App\Elective::where('subject_allocation_id', $sid)->remember(60)
                      ->where('batch_no', 1)->lists('classpivot_user_id');
              $subs[$sid]["batches"][1]["name"]=$bname.'2';
            }
          }

          $subs[$sid]["modules"] = \App\Module::remember(60)->where('subject_id', $sub_all[$j]->subject_id)->select('id','name', 'desc')->get()->toArray();
        }
        $temp["classes"][$cid]["subjects"] = $subs;
      }
      //return dd($temp);
      \JavaScript::put(['data' => $temp]);
      return view('lecture/new')->withTemp($temp);
    }

    /**
     * Store a newly created lecture in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $fac = \Auth::user()->emp->fac;
      $sub_all = \App\SubjectAllocation::find(\Input::get('subject_id'));
      $tid = \Input::get('timeslot_id');
      $mid = \Input::get('module_id');
      $is_lab = \Input::get('is_lab', 0);
      $batch = \Input::get('batch_no');
      $date = \Carbon\Carbon::now();
      //return dd(array($sub_all, $tid, $mid, $lab, $date));
      if($sub_all==null || $tid==null || $date==null || $mid==-1 || $tid==-1)
      {
        return redirect()->back()->withInput()->withErrors(array('message' => 'Please fill all inputs.'));
      }
      $temp = \App\FacultyLecture::where('subject_allocation_id', $sub_all->id)->where('timeslot_id', $tid)->where('date', $date)->first();
      if(!$temp==null)
        return redirect()->back()->withInput()->withErrors(array('message' => 'Can\'t have two entries on same timeslot in same day, You were teaching '.$temp->subject_name.' in '.$temp->class));
      $sas = \App\SubjectAllocation::where('faculty_id', $fac->id)->lists('id');
      $temp = \App\FacultyLecture::whereIn('subject_allocation_id', $sas)->where('date', $date->toDateString())->get();
      $ts = \App\Timeslot::find($tid);
      $ts_s_c = \Carbon\Carbon::parse($ts->start_time);
      $ts_e_c = \Carbon\Carbon::parse($ts->end_time);
      foreach($temp as $t1)
      {
        $t1_s_c = \Carbon\Carbon::parse($t1->timeslot->start_time);
        $t1_e_c = \Carbon\Carbon::parse($t1->timeslot->end_time);
        if($ts_s_c->between($t1_s_c, $t1_e_c, false) || $ts_e_c->between($t1_s_c, $t1_e_c, false) ||
        $t1_s_c->between($ts_s_c, $ts_e_c, false) || $t1_e_c->between($ts_s_c, $ts_e_c, false))
          return redirect()->back()->withInput()->withErrors(array('message' => 'Can\'t have two entries on same timeslot in same day, You were teaching '.$t1->subject_name.' in '.$t1->class));
      }
      $flid = \App\FacultyLecture::create(['subject_allocation_id'=>$sub_all->id,
              'timeslot_id'=>$tid, 'module_id'=>$mid, 'is_lab'=>$is_lab, 'batch_no'=>$batch , 'date'=>$date->toDateString()]);

      if($flid->id!=null)
        return redirect()->action('AttendanceController@create', ['fl_id'=>$flid])->with('success', 'Lecture Added!');
      else
        return redirect()->back()->withInput()->with('errors', $flid->errors);

    }

    /**
     * Display the specified lecture.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $fac_id = \Auth::user()->emp->fac->id;
      $classrooms = \Auth::user()->classp;
      $temp["timeslots"]=\App\Timeslot::select('id', 'start_time', 'end_time')->remember(60)->get();
      for($i=0; $i<count($classrooms); $i++)
      {
        $croom = \App\Classroom::remember(60)->find($classrooms[$i]->classroom_id);
        $cname = $croom->name;
        $bname = explode(' ', $cname)[2];
        $cid = $classrooms[$i]->id;
        $temp["classes"][$cid]["id"]= $cid;
        $temp["classes"][$cid]["name"]=$cname;
        $dept = \App\Department::remember(60)->find($croom->department_id);
        $temp["classes"][$cid]["dept"]=$dept->name;
        $temp["classes"][$cid]["capacity"]='80';
        $temp["classes"][$cid]["batches"][0]["name"]=$bname.'1';
        $temp["classes"][$cid]["batches"][1]["name"]=$bname.'2';
        $subs = array();
        $sub_all = \App\SubjectAllocation::where('classpivot_id', $classrooms[$i]->id)->remember(60)->where('faculty_id', $fac_id)->get();
        for($j=0; $j<count($sub_all); $j++)
        {
          $sid = $sub_all[$j]["id"];
          $sub = \App\Subject::remember(60)->find($sub_all[$j]->subject_id);
          $subs[$sid]["id"] = $sid;
          $subs[$sid]["name"] = $sub->name;
          $subs[$sid]["shortform"] = $sub->shortform;
          $subs[$sid]["is_elective"] = $sub->is_elective;
          $subs[$sid]["has_prac"] = $sub->has_prac;
          $subs[$sid]["has_theory"] = $sub->has_theory;

          if($subs[$sid]["is_elective"])
          {
            $ids = \App\Elective::where('subject_allocation_id', $sid)->remember(60)->where('batch_no', 0)->lists('classpivot_user_id');

            $subs[$sid]["batches"][0]["name"]=$bname.'1';
            if($sub_all[$j]["has_batches"])
            {
              $ids = \App\Elective::where('subject_allocation_id', $sid)->remember(60)
                      ->where('batch_no', 1)->lists('classpivot_user_id');
              $subs[$sid]["batches"][1]["name"]=$bname.'2';
            }
          }

          $subs[$sid]["modules"] = \App\Module::remember(60)->where('subject_id', $sub_all[$j]->subject_id)->select('id','name', 'desc')->get()->toArray();
        }
        $temp["classes"][$cid]["subjects"] = $subs;
      }
      $fl = \App\FacultyLecture::find($id);
      $subject_id = $fl->subject_allocation_id;
      $sub_all = \App\SubjectAllocation::find($subject_id);
      $class_id = $sub_all->classpivot_id;
      $timeslot_id = $fl->timeslot_id;
      $module_id = $fl->module_id;
      $is_lab = $fl->is_lab;
      $batch_no = $fl->batch_no;
      \JavaScript::put(['data' => $temp]);
      return view('lecture/edit')->withTemp($temp)->with(compact(['id', 'subject_id', 'class_id', 'timeslot_id', 'module_id', 'is_lab', 'batch_no']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $fac = \Auth::user()->emp->fac;
      $fl = \App\FacultyLecture::find($id);
      $sub_all = \App\SubjectAllocation::find(\Input::get('subject_id'));
      $tid = \Input::get('timeslot_id');
      $mid = \Input::get('module_id');
      $lab = \Input::get('is_lab');
      $batch = \Input::get('batch_no');
      //return dd(array($sub_all, $tid, $mid, $lab, $date));
      if($sub_all==null || $tid==null)
      {//Please fill all inputs.
        return redirect()->back()->withInput()->withErrors(array('message' => 'Please fill all inputs.'));
      }
      $temp = \App\FacultyLecture::where('subject_allocation_id', $sub_all->id)->where('module_id', $mid)
              ->where('is_lab', $lab)->where('timeslot_id', $tid)->where('date', $fl->date)->where('batch_no', $batch)->first();
      if($temp!=null)
          return redirect()->back()->withInput()->withErrors(array('message' => 'No update done'));

      if( ($fl->subject_allocation->class_detail!=$sub_all->class_detail) ||
          ($fl->is_lab!=$lab) || ($fl->batch_no!=$batch) )
        $st_att = \App\StudentAttendance::where('faculty_lecture_id', $id)->delete();

      $users = '';
      if($fl->subject_allocation->has_batches==1)
      {
        if($fl->subject_allocation->subject->is_elective==1)
        {
          $uid = \App\Elective::where('subject_allocation_id', $fl->subject_allocation_id)->lists('classpivot_user_id');
          $users = \App\ClasspivotUser::whereIn('id', $uid)->where('roll_no', '!=', 0)->get();
        }
        else
        {
          $users = \App\ClasspivotUser::where('roll_no', '!=', 0)->where('classpivot_id', $fl->subject_allocation->classpivot_id)->where('batch_no', $fl->batch_no)->get();
          //return dd($users);
        }
      }
      else
        $users = \App\ClasspivotUser::where('roll_no', '!=', 0)->where('classpivot_id', $fl->subject_allocation->classpivot_id)->get();


      $flid = \App\FacultyLecture::find($id)->update(['subject_allocation_id'=>$sub_all->id,
              'timeslot_id'=>$tid, 'module_id'=>$mid, 'is_lab'=>$lab, 'batch_no'=>$batch]);

      foreach ($users as $key => $user) {
        \App\StudentAttendance::create(['faculty_lecture_id'=>$id,
                        'classpivot_user_id'=>$user->id, 'is_present'=>false]);
      }


      if($flid)
        return redirect()->action('AttendanceController@index')->with('highlight', $id);
      else
        return redirect()->back()->with('errors', $flid->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $lec = \App\FacultyLecture::find($id);
      $lec->delete();
      return response()->json(array('success'=>true));
    }
}
