<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AttendanceController extends Controller
{

    public function verify($username, $password)
    {
      $credentials = [
        'email' => $username,
        'password' => $password,
      ];

      if (\Auth::once($credentials)) {
        return \Auth::user()->id;
      } else {
        return false;
      }
    }

    public function __construct()
    {
        //$this->middleware('oauth', ['only' => ['store']]);
    }

    /**
     * Display a listing of the lectures conducted by a faculty.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/lecture/');
    }

    /**
     * Show the form for adding attendance for a new Lecture.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $fl_id = \Input::get('fl_id');
      $fl = \App\FacultyLecture::find($fl_id);
      if($fl==null)
        return redirect('/lecture/');
      $temp = \App\StudentAttendance::where('faculty_lecture_id', $fl_id)->get();
      //return dd()
      if(!$temp->isEmpty())
        return redirect('/attendance/'.$fl_id);
      if($fl->subject_allocation->has_batches==1)
      {
        if($fl->subject_allocation->subject->is_elective==1)
        {
          $uid = \App\Elective::where('subject_allocation_id', $fl->subject_allocation_id)->lists('classpivot_user_id');
          $users = \App\ClasspivotUser::whereIn('id', $uid)->where('roll_no', '!=', 0)->get();
        }
        else
        {
          $users = \App\ClasspivotUser::where('roll_no', '!=', 0)->where('classpivot_id', $fl->subject_allocation->classpivot_id)->where('batch_no', $fl->batch_no)->get();
          //return dd($users);
        }
      }
      else
        $users = \App\ClasspivotUser::where('roll_no', '!=', 0)->where('classpivot_id', $fl->subject_allocation->classpivot_id)->get();
      //return dd($users);
      return view('attendance/new')->withUsersP($users)->withFl($fl_id);
    }

    /**
     * Store attendance for newly created lecture in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $pre_arr = \Input::get('present');
      $fl_id = \Input::get('fl');
      $temp = \App\FacultyLecture::find($fl_id);
      if($temp==null)
        return redirect('/lecture/');
      if($pre_arr!==null)
        foreach($pre_arr as $att)
        {
          \App\StudentAttendance::create(['faculty_lecture_id'=>$fl_id,
                          'classpivot_user_id'=>$att, 'is_present'=>true]);
        }

      $abs_arr = \Input::get('absent');
      if($abs_arr!==null)
        foreach($abs_arr as $att)
        {
          \App\StudentAttendance::create(['faculty_lecture_id'=>$fl_id,
                          'classpivot_user_id'=>$att, 'is_present'=>false]);
        }

      return redirect('/lecture/');
    }

    /**
     * Display the form to edit attendance for specified lecture.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $fl = \App\FacultyLecture::find($id);
      $sa = \DB::table('subject_allocations')->find($fl->subject_allocation_id);
      $abs = \App\StudentAttendance::where('faculty_lecture_id', $id)->where('is_present', 0)->lists('classpivot_user_id');
      $pres = \App\StudentAttendance::where('faculty_lecture_id', $id)->where('is_present', 1)->lists('classpivot_user_id');
      $users_a = \App\ClasspivotUser::whereIn('id', $abs)->get();
      $users_p = \App\ClasspivotUser::whereIn('id', $pres)->get();
      return view('attendance/edit')->withUsersP($users_p)->withUsersA($users_a)->withFl($fl->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sa = \App\StudentAttendance::find($id);
      $sa->update(['is_present'=>!$sa->is_present]);
      return response()->json(array('success'=>true, 'is_present'=>$sa->is_present));
    }

    /**
     * Update the attendance for specified lecture in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $pre_arr = \Input::get('present');
      $fl_id = $id;
      $temp = \App\FacultyLecture::find($fl_id);
      if($temp==null)
        return redirect('/lecture/');
      foreach($pre_arr as $att)
      {
        \App\StudentAttendance::where('faculty_lecture_id', $fl_id)
        ->where('classpivot_user_id',$att)
        ->update(['is_present'=>true]);
      }

      $abs_arr = \Input::get('absent');
      foreach($abs_arr as $att)
      {
        \App\StudentAttendance::where('faculty_lecture_id', $fl_id)
        ->where('classpivot_user_id',$att)
        ->update(['is_present'=>false]);
      }
      return redirect('/attendance/'.$id)->with('success', 'Update Successful');
    }

    /**
     * Destroy method not required for attendance as it will be associated with lecture.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 'Not Required. Destroy '.$id;
    }
}
