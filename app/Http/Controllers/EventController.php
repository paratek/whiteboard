<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Display a listing of all events.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('events.index');
    }

    /**
     * Show the form for creating a new event.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(\Auth::guest())
          return redirect('/auth/login');
        else
          return view('events.new');

    }

    /**
     * Store a newly created form in storage.
     *
     * @param  \Illum'inate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $main = "<blockquote>
          Event Description
      </blockquote>
      <img alt=\"Example of bad variable names\" height=\"174\" src=\"http://whiteboard.co/vendor/content-tools/images/image.png\" width=\"600\">
      <p class=\"article__caption\">
          <b>Above:</b>&nbsp;Here you see a photo of the previous year's event.
      </p>
      <p>
          The names we devise for folders, files, variables, functions, classes and any other user definable construct will (for the most part) determine how easily someone else can read and understand what we have written.
      </p>
      <p>
          To avoid repetition in the remainder of this section we use the term variable to cover folders, files, variables, functions, and classes.
      </p>
      <iframe frameborder=\"0\" height=\"300\" src=\"https://www.youtube.com/embed/I9jSKFk5Zp0\" width=\"400\"></iframe>
      <h2>
          Be descriptive and concise
      </h2>
      <p>
          It’s the most obvious naming rule of all but it’s also perhaps the most difficult. In my younger years I would often use very descriptive names, I pulled the following doozy from the archives:
      </p>";
      $about = "<h3 class=\"author__about\">
          About the speaker
      </h3>
      <img alt=\"Anthony Blackshaw\" class=\"[ author__pic ]  [ align-right ]\" height=\"80\" src=\"http://whiteboard.co/vendor/content-tools/images/author-pic.jpg\" width=\"80\">
      <p class=\"author__bio\">
          Anthony Blackshaw is a co-founder of Getme, an employee owned company with a focus on web tech. He enjoys writing and talking about tech, especially code and the occasional Montecristo No.2s.
      </p>";
      $learn_more = "<h3>
          Want to learn more?
      </h3>
      <p>
          If you'd like to learn more about the event, <a href=\"https://google.com\"><u>click here.</u></a>
      </p>";

      $event = new \App\Event;
      $event->title = \Input::get('title');
      $event->main = $main;
      $event->about = $about;
      $event->learn_more = $learn_more;
      $event->user_id = \Auth::user()->id;
      $event->start = \Input::get('start');
      $event->end = \Input::get('end');
      if(\Input::has('capacity'))
      {
        $event->capacity = \Input::get('capacity');
        $event->seats_left = \Input::get('capacity');
      }
      $event->save();

      $event->classpivot()->attach(\Input::get('for'));

      return redirect()->action('EventController@show', ['id'=>$event->id]);

    }

    /**
     * Display the specified event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = \App\Event::find($id);
        if($event==null)
          return redirect('/event');
        else
          return view('events.design')->withEvent($event);
    }

    /**
     * Show the form for editing the specified event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $event = \App\Event::find($id);
      if($event==null)
        return redirect('/event');
      else
        return view('events.edit')->withEvent($event);
    }

    /**
     * Update the specified event in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try{
        $event = \App\Event::find($id);

        $cap = \Input::get('capacity');
        $left = $event->seats_left;
        if($cap < $event->capacity && $cap < $event->capacity - $event->seats_left)
        {
          return redirect()->back()->withInput()->withErrors('Capacity is less than the number of users attending event');
        }
        if($cap!=$event->capacity && $cap!=null)
        {
          $left = $cap - count(\DB::table('event_user')->where('event_id', $id)->get());
        }
        else if($cap==null)
        {
          $left=null;
        }

        //return dd(\Input::all());

        if($event->update(array_merge(\Input::only('title', 'start', 'end', 'capacity'), ['seats_left'=>$left])))
        {
          $event->classpivot()->sync(\Input::get('for'));
          return redirect()->action('EventController@edit', ['id'=>$id])->with('success', 'Event Updated!');
        }
        else {
          return redirect()->back()->withInput()->withErrors('Event not updated');
        }
      }
      catch(\Exception $e)
      {
        return $e;
        return redirect()->back()->withInput()->withErrors('System Error');
      }

    }

    /**
     * Remove the specified event from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $e = \App\Event::find($id);
      return response()->json(array('success'=>$e->delete()));
    }

    public function getAttendeeData(Request $request, $id)
    {
      $u_id = \DB::table('event_user')->where('event_id', $id)->lists('user_id');
      $users = \App\User::whereIn('id', $u_id);
      $dtables = \Datatables::of($users);
      if ($keyword = $request->get('search')['value']) {
            $dtables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }
      return $dtables->make(true);
    }

    public function getDtData(Request $request)
    {
      $cp = \App\ClasspivotUser::where('user_id', \Auth::user()->id)->lists('classpivot_id');

      $e = \DB::table('classpivot_event')->whereIn('classpivot_id', $cp)->lists('event_id');

      $events = \App\Event::whereIn('id', $e)->orderBy('start');

      $dtables = \Datatables::of($events);

      if ($keyword = $request->get('search')['value']) {
            $dtables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

      $user = \Auth::id();

      return $dtables
      ->addColumn('options', function ($event) use($user) {
        if($user==$event->user_id)
          return "<div class=\"btn-group\" role=\"group\" >
            <a href=\"/event/".$event['id']."\" type=\"button\" class=\"btn btn-success\">View</a>
            <a href=\"/event/".$event['id']."/edit\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-pencil\"></span></a>
            <a type=\"button\" class=\"btn btn-danger\" data-backdrop=\"false\" data-toggle=\"modal\" data-target=\"#modal".$event['id']."\"><span class=\"glyphicon glyphicon-trash\"></a>
          </div>
          <div id=\"modal".$event['id']."\" class=\"modal fade\" role=\"dialog\">
            <div class=\"modal-dialog\">

              <!-- Modal content-->
              <div class=\"modal-content\">
                <div class=\"modal-header\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
                  <h4 class=\"modal-title\">Warning</h4>
                </div>
                <div class=\"modal-body\">
                  <p>Are you sure you want to delete this event along with the <b><u>attendees data</u></b>?</p>
                </div>
                <div class=\"modal-footer\">
                  <button id=\"delete-lecture".$event['id']."\" type=\"button\"  value=\"".$event['id']."\" class=\"btn btn-danger\" data-dismiss=\"modal\">Delete</button>
                </div>
              </div>

            </div>
          </div>";
        else
          return "<a href=\"/event/".$event['id']."\" class=\"btn btn-primary\">View</a>";
      })
      ->make(true);
    }
}
