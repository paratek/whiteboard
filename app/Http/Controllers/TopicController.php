<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TopicController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($fid)
  	{
      $forum = \App\Forum::find($fid);
      $topics = \App\Topic::where('forum_id', $fid)->paginate(10);
  		return view('forum.forum')->with('forum', $forum)->with('topics', $topics);
  	}

  	/**
  	 * Show the form for creating a new resource.
  	 *
  	 * @return \Illuminate\Http\Response
  	 */
  	public function create()
  	{
  		$depts = \App\Department::whereIn('id', \App\Classroom::whereIn('id', \DB::table('classroom_user')->where('user_id',\Auth::user()->id)->lists('classroom_id'))->lists('department_id'))->get()->toArray();
  		return view('forum.new_topic')->with('classrooms', \Auth::user()->classrooms->toArray())->with('depts', $depts);
  	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  	public function store()
  	{
  		//$topic = \App\Topic::create(['title'=>Request::input('title')]);
  		//$topic->classroom()->attach()
  		$input = Input::all();
  		$validation = Validator::make($input, Post::$rules);

  		if ($validation->passes())
  		{
  			$this->post->create($input);
  			return Response::json(array('success' => true, 'errors' => '', 'message' => 'Post created successfully.'));
  		}
  		return Response::json(array('success' => false, 'errors' => $validation, 'message' => 'All fields are required.'));
  	}

  	/**
  	 * Display the specified resource.
  	 *
  	 * @param  int  $id
  	 * @return \Illuminate\Http\Response
  	 */
  	public function show($fid, $tid)
  	{
  		$class = \App\Forum::find($fid)->classrooms;
  		foreach($class as $temp)
  			if(in_array($temp->id, \Auth::user()->classrooms->lists('id')->toArray()))
  			{
  				\App\Topic::find($tid)->increment('views');
  				return view('forum.topic')->with('posts', \App\Post::where('topic_id', $tid)->paginate(10))->with('topic', \App\Topic::find($tid))->with('forum', \App\Forum::find($fid));
  			}
  		return redirect(url('/home'));


  	}

  	/**
  	 * Show the form for editing the specified resource.
  	 *
  	 * @param  int  $id
  	 * @return \Illuminate\Http\Response
  	 */
  	public function edit($id)
  	{
  		//
  	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  	public function update(Request $request, $id)
  	{
  		//
  	}

  	/**
  	 * Remove the specified resource from storage.
  	 *
  	 * @param  int  $id
  	 * @return \Illuminate\Http\Response
  	 */
  	public function destroy($id)
  	{
  		//
  	}

}
