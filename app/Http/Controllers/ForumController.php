<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ForumController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDtClassForumData(Request $request)
    {
      if(\Auth::user()->hasRole('fact'))
        return;
      \DB::statement(\DB::raw('set @rownum=0'));
      $lectures = \App\Post::select([
        \DB::raw('@rownum  := @rownum  + 1 AS rownum'),
        'id',
        'user_id',
        'message',
        'upvotes',
        'postable_id',
        'created_at'
        ])
        ->where('postable_id', \Auth::user()->classp[0]->id)->where('postable_type', 'App\ClassForum');

      $dtables = \Datatables::of($lectures);

      if ($keyword = $request->get('search')['value']) {
            $dtables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

      return $dtables
      ->addColumn('options', function ($lecture) {
        return "<div class=\"btn-group\" role=\"group\" >
          <a type=\"button\" class=\"btn btn-danger\" data-backdrop=\"false\" data-toggle=\"modal\" data-target=\"#modal".$lecture['id']."\"><span class=\"glyphicon glyphicon-flag\"></a>
        </div>
        <div id=\"modal".$lecture['id']."\" class=\"modal fade\" role=\"dialog\">
          <div class=\"modal-dialog\">

            <!-- Modal content-->
            <div class=\"modal-content\">
              <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
                <h4 class=\"modal-title\">Warning</h4>
              </div>
              <div class=\"modal-body\">
                <p>Does this post conatin <b><u>offensive words</u></b>?</p>
              </div>
              <div class=\"modal-footer\">
                <button id=\"delete-lecture".$lecture['id']."\" type=\"button\"  value=\"".$lecture['id']."\" class=\"btn btn-danger\" data-dismiss=\"modal\">Yes</button>
                <button type=\"button\"  value=\"".$lecture['id']."\" class=\"btn btn-primary\" data-dismiss=\"modal\">No</button>
              </div>
            </div>

          </div>
        </div>";
      })
      ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('forum/class_index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $class_list = \Auth::user()->classrooms->lists('id');
        $depts = \App\Department::whereIn('id', $class_list)->get();
        //return dd($depts);
        return view('forum.new_forum')->with('classrooms', \Auth::user()->classrooms)->with('depts', $depts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      return dd($request->input());
      $input =  $request->input();
      $validation = \Validator::make($input, \App\Forum::$rules);

      if ($validation->passes())
      {
        $forum = \App\Forum::create(['name'=>$input['name'], 'desc'=> $input['desc']]);

        //return \Response::json(array('success' => true, 'errors' => '', 'message' => 'Post created successfully.'));
        return back()->with('status', 'Profile updated!');
      }
      //return \Response::json(array('success' => false, 'errors' => $validation, 'message' => 'All fields are required.'));
      return back()->with('error', 'Error, try again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
