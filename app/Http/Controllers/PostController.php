<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PostController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($tid)
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($tid)
	{
		return "Create ".$tid;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request, $fid, $tid)
	{
		$input =  $request->input();
		$validation = \Validator::make($input, \App\Post::$rules);

		if ($validation->passes())
		{
			\App\Post::create(['message'=>$input['message'], 'user_id'=>\Auth::user()->id, 'topic_id'=> $tid]);
			//return \Response::json(array('success' => true, 'errors' => '', 'message' => 'Post created successfully.'));
			return back()->with('status', 'Profile updated!');
		}
		//return \Response::json(array('success' => false, 'errors' => $validation, 'message' => 'All fields are required.'));
		return back()->with('error', 'Error, try again');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($tid, $id)
	{
		return $tid." ".$id;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
