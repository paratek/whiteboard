<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Employee
 *
 * @property-read \App\User $user
 * @property-read \App\Faculty $fac
 * @property integer $id
 * @property integer $user_id
 * @property integer $r_no
 * @property string $shortform
 * @property integer $category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Employee extends Model
{
  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function fac()
  {
    return $this->hasOne('App\Faculty');
  }
}
