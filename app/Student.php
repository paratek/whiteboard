<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Student
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $gr_no
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 */
class Student extends Model
{
  public function user()
  {
    return $this->belongsTo('App\User');
  }
}
