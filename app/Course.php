<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @property integer $id
 * @property string $course_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Course extends Model {

	//

}
